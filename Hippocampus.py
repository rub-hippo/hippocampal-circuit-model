import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
from sets import Set
import matplotlib.pyplot as plt
import random
import time
import copy
import scipy.stats
import scipy.misc
import scipy.signal
from matplotlib import cm
import cPickle as pickle
import matplotlib.image as mpimg
import matplotlib._png as png
import matplotlib.gridspec as gridspec
from matplotlib.cbook import get_sample_data
from mpl_toolkits.axes_grid1 import ImageGrid
from matplotlib.path import Path
import matplotlib.patches as patches
from sklearn import svm, linear_model , lda, decomposition, preprocessing



#mpl Figure parameter
mpl.rcParams.update({'font.size': 12})
mpl.rcParams.update({'legend.handlelength': 1.})
mpl.rcParams.update({'legend.labelspacing': 0.1})
#mpl.rcParams['figure.figsize'] = [50, 50]
mpl.rcParams['font.family'] = ['sans-serif']
mpl.rcParams['text.usetex'] = False
mpl.rcParams['svg.fonttype'] = 'none'
mpl.rcParams['mathtext.default'] = 'regular'

begin = time.time()
# useable functions#############################################################################################
def save_figures(path = '/home/torsten/Documents/figures/', start = 0, title='', file_type = 'png'):
	figures=[manager.canvas.figure for manager in mpl._pylab_helpers.Gcf.get_all_fig_managers()]

	for i, figure in enumerate(figures):
		figure.savefig(path+ title +'%d.'% int(i+start) +file_type, dpi = 600)

def normalize(pattern, one_by_one = True): #normalize pattern over last dimension;
	
	dim =len(pattern.shape)
	
	if dim == 4:
		norms = np.sqrt(np.einsum('enpj,enpj->enp', pattern*1.,pattern*1.)).repeat(pattern.shape[-1]).reshape(pattern.shape)
		norms[norms == 0] =1
		pattern /= norms
	
	if dim == 3:
		norms = np.sqrt(np.einsum('npj,npj->np', pattern*1.,pattern*1.)).repeat(pattern.shape[-1]).reshape(pattern.shape)
		norms[norms == 0] =1
		pattern /= norms
	
	if dim ==2:
		if one_by_one:
			for p in pattern:
				p /= np.sqrt(np.dot(p,p))
		else:
			pattern /= np.sqrt(np.einsum('pj,pj->p', pattern*1.,pattern*1.)).repeat(pattern.shape[1]).reshape(pattern.shape)
	
	if dim ==1:
		pattern/= np.sqrt(np.sum(pattern**2))

def normalize_1norm(pattern):
	pattern -= np.einsum('ij -> i', pattern).repeat(pattern.shape[1]).reshape(pattern.shape)

def determineSparsity(pattern, binary = True): # return activity level of each pattern, patterns must be of dimension 2

	if binary:
		p = (pattern + 1)/2
	else:
		p = pattern
	activity = np.sum(p, -1)/(pattern.shape[-1]+0.0)
	return activity

def makeFigure2Spatialcells(fig = None, p1=None, p2=None, locations=None, label1 = None, label2 = None, title = None, legend = 0, geometry = [1,1,1], colors = ['b', 'g']):
	
	if fig == None:
		fig = plt.figure()
	ax1 = fig.add_subplot(geometry[0],geometry[1],geometry[2] )
	non_z1 = np.flatnonzero(p1)
	non_z2 = np.flatnonzero(p2)
	ax1.scatter( locations[:,0][non_z1], locations[:,1][non_z1], c= colors[0], faceted = 0, label = label1)
	ax1.scatter( locations[:,0][non_z2], locations[:,1][non_z2], c= colors[1], alpha = 0.5, faceted = 0, label = label2)
	ax1.set_xlim(0,1)
	ax1.set_ylim(0,1)
	if title != None:
		ax1.set_title(title)
	if legend:
		ax1.legend()
	return ax1

def makeInset(fig = None, ax = None, ax_xy = None, ax_width = None, ax_height = None): #make inset to axes ax in figure fig at axcoordinates (bottomleft) ax_xy; return inset, instances of Axes
	
	display_xy = ax.transData.transform(ax_xy)
	display_area = ax.transData.transform((ax_width, ax_height))- ax.transData.transform((0, 0))
	inv = fig.transFigure.inverted()
	fig_xy = inv.transform(display_xy)
	fig_area = inv.transform(display_area)
	return fig.add_axes([fig_xy[0], fig_xy[1], fig_area[0], fig_area[1]])
	
def makeLabel(ax = None, label = None, sci = False): #puts letter at the upper right corner at axes
	
	if sci:
		ax.annotate(xytext=(-0.15, 1.04), xy=(-0.15, 1.04), textcoords = 'axes fraction' ,s =label, size = 'xx-large', weight = 'extra bold', visible = True, annotation_clip=False)
		ax.yaxis.get_major_ticks()[-1].label1.set_visible(False)
	else:
		ax.annotate(xytext=(-0.2, 1.1), xy=(-0.2, 1.1),textcoords = 'axes fraction' ,s =label, size = 'xx-large', weight = 'extra bold', visible = True, annotation_clip=False)


def plotCell(In, patterns = None, env = 0, cell = None, binary = False, fig = None, noise = None, only_stored=True, ax_index = None, ax = None, color = 'r', cb = 0, size =20, zeros = False,vmin = None, vmax = None):
	'''
	abstract
	plots cell firing over space of cell cell. Location of pixels are provided by Input Instance In. Plot is made in ax of figure fig.
	:param In: Input Instance that provides locations of the pixels. Additional if patterns is not given, it uses self.input_stored in In.
	:type In: Input Instance; In.number_patterns must be the same as number of patterns in patterns
	:param patterns: cell population firing over space
	:type patterns: array of dimension 3 (envirionment, pattern, cell fire)
	:param cell: Index of which cell is plotted
	:type cell: int
	:param env: Index of environment
	:type env: int
	:param binary: Whether firing is plotted as 1 or 0
	:type binary: bool
	:param fig: Figure Instance in which the plot is plotted; if None, new Figure is created
	:type fig:Figure Instance 
	:param ax: optional, gives the axes into which plot is plotted. If not given, a new axes instance is created in fig
	:type ax:axes instance 
	:param ax_index: index of new created axes Intance in the figure fig
	:type ax_index lsit of length 3
	:param color: color of pixel, when binary = True
	:type color: mpl color
	:param cb: Whether colorbar is added
	:type cb: bool
	:param size: size of pixel
	:type size: int
	'''



	if fig ==None:
		fig = plt.figure()
	if patterns ==None:
		patterns = In.input_stored
	if ax == None:
		ax = fig.add_subplot(ax_index[0], ax_index[1], ax_index[2])
		ax.set_title('Firing of cell '+str(cell))
	if only_stored:
		loc = In.locations[In.store_indizes]
	else:
		loc = np.tile(In.locations, (env+1,1,1))
	ax.set_xlim(-0.05,In.cage[0])
	ax.set_ylim(-0.05,1.0, In.cage[1])
	#print 'plotcell', patterns.shape, loc.shape
	cb_possible = True
	if zeros == False:
		if noise == None:
			if binary:

				s = ax.scatter(loc[env][:,0][np.flatnonzero(patterns[env][:,cell] != 0)], loc[env][:,1][np.flatnonzero(patterns[env][:,cell] != 0)], c = color, faceted = False, s = size)
				
			else:
				s = ax.scatter(loc[env][:,0][np.flatnonzero(patterns[env][:,cell] != 0)], loc[env][:,1][np.flatnonzero(patterns[env][:,cell] != 0)], c = patterns[env][:,cell][np.flatnonzero(patterns[env][:,cell] != 0)], edgecolor = 'none',cmap=cm.jet, s = size, vmin = vmin, vmax = vmax)
				if np.flatnonzero(patterns[env][:,cell] != 0).shape < 1:
					cb_possible = False
		else:
			if binary:
				s = ax.scatter(loc[env][:,0][np.flatnonzero(patterns[env, noise][:,cell] != 0)], loc[env][:,1][np.flatnonzero(patterns[env,noise][:,cell] != 0)], c = color, faceted = False, s = size)
			else:
				s = ax.scatter(loc[env][:,0][np.flatnonzero(patterns[env, noise][:,cell] != 0)], loc[env][:,1][np.flatnonzero(patterns[env,noise][:,cell] != 0)], c = patterns[env,noise][:,cell][np.flatnonzero(patterns[env,noise][:,cell] != 0)], faceted = False,cmap=cm.jet, s = size, vmin = vmin, vmax = vmax)
				if np.flatnonzero(patterns[env,noise][:,cell] != 0).shape < 1:
					cb_possible = False
	if zeros == True:
		if noise == None:
			if binary:
				s = ax.scatter(loc[env][:,0], loc[env][:,1], c = color, faceted = False, s = size)
			else:
				s = ax.scatter(loc[env][:,0], loc[env][:,1], c = patterns[env][:,cell], faceted = False,cmap=cm.jet, s = size, vmin = vmin, vmax = vmax)
		else:
			if binary:
				s = ax.scatter(loc[env][:,0], loc[env][:,1], c = color, faceted = False, s = size)
			else:
				s = ax.scatter(loc[env][:,0], loc[env][:,1], c = patterns[env,noise][:,cell], faceted = False,cmap=cm.jet, s = size, vmin = vmin, vmax = vmax)
	if cb and cb_possible:
		fig.colorbar(s)
	return [ax,s]


def calcSeparationIndex(In = None, corrs = None):
	corr_input = In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1)
	relevant_pairs = np.nonzero(corr_input >= 0.2)
	index = corrs[relevant_pairs]/corr_input[relevant_pairs]
	
	return np.sum(index)/index.shape[0]
	

def regressionLine2(points_x = None, points_y = None, relevant = None, return_pairs = False):
	
	if relevant != 0:
		relevant_pairs = np.nonzero(points_x >= relevant)
		fit = np.polyfit(points_x[relevant_pairs], points_y[relevant_pairs], 1)
		line = np.poly1d(fit)
		pearson_r = scipy.stats.pearsonr(points_x[relevant_pairs], points_y[relevant_pairs])[0]

	else:
		fit = np.polyfit(points_x, points_y, 1)
		line = np.poly1d(fit)
		pearson_r = scipy.stats.pearsonr(points_x, points_y)[0]
	if return_pairs:
		return [line, pearson_r, relevant_pairs]
	else:
		return [line, pearson_r]
	
	
def regressionLine(points_x = None, points_y = None, relevant = None, return_pairs = False):
	
	if relevant != None:
		relevant_pairs = np.nonzero(points_x >= relevant)
		regression = scipy.stats.linregress(points_x[relevant_pairs], points_y[relevant_pairs])
		line = np.poly1d(regression[:2])
	else:
		regression = scipy.stats.linregress(x = points_x, y = points_y)
		line = np.poly1d(regression[:2])

	if return_pairs:
		return [line, regression[2], relevant_pairs]
	else:
		return [line, regression[2]]
		#ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoDg.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), c ='g')




#save and load functions########################################################################	
def save(data, title, path ='/home/torsten/Documents/simulations/' ):
	foo = open( path+title+'.p', "wb" )
	pickle.dump( data, foo )
	foo.close()
	
def load(stored_file, path ='/home/torsten/Documents/simulations/' ):
	#foo = open( '/scratch/tneh/' + stored_file, "rb" )
	foo = open( path + stored_file, "rb" )
	data = pickle.load(foo)
	foo.close()
	return data

def saveInput(In = None, title= None, path ='/home/torsten/Documents/simulations/' ):
	ToStore = copy.copy(In)
	ToStore.inputMethod = None
	ToStore.noiseMethod = None
	ToStore.actFunction = None
	ToStore.Lec = None
	save(ToStore, title, path)
	
def saveHippo(hippo = None, title= None, path ='/home/torsten/Documents/simulations/' ):
	ToStore = copy.copy(hippo)
	
	ToStore.incrementalLearnMethod = None
	ToStore.initMethod = None
	ToStore.actFunctionsRegions = None
	ToStore.init_ca3_ca1 = None
	
	if ToStore.Ec_Dg != None:
		ToStore.Ec_Dg.getOutput = None
		ToStore.Dg_Ca3.getOutput = None
		ToStore.Ec_Dg.weights = None
		ToStore.Dg_Ca3.weights = None
	
	ToStore.Ec_Ca1.getOutput = None
	ToStore.Ec_Ca3.getOutput = None
	ToStore.Ca3_Ca1.getOutput = None 
	ToStore.Ca3_Ca3.getOutput = None 
	ToStore.Ca1_Ec.getOutput = None 
	

	ToStore.Ec_Ca1.weights = None
	ToStore.Ec_Ca3.weights = None
	ToStore.Ca3_Ca1.weights = None 
	ToStore.Ca3_Ca3.weights = None 
	ToStore.Ca1_Ec.weights = None 

	
	ToStore.In.inputMethod = None
	ToStore.In.noiseMethod = None
	ToStore.In.actFunction = None
	
	if ToStore.InCa1 != None:
		if ToStore.InCa1.n_lec != 0:
			ToStore.InCa1.Lec.inputMethod = None
			ToStore.InCa1.Lec.noiseMethod = None
			ToStore.InCa1.Lec.actFunction = None
	
	
	ToStore.Ca1Activation = None
	ToStore.Ca3Activation = None
	ToStore.In = None
	ToStore.InCa1 = None
	save(ToStore, title,path)
	
def saveHippoEcCa3Weights(hippo = None, title= None, path ='/home/torsten/Documents/simulations/' ):
	ToStore = copy.copy(hippo)
	
	ToStore.incrementalLearnMethod = None
	ToStore.initMethod = None
	ToStore.actFunctionsRegions = None
	ToStore.init_ca3_ca1 = None
	
	if ToStore.Ec_Dg != None:
		ToStore.Ec_Dg.getOutput = None
		ToStore.Dg_Ca3.getOutput = None
	ToStore.Ec_Ca1.getOutput = None
	ToStore.Ec_Ca3.getOutput = None
	ToStore.Ca3_Ca1.getOutput = None 
	ToStore.Ca3_Ca3.getOutput = None 
	ToStore.Ca1_Ec.getOutput = None 
	
	if ToStore.Ec_Dg != None:
		ToStore.Ec_Dg.weights = None
		ToStore.Dg_Ca3.weights = None
	ToStore.Ec_Ca1.weights = None

	ToStore.Ca3_Ca1.weights = None 
	ToStore.Ca3_Ca3.weights = None 
	ToStore.Ca1_Ec.weights = None 

	ToStore.In.inputMethod = None
	ToStore.In.noiseMethod = None
	ToStore.In.actFunction = None
	
	
	if ToStore.InCa1 != None:
		if ToStore.InCa1.n_lec != 0:
			ToStore.InCa1.Lec.inputMethod = None
			ToStore.InCa1.Lec.noiseMethod = None
			ToStore.InCa1.Lec.actFunction = None
	
	ToStore.Ca1Activation = None
	ToStore.Ca3Activation = None
	ToStore.In = None
	
	save(ToStore, title,path)
	
def saveHippoWeights(hippo = None, title= None, path ='/home/torsten/Documents/simulations/' ):
	
	ToStore = copy.copy(hippo)
	
	ToStore.incrementalLearnMethod = None
	ToStore.initMethod = None
	ToStore.actFunctionsRegions = None
	ToStore.init_ca3_ca1 = None
	
	if ToStore.Ec_Dg != None:
		ToStore.Ec_Dg.getOutput = None
		ToStore.Dg_Ca3.getOutput = None
	ToStore.Ec_Ca1.getOutput = None
	ToStore.Ec_Ca3.getOutput = None
	ToStore.Ca3_Ca1.getOutput = None 
	ToStore.Ca3_Ca3.getOutput = None 
	ToStore.Ca1_Ec.getOutput = None 
	
	if ToStore.Ec_Dg != None:
		ToStore.Ec_Dg.weights = None
		ToStore.Dg_Ca3.weights = None

	ToStore.In.inputMethod = None
	ToStore.In.noiseMethod = None
	ToStore.In.actFunction = None
	
	if ToStore.InCa1 != None:
		if ToStore.InCa1.n_lec != 0:
			ToStore.InCa1.Lec.inputMethod = None
			ToStore.InCa1.Lec.noiseMethod = None
			ToStore.InCa1.Lec.actFunction = None
	
	
	ToStore.Ca1Activation = None
	ToStore.Ca3Activation = None
	ToStore.In = None
	
	save(ToStore, title,path)
	
def saveEcCa1Ec(hippo = None, title= None, path ='/home/torsten/Documents/simulations/' ):
	ToStore = copy.copy(hippo)
	
	ToStore.incrementalLearnMethod = None
	ToStore.initMethod = None
	ToStore.actFunctionsRegions = None
	ToStore.init_ca3_ca1 = None
	
	ToStore.Ec_Ca1.getOutput = None
	ToStore.Ec_Ca3.getOutput = None
	ToStore.Ca3_Ca1.getOutput = None 
	ToStore.Ca1_Ec.getOutput = None 
	
	ToStore.Ec_Ca1.weights = None
	ToStore.Ec_Ca3.weights = None
	ToStore.Ca3_Ca1.weights = None 
	ToStore.Ca1_Ec.weights = None 

	ToStore.In.inputMethod = None
	ToStore.In.noiseMethod = None
	ToStore.In.actFunction = None
	
	ToStore.In = None
	
	save(ToStore, title,path)
	
def saveEcCa1EcWeights(hippo = None, title= None, path ='/home/torsten/Documents/simulations/' ):
	ToStore = copy.copy(hippo)
	
	ToStore.incrementalLearnMethod = None
	ToStore.initMethod = None
	ToStore.actFunctionsRegions = None
	ToStore.init_ca3_ca1 = None
	
	ToStore.Ec_Ca1.getOutput = None
	ToStore.Ec_Ca3.getOutput = None
	ToStore.Ca3_Ca1.getOutput = None 
	ToStore.Ca1_Ec.getOutput = None 
	
	ToStore.In.inputMethod = None
	ToStore.In.noiseMethod = None
	ToStore.In.actFunction = None
	
	ToStore.In = None
	
	save(ToStore, title,path)
	

# to fasten up the simulations, exp(x) is calculated beforehand for many x values and stored in a list. When exp(x) has to be calculated later it is just looked up in the list
#list of arguments for them np.exp() are calculated
exp_list = np.arange(-50, 30, 0.001)
# list of calculated np.exp() for the arguemnts defined above 
exp = np.exp(exp_list)
def calcExp(x): #faster exp(x) function
	'''
	abstract
	substition for np.exp(x). Searches for x the argument (index) which is most similar as in exp_list and returns np.exp() for this index. This is much faster, since np.exp() is calculated only once for the arguments specified in exp_list
	''' 
	index = np.int32((x-exp_list[0])/0.001)
	index[index < 0] = 0
	index[index >= exp_list.shape[0]] = exp_list.shape[0] -1
	return exp[index]



#####################################################################Networks########################################################################################################
class Network(object): # Generic network class 
	
	'''
	Generic network
	
	:param input_cells: number of input cells
	:type input_cells: int
	:param connectivity: proportion of input cells to which one outputcell is connected to
	:type connectivity: float in [0,1]
	:param learnrate: factor used for learning each pattern
	:type learnrate: float
	:param subtract_input_mean: determines whether input mean is subtracted by applying Hebbian learning+
	:param subtract_input_mean: bool
	:param subtract_output_mean: determines whether output mean is subtracted by applying Hebbian learning+
	:param subtract_output_mean: bool
	:param actFunction: How output is computed; e.g. activation Function
	:type actFunction: Network.getOutput method
	:param number_winner: number of firing neurons in the output in one pattern if actFunction is a WTA function
	:type number_winner: int
	:param e_max: Parameter for getOutput function ' getOutputEMax'. Determines activity threshold.
	:type weight_mean: float
	:param active_in_env: number of cells that are allowed to be active in one environment; if None all cells can fire in all environments
	:type active_in_env: int in [0, self.cells]
	:param n_e: Number of environments; only necessary if active_in_environnment != None
	:type n_e: int
	:param initMethod: How the weights are initialized
	:type initMethod: makeWeights method in :class:`Network`
	:param weight_sparsity: parameter needed of weight initMethod 'Network.makeWeightsSparsity'
	:type weight_sparsity: float in [0,1]
	:param weght_mean: Parameter for weight init function ' makeWeightsNormalDistributed'. Determines mean of the normal distribution how weights are initialized
	:type weight_mean: float
	:param weght_sigma: Parameter for weight init function ' makeWeightsNormalDistributed'. Determines sigma of the normal distribution how weights are initialized
	:type weight_mean: float
	'''
	
	
	def __init__(self, input_cells=None, cells=None, connectivity = None, learnrate= None, subtract_input_mean = None, subtract_output_mean = None, actFunction = None, number_winner=None, e_max = 1, active_in_env = None, n_e = 1, initMethod = None, weight_sparsity = None, weight_mean = 1, weight_sigma = 0.5):
		 

		self.no_presynapses = int(connectivity*input_cells)
		self.learnrate = learnrate
		self.number_winner = number_winner
		self.sparsity = self.number_winner/(cells*1.)
		self.cells = cells
		self.input_cells = input_cells
		self.subtract_input_mean = subtract_input_mean
		self.subtract_output_mean = subtract_output_mean
		self.getOutput = actFunction
		self.n_e = n_e
		self.e_max = e_max		
		if active_in_env == None:
			active_in_env = self.cells
		self.active_in_env = active_in_env
		
		self.initActiveCells()
		self.initConnection()
		initMethod(self, sparsity=weight_sparsity, mean = weight_mean, sigma = weight_sigma)
		
		self.Cor = {} #Dictionarry of Corelation instances constisting of input and outputstatistics
		self.output_stored = None #stored output pattern
		self.noisy_output = None #reconstructed stored output when noisy input was given 



	####Initialize the connectivity matrix
	def initConnection(self):
		self.connection = np.zeros([self.cells, self.input_cells], 'bool') # connection matrix; [i,j] = 1 iff there is a connection from j to i, otherwise 0
		self.connection[np.mgrid[0:self.cells, 0:self.no_presynapses][0], np.array(map(random.sample, [range(self.input_cells)]*self.cells,  [self.no_presynapses]*self.cells))] = 1
	
	###Determines which cells can be active in the environments
	def initActiveCells(self):
		self.active_cells = np.zeros([self.n_e, self.active_in_env], 'int')
		self.active_cells_vector = np.zeros([self.n_e, self.cells], 'bool')

		for h in range(self.n_e):
				self.active_cells[h] = np.array(random.sample(range(self.cells), self.active_in_env))
				self.active_cells_vector[h][self.active_cells[h]]=1

	#Methods for initializing the weights##################################################################
	def makeWeightsZero(self, **kwargs):
		'''
		
		init weights as zero
		'''
		self.weights = np.zeros(self.connection.shape)
	
	def makeWeightsUniformDistributed(self,**kwargs):
		'''
		
		init weighs created over uniform distribution between [0,1] and normalize them;
		'''
		self.weights = np.random.uniform(0,1, self.connection.shape)
		self.weights*=self.connection
		normalize(self.weights)
		
	def makeWeightsNormalDistributed(self, mean = None, sigma = None, **kwargs):
		'''
		
		init weighs created due to normal distribution with mean = mean and sigma = sigma. Finally, weights are normalized
	
		:param mean: Mean of distribution
		:type mean: float
		:param sigma: sigma of distribution
		:type sigma: float 
		'''
		self.weights = np.random.normal(loc = mean, scale =sigma, size = self.connection.shape)
		self.weights*=self.connection
		normalize(self.weights)
		
	def makeWeightsOne(self, **kwargs):
		'''
		
		init weights as one and normalize them
		'''
		self.weights = np.ones(self.connection.shape)
		self.weights *= self.connection
		normalize(self.weights)

	def makeWeightsSparsity(self, sparsity=0.05, **kwargs):
		'''
		
		init weights as 0 or 1 and normalize them
		
		:param sparsity: proportion of ones in one incomming weight vector of one cell. This proportion cannot be higher as self.connectivity
		:type sparsity: float in (0,1]
		'''
		self.weights = np.zeros(self.connection.shape)
		active_units = np.int(sparsity*self.no_presynapses)
		for row in range(self.connection.shape[0]):
			make_active = random.sample(np.nonzero(self.connection[row])[0], active_units) #which existing weights are set to 1
			self.weights[row][make_active] = 1
		self.weights*=self.connection
		normalize(self.weights)

		
	##### Learning Methods ##################################################
	# These methods learn patterns by adjusting self.weights 
	def hebbianLearning(self,input_pattern = None, output_pattern = None, learnrate = None):
		'''
		
		adjusts weights according to the standard hebbian rule wij = k*p_i*q_j, k = learnfactor = self.learnrate; if self.subtract_input_mean, input_mean is subtracted from input before learning; similar if self.subtract_output_mean
	
		:param input_pattern: input to associate
		:type input_pattern: array of max 3 dimensions, last one must be self.connection.shape[1]
		:param output_pattern: output to associate
		:type output_pattern: array of max 3 dimensions, last one must be self.connection.shape[0]
		'''
		if learnrate == None:
			learnrate = self.learnrate
		if learnrate != 0:
			input_scaled = np.copy(input_pattern)*1.0
			output_scaled = np.copy(output_pattern)*1.0
	
			if len(input_pattern.shape) == 1:
				self.weights += learnrate * np.einsum('j,i,ij->ij', input_scaled, output_scaled, self.connection)
			if len(input_pattern.shape) == 2: #(pattern,cell)
				if self.subtract_input_mean:
					input_mean =np.einsum('pi->i',input_scaled)/(input_scaled.shape[0]+0.0)
					input_mean2 =np.sum(input_scaled, axis = -2)/(input_scaled.shape[0]+0.0)
					print "newput mean correct?"
					print (input_mean == input_mean2) 
					
					input_scaled -= input_mean
				if self.subtract_output_mean:
					output_mean =np.einsum('pi->i',output_scaled)/(output_scaled.shape[0]+0.0)
					output_scaled -= output_mean
				self.weights += learnrate * np.einsum('pj,pi,ij->ij', input_scaled, output_scaled, self.connection)
				w2 = np.tensordot(output_scaled, input_scaled, (-2,-2)).reshape(self.weights.shape) *self.connection
				print 'w=w2 in hebb learning ? Change it!!!!!!!'
				print self.weights == w2
				
			if len(input_pattern.shape) == 3:#(environment, pattern, cell)
				if self.subtract_input_mean:
					#input_mean =np.einsum('epi->i',input_scaled)/(input_pattern.shape[0]*input_pattern.shape[1]+0.0)
					input_mean = np.sum(np.sum(input_scaled, 0), 0)/(input_pattern.shape[0]*input_pattern.shape[1]+0.0)
					input_scaled -= input_mean
				if self.subtract_output_mean:
					output_mean =np.einsum('epi->i',output_scaled)/(output_pattern.shape[0]*output_pattern.shape[1]+0.0)
					output_scaled -= output_mean
				print "adjust weights"
				if input_scaled.shape[0] >1:
					self.weights += learnrate * np.einsum('epj,epi,ij->ij', input_scaled, output_scaled, self.connection)
				else: #faster
					self.weights = np.tensordot(output_scaled, input_scaled, (-2,-2)).reshape(self.weights.shape) *self.connection
	
	def learnRegression(self,input_pattern = None, output_pattern = None, key = 'StoredStored'):
		
		'''
		
		learns weight by using linear regression between input pattern and outputpattern
	
		:param input_pattern: input
		:type input_pattern: array of max 3 dimensions, last one must be self.connection.shape[1]
		:param output_pattern: output
		:type output_pattern: array of max 3 dimensions, last one must be self.connection.shape[0]
		'''
		
		if (self.connection == 0).any():
			self.weights = self.calcRegressionNoFullConnectivity(input_pattern = input_pattern, output_pattern = output_pattern)
		else:
			self.weights = self.calcRegression(input_pattern = input_pattern, output_pattern = output_pattern)

		self.output_stored = output_pattern
		self.input_stored = input_pattern
		self.Cor[key] = Corelations(patterns_1 = self.output_stored, patterns_2 = np.tile(self.output_stored, (2,1,1)))
	
	def makeWeightsPattern(self, **kwargs):
		
		'''
		
		stores pattern by setting incomming weights to patterns that are going to be stored. If possible each pattern occures number_winner times in the set of weights
		'''
		no_pattern = max(int(self.cells/self.number_winner), self.input_stored.shape[1])# If possible each pattern occures number_winner times in the set of weights
		self.weights = np.zeros(self.weights.shape)
		for i in range(no_pattern):
			self.weights[i*self.number_winner : (i+1)*self.number_winner] = np.tile(self.input_stored[0][i], (self.number_winner, 1))
		self.weights *= self.connection
		normalize(self.weights)
		self.output_stored = self.getOutputWTALinear(input_pattern = self.input_stored)
	
	
	#####################actFunction:############################################
	def calcActivity(self,input_pattern=None): 
		'''
		
		calculates the output activity
		
		:param input_pattern: input_pattern
		:type input_pattern: numpy.array of arbritrary dimension
		:param return: Activity in the output
		:type return: numpy array. of same dimension as input_pattern, only last dimension differ if number of input cells is different to number of output cells.
		'''
		activity = np.tensordot(input_pattern, self.weights, (-1,-1))
		return activity
	
	def getOutputWTA(self,input_pattern=None, env = None, **kwargs): #returns the firing of the network given the input patterns; 
		'''
		
		calculates outputfiring  given input_pattern; the highest self.number_winner activated neurons fire; firing rate is either 1 or 0; Only cells that are allowed to be active in the enviroment are considered.
		
		:param input_pattern: input
		:type input_pattern: array of max 4 dimension, last one must be self.connection.shape[1]. Dimesnions are (environments, noise_level, patterns, cells)
		:param env: specifies current enviromnent if input dimension = 1 
		:param return: firing of the outputcells 
		:type return: array
		'''
		size = list(np.shape(input_pattern)) #dimension of input
		size[-1] = self.weights.shape[0] # change to dimension of output
		
		#set activity of those cells to zero, that are not allowed to be active in the environment
		if len(size) == 1:
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector[env]
		if len(size) == 2: #env, pattern--- not possible; use len(size) = 1 instead and specify env.
			print 'len 2 not possible getoutput wta'
		if len(size) == 3:#env, pattern, cell
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector.repeat(size[1], axis = 0).reshape(size)
		else: #env, noise, pattern, cell
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector.repeat(size[1]*size[2], axis = 0).reshape(size)
		

		winner = np.argsort(activity)[...,-self.number_winner:size[-1]]

		fire_rate = np.ones(size, 'bool')
		out_fire = np.zeros(size, 'bool')

			
		if len(size) ==1:#pattern
			out_fire[winner] = fire_rate[winner]
		if len(size) ==2:#env, pattern--- not possible
			out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
		if len(size) ==3: #env, pattern, cells
			indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
			out_fire[indices[0], indices[1], winner] =fire_rate[indices[0], indices[1], winner]
		if len(size) ==4: # env, noise, time, pattern
			indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
			out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
		if len(size) > 4:
				print 'error in input dimension in calckWTAOutput'
		return out_fire
	
	def getOutputWTALinear(self,input_pattern=None, env = 0, **kwargs): #returns the firing of the network given the input patterns; output is the activity of the winners
		'''
		
		Same as getOutputWTA, but now outputfiring is equal to activity of cell
		'''
		
		size = list(np.shape(input_pattern)) #dimension of input
		size[-1] = self.weights.shape[0] # change to dimension of output

		
		if len(size) == 1:#cells
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector[env]
		if len(size) == 2:
			print 'len 2 not possible getoutput wta'
		if len(size) == 3:#env, pattern, cells
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector.repeat(size[1], axis = 0).reshape(size)
		if len(size) == 4:#env, noise, pattern,cells
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector.repeat(size[1]*size[2], axis = 0).reshape(size)
		

		winner = np.argsort(activity)[...,-self.number_winner:size[-1]]

		fire_rate = activity # activities < 0 are set to 0
		fire_rate[fire_rate< 0] = 0
		out_fire = np.zeros(size)

		if len(size) ==1:

			out_fire[winner] = fire_rate[winner]
		if len(size) ==2:
			out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
		if len(size) ==3:
			indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
			out_fire[indices[0], indices[1], winner] =fire_rate[indices[0], indices[1], winner]
		if len(size) ==4: # env, noise, time, pattern
			indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
			out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
		if len(size) > 4:
				print 'error in input dimension in calckWTAOutput'
		#normalize(out_fire)
		return out_fire
	
	def getOutputSign(self,input_pattern=None, **kwargs): #returns the firing of the network given the input patterns; output is binary, all cells with activity above 0 fire, the others do not.

		'''
		
		calculates outputfiring  given input_pattern; neuron fires if activation >=0;
	
		:param input_pattern: input
		:param return: firing of the outputcells;
		:type return: array
		'''
		activity = self.calcActivity(input_pattern=input_pattern)
		size = list(np.shape(input_pattern)) #dimension of input
		size[-1] = self.weights.shape[0] # change to dimension of output
		fire = np.zeros(size)
		fire[np.nonzero(activity > 0)] = 1
		return fire
	
	def getOutputId(self, input_pattern=None, **kwargs):
		'''
		
		calculates outputfiring  given input_pattern; Firing rate of all neurons is equal to their activity level
	
		:param input_pattern: input
		:param return: firing of the outputcells;
		:type return: array
		'''
		activity = self.calcActivity(input_pattern = input_pattern)
		return activity
	
	def getOutputEMax(self, input_pattern = None, env = 0, e_max = None,**kwargs):
		
		'''
		
		calculates outputfiring  given input_pattern; EMax activation function is used. Cells that are within (1-emax) times the maximal activation of one cell in that pattern fire, the others not. Firing rate is activation level
	
		:param input_pattern: input
		:param return: firing of the outputcells;
		:type return: array
		'''
		
		if e_max == None:
			e_max = self.e_max
		size = list(np.shape(input_pattern)) #dimension of input

		size[-1] = self.weights.shape[0] # change to dimension of output
		if len(size) == 1:#cells
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector[env]

		if len(size) == 2:
			print 'len 2 not possible getoutput wta'
		if len(size) == 3:#env, pattern, cells
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector.repeat(size[1], axis = 0).reshape(size)
		if len(size) == 4:#env, noise, pattern,cells
			activity = self.calcActivity(input_pattern=input_pattern) * self.active_cells_vector.repeat(size[1]*size[2], axis = 0).reshape(size)
			
		if e_max < 1:
			min_activity = (np.max(activity, -1) * (1-e_max)).repeat(activity.shape[-1]).reshape(activity.shape)#max act in one pattern *emax = min act one cell must have to be active
			activity[activity < min_activity] = 0
		return activity
		
	def getOutputLinearthreshold(self,input_pattern=None, env = None, **kwargs):
		
		'''
		
		calculates outputfiring  given input_pattern; Firing is calcualted accoriding to Rolls 1995 activation function. A threshold is determined for each pattern individually to assure sparsity level.
	
		:param input_pattern: input
		:param return: firing of the outputcells; silent cells are 0 
		:type return: array
		'''
		
		activity = self.calcActivity(input_pattern = input_pattern)		
		activity[activity <= 0] = 0
		for k in np.linspace(np.min(activity),np.min(np.max(activity, -1)), 100):
			if k>=np.max(activity):
				if len(input_pattern.shape) >= 2:
					print 'break precalc since otherwise all 0'
				break
			activity_help = np.copy(activity)
			activity_help[activity_help <= k] = 0
			a = Network.calcFireSparsity(activity_help)
			if (a < self.sparsity).any():
				if len(input_pattern.shape) >= 2:
					print 'break precalc linthreshold output at k = '+str(k)
				break
			else:
				activity = activity_help
		
		
		if len(input_pattern.shape) >= 2:
			
			print 'calc Output Hetero Linthreshold'
			a = self.calcFireSparsity(activity)
			not_sparse = True
			i=0
			while not_sparse:
				if (i/100.) == int(i/100):
					print i
				too_large_a_rows = a > self.sparsity
				if not too_large_a_rows.any():
					not_sparse = False
					print 'number cells reduced to zero'
					print i
					break
				activity[activity == 0] = 10**15
				min_activity_cell = np.argmin(activity, -1) 
				activity[too_large_a_rows, min_activity_cell[too_large_a_rows]] = 0
				activity[activity == 10**15] = 0
				a = self.calcFireSparsity(activity)
				i+=1
			normalize(activity)
				
		if len(input_pattern.shape) == 1:
			activity = self.calcActivity(input_pattern = input_pattern)
			a = self.calcFireSparsity(activity)
			not_sparse = True
			i=0
		
			while a > self.sparsity:
				min_activity = np.min(activity[activity !=0])
				activity[activity == min_activity] = 0
				a = self.calcFireSparsity(activity)
				i+=1
			normalize(activity)
		out_fire = activity
			
		
		return activity
	

			
	########################### Linear Regression help methods #############################################################################
	def calcRegression(self,input_pattern = None, output_pattern = None): #learn linear map from input pattern to output pattern
		print 'Full used______________________________'
		weights = np.zeros(self.weights.shape)
		#####np.lstsq solves b = ax. In our case input_pattern = output_pattern * weights.T
		if len(input_pattern.shape) <=2:
			weights = np.linalg.lstsq(input_pattern, output_pattern)[0].T
		if len(input_pattern.shape) ==3:
			weights = np.linalg.lstsq(input_pattern.reshape(input_pattern.shape[0]*input_pattern.shape[1], input_pattern.shape[2]), output_pattern.reshape(output_pattern.shape[0]*output_pattern.shape[1],output_pattern.shape[2]))[0].T
		if len(input_pattern.shape) >3:
			print 'too much dim in learn regression'
		return weights
	
	def calcRegressionNoFullConnectivity(self,input_pattern = None, output_pattern = None): #learn linear map from input pattern to output pattern without full connectivity
		weights = np.zeros(self.weights.shape)
		print 'no full used_____________________________________________'
		#####np.lstsq solves b = ax. In our case output_pattern = input_pattern * weights.T
		if len(input_pattern.shape) <=2:
			for i in range(output_pattern.shape[-1]):
				weights[i][np.flatnonzero(self.connection[i]==1)] = np.linalg.lstsq(input_pattern[:, np.flatnonzero(self.connection[:,i]==1)], output_pattern[:,i])[0].T
		if len(input_pattern.shape) ==3:
			input_p = input_pattern
			for i in range(output_pattern.shape[-1]):
				weights[i][np.flatnonzero(self.connection[i]==1)] = np.linalg.lstsq(input_p[0][:, np.flatnonzero(self.connection[i]==1)], output_pattern[0][:,i])[0]
		if len(input_pattern.shape) >3:
			print 'too much dim in learn regression'
		return weights
		
	def calcRegressionOutput(self,input_pattern = None, output_pattern = None, x = None, **kwargs):
		
		'''
		
		calculates outputfiring  given x as input; Output firing is computed after linear regression is applied on input and output. Linear regression is not learned here in the weights
	
		:param input_pattern: input
		:param return: firing of the outputcells;
		:type return: array
		'''
		if (self.connection == 0).any():
			weights = self.calcRegressionNoFullConnectivity(input_pattern = input_pattern, output_pattern = output_pattern)
		else:
			weights = self.calcRegression(input_pattern = input_pattern, output_pattern = output_pattern)
		return np.tensordot(x, weights, (-1,-1))
	
	def calcDifftoLinReg(self): #return reg(mean(p)) - 2*,mean(q)
		input_mean =np.einsum('epi -> i', self.input_stored)/(self.input_stored.shape[0]*self.input_stored.shape[1]+0.0)
		output_mean =np.einsum('epi->i',self.output_stored)/(self.output_stored.shape[0]*self.output_stored.shape[1]+0.0)
		diff = self.calcRegressionOutput(input_pattern = self.input_stored - input_mean, output_pattern = self.output_stored, x = input_mean) #- 2* output_mean
		return diff

	def printExpectedSparsity(self): 

		input_mean =np.sum(np.sum(self.input_stored, 0),0)/(self.input_stored.shape[0]*self.input_stored.shape[1]+0.0)
		diff = self.calcDifftoLinReg()
		q_hat = self.calcRegressionOutput(input_pattern = self.input_stored-input_mean, output_pattern = self.output_stored, x =self.input_stored )
		sparsity = self.calcFireSparsity(patterns = q_hat +diff)

		
		
		
	######################## Recall Function #########################################
	def recall(self, input_pattern = None, key = '', first = None):
		
		'''
		
		calculates output given input cues and creates Correlation Classes comparing stored and recalled patterns and recalled with recalled ones.
	
		:param input_pattern: input
		:type input_pattern: array
		:param first: if first != None, only first 'first' patterns are considered for analysis (if first >0 ). If first <0 only last stored patterns are considered. 
		:type first: integer
		'''
		
		if first == None:
			first = input_pattern.shape[-2]	
			
		if self.output_stored == None: #if nothing is stored
			self.output_stored = np.zeros([1,1,first])
		
		if first >=0 :
			input_pattern = input_pattern[:,:,:first]
			self.noisy_output = self.getOutput(self,input_pattern)
			self.Cor['StoredRecalled'+key] = Corelations(patterns_1 = self.output_stored[:,:first], patterns_2 = self.noisy_output)
			self.Cor['RecalledRecalled'+key] = Corelations(patterns_1 =self.noisy_output[:,0], patterns_2 = self.noisy_output)
		
		if first < 0:
			input_pattern = input_pattern[:,:,first:]
			self.noisy_output = self.getOutput(self,input_pattern)
			self.Cor['StoredRecalled'+key] = Corelations(patterns_1 = self.output_stored[:,first:], patterns_2 = self.noisy_output)
			self.Cor['RecalledRecalled'+key] = Corelations(patterns_1 =self.noisy_output[:,0], patterns_2 = self.noisy_output)

	@classmethod
	def calcFireSparsity(cls, patterns = None):#patterns 0 (loc, pattern)
		
		'''
		
		Help function to determine sparsity threshold for getOutputLinearthreshold
		'''
		enumerator = (np.sum(patterns*1., axis = -1)/patterns.shape[-1])**2
		denominator = np.sum((patterns*1)**2, -1)/patterns.shape[-1]
		return enumerator/denominator
	
	def getActivationActiveSilentCells(self, input_pattern  = None, at_location = None):
		
		'''
		
		gets the activation of active (silent) cells during storage when the input is presented after learning; if at_location != None only at a specific stored location 
	
		:param input_pattern: input
		:param return: activity levels of active and silent cells
		:type return: list of arrays
		'''
		activity = self.calcActivity(input_pattern = input_pattern)
		if len(activity.shape) == 3:
			activity = activity.reshape(activity.shape[0]*activity.shape[1], activity.shape[2])
		if at_location == None: #all locations
			output_pattern = self.Cor['StoredStored'].patterns_1
			active_cells = np.nonzero(output_pattern)
			silent_cells = np.nonzero(output_pattern==0)
		else:
			output_pattern = self.Cor['StoredStored'].patterns_1[at_location]
			active_cells = np.nonzero(output_pattern)
			silent_cells = np.nonzero(output_pattern==0)
		return [activity[active_cells], activity[silent_cells]]
		
	def getSilentCellWithMaxActivity(self, input_pattern  = None, at_location = None):
		
		'''
		
		return cell index of cell that was silent duirng storage at location at_lolcation and has now highest activity given the input input_pattern
		'''
		
		activity = self.getActivationActiveSilentCells(input_pattern  = input_pattern, at_location = at_location)[1]
		arg_max = np.argmax(activity, axis = -1)
		return arg_max
		
	def getActiveCellWithMaxActivity(self, input_pattern  = None, at_location = None):
		'''
		
		return cell index of cell that was active duirng storage at location at_lolcation and has now highest activity given the input input_pattern
		'''
		activity = self.getActivationActiveSilentCells(input_pattern  = input_pattern, at_location = at_location)[0]
		arg_max = np.argmax(activity, axis = -1)
		return arg_max
		
	def getActiveCellWithMinActivity(self, input_pattern  = None, at_location = None):
		'''
		
		return cell index of cell that was active duirng storage at location at_lolcation and has now lowest activity given the input input_pattern
		'''
		activity = self.getActivationActiveSilentCells(input_pattern  = input_pattern, at_location = at_location)[0]
		arg_min = np.argmin(activity, axis = -1)
		return arg_min
		
class HeteroAssociation(Network):
	
	def learnAssociation(self,input_pattern = None, output_pattern = None, key = 'StoredStored', first = None): #Association of input pattern and output pattern
		'''
		
		hebbian association input with output; self.input_stored becomes input_pattern; self.output_stored becomes outputpattern. Creates Correlation Class self.Cor[key] that analsizes the stored output
		
		:param input_pattern: input to associate
		:type input_pattern: array of max 4 dimensions, last one must be self.connection.shape[1]
		:param output_pattern: output to associate
		:type output_pattern: array of max 4 dimensions, last one must be self.connection.shape[0]
		:param first: if first != None, only the first 'first' stored pattern are considered for analysis. If negative, only the last ones are considered. However all patterns are stored.
		:type first: integer
		'''

		self.hebbianLearning(input_pattern = input_pattern, output_pattern = output_pattern)
		self.output_stored = output_pattern
		self.input_stored = input_pattern
		if first == None:
			first = input_pattern.shape[-2]
		if first >= 0:
			self.Cor[key] = Corelations(patterns_1 = self.output_stored[:,:first])
		else:

			self.Cor[key] = Corelations(patterns_1 = self.output_stored[:,first:])

class AutoAssociation(HeteroAssociation):
	
	
	'''
	
	Network with recurrent dynamics. getOutputfunctions as in :class: 'Network' but now activation cycles are possible with external input clamped on.
	
	:param cycles: Number of activation cycles. In one cycle all neurons are updated synchronously.
	:type cycles: int
	:param external_force: Determines influence of external input during dynamics; if 0 no clapmed external input is considered.
	:type external_force: int
	:param internal_force: Determines influence of recurrent input during dynamics;
	:type internal_force: int
	:param external_weights: Weight matrix that connect external input to the network. Necessary when external_force != 0
	:type external weights: np.array of dimenstion (input_cells, cells)
	
	'''	

	def __init__(self, input_cells=None, cells=None, number_winner=None, connectivity = None, learnrate= None, subtract_input_mean = None, subtract_output_mean = None, initMethod = None, weight_sparsity = None, actFunction = None, weight_mean = None, weight_sigma = None, cycles = None, external_force = None, internal_force = None, external_weights = None, active_in_env = None, n_e = None):
		

		self.cycles = cycles
		self.external_force = external_force
		self.internal_force = internal_force
		self.external_weights = external_weights
		super(AutoAssociation, self).__init__(input_cells=input_cells, cells=cells, number_winner=number_winner, connectivity = connectivity, learnrate= learnrate, subtract_input_mean = subtract_input_mean, subtract_output_mean = subtract_output_mean, initMethod = initMethod, weight_sparsity = weight_sparsity, actFunction = actFunction, active_in_env = active_in_env, n_e = n_e, weight_mean = weight_mean, weight_sigma = weight_sigma)
	
	
	
	
	### Helper Function; returns activity that arrives externally
	def calcExternalActivity(self, external_pattern = None):
		activity = np.tensordot(external_pattern, self.external_weights, (-1,-1))
		return activity

	##### Output Function ################
	#As in Network class, but now implemented with recurrent dynamics
	def getOutputWTA(self,input_pattern=None, external_activity = None, env = None): #returns the winner of the network given the input patterns; if not discrete, output is the membrane potential of the winners
		'''
		
		calculates outputfiring  given input_pattern; the highest self.number_winner activated neurons fire;
	
		:param input_pattern: input
		:type input_pattern: array of max 4 dimension, last one must be self.connection.shape[1]
		:param return: firing of the outputcells 
		:type return:  array possible dimensions (env, noise, pattern, cellfire)
		'''		
		
		def calcOutput(internal_pattern = None, external_activity = None, env = None):

			internal_activity = self.calcActivity(input_pattern=internal_pattern)
			normalize(internal_activity)

			size = list(np.shape(input_pattern)) #dimension of input
			size[-1] = self.weights.shape[0] # change to dimension of output
			if len(size) == 1:
				activity = (internal_activity*self.internal_force  + external_activity*self.external_force) * self.active_cells_vector[env]
			if len(size) == 2:
				print 'len 2 not possible getoutput wta'
			if len(size) == 3:
				activity = (internal_activity*self.internal_force  + external_activity*self.external_force) * self.active_cells_vector.repeat(size[1], axis = 0).reshape(size)
			else:
				activity = (internal_activity*self.internal_force  + external_activity*self.external_force) * self.active_cells_vector.repeat(size[1]*size[2], axis = 0).reshape(size)
		

			winner = np.argsort(activity)[...,-self.number_winner:size[-1]]
	
			fire_rate = np.ones(size, 'bool')
			out_fire = np.zeros(size, 'bool')
				
			if len(size) ==1:
				out_fire[winner] = fire_rate[winner]
			if len(size) ==2:
				out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
			if len(size) ==3:
				indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
				out_fire[indices[0], indices[1], winner] =fire_rate[indices[0], indices[1], winner]
			if len(size) ==4: # env, noise, time, pattern
				indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
				out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
			if len(size) > 4:
					print 'error in input dimension in calckWTAOutput'
			return out_fire


		if external_activity != None: 
			normalize(external_activity)
			out_fire = calcOutput(internal_pattern = input_pattern, external_activity = external_activity, env = env)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = calcOutput(internal_pattern =out_fire, external_activity = external_activity, env = env)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
				if c == self.cycles -1:
					print 'all ' +str(c) +' cycles used'
			
		else: #rec dynamics without consistent input form the outside
			out_fire = super(AutoAssociation, self).getOutputWTA(input_pattern=input_pattern, env = env)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = super(AutoAssociation, self).getOutputWTA(input_pattern=out_fire, env = env)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
		normalize(out_fire)
		return out_fire
	
	def getOutputWTALinear(self,input_pattern=None, external_activity = None, env = None): #returns the winner of the network given the input patterns; if not discrete, output is the membrane potential of the winners
		'''
		
		calculates outputfiring  given input_pattern; the highest self.number_winner activated neurons fire;
	
		:param input_pattern: input
		:type input_pattern: array of max 4 dimension, last one must be self.connection.shape[1]
		:param return: firing of the outputcells 
		:type return: array possible dimensions (env, noise, pattern, cellfire)
		'''		
		
		def calcOutput(internal_pattern = None, external_activity = None, env = None):
			
			internal_activity = self.calcActivity(input_pattern=internal_pattern)
			normalize(internal_activity)
		
			#activity = (internal_activity*self.internal_force  + external_activity*self.external_force)* self.active_cells_vector
			
			size = list(np.shape(input_pattern)) #dimension of input
			size[-1] = self.weights.shape[0] # change to dimension of output
			if len(size) == 1:
				activity = (internal_activity*self.internal_force  + external_activity*self.external_force) * self.active_cells_vector[env]
			if len(size) == 2:
				print 'len 2 not possible getoutput wta'
			if len(size) == 3:
				activity = (internal_activity*self.internal_force  + external_activity*self.external_force) * self.active_cells_vector.repeat(size[1], axis = 0).reshape(size)
			else:
				activity = (internal_activity*self.internal_force  + external_activity*self.external_force) * self.active_cells_vector.repeat(size[1]*size[2], axis = 0).reshape(size)
		

			winner = np.argsort(activity)[...,-self.number_winner:size[-1]]
	
			activity[activity<0] = 0
			fire_rate = activity
			out_fire = np.zeros(size, 'bool')

				
			if len(size) ==1:
				out_fire[winner] = fire_rate[winner]
			if len(size) ==2:
				out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
			if len(size) ==3:
				indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
				out_fire[indices[0], indices[1], winner] = fire_rate[indices[0], indices[1], winner]
			if len(size) ==4: # env, noise, time, pattern
				indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
				out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
			if len(size) > 4:
					print 'error in input dimension in calckWTAOutput'
			return out_fire

		

		if external_activity != None: 
			normalize(external_activity)

			out_fire = calcOutput(internal_pattern = input_pattern, external_activity = external_activity, env = env)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = calcOutput(internal_pattern =out_fire, external_activity = external_activity, env = env)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
				if c == self.cycles -1:
					print 'all ' +str(c) +' cycles used'
			return out_fire
			
		else: #rec dynamics without consistent input form the outside
			out_fire = super(AutoAssociation, self).getOutputWTA(input_pattern=input_pattern, discrete = discrete, env = env)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = super(AutoAssociation, self).getOutputWTA(input_pattern=out_fire, discrete = discrete, env = env)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
			return out_fire
	
	def getOutputLinearthreshold(self,input_pattern=None, external_activity = None): #returns the winner of the network given the input patterns; if not discrete, output is the membrane potential of the winners
		'''
		
		calculates outputfiring  given input_pattern; the highest self.number_winner activated neurons fire;
	
		:param input_pattern: input
		:type input_pattern: array of max 4 dimension, last one must be self.connection.shape[1]
		:param return: firing of the outputcells 
		:type return: array possible dimensions (env, noise, pattern, cellfire)
		'''		
		
		def calcOutput(internal_pattern = None):
			
			internal_activity = self.calcActivity(input_pattern=internal_pattern)
			normalize(internal_activity)
				
			activity = self.internal_force * internal_activity + self.external_force * external_activity
		
			if len(internal_pattern.shape) >= 2:
				a = Network.calcFireSparsity(activity)
				not_sparse = True
				i=0
		
				while not_sparse:

					too_large_a_rows = a > self.sparsity
					if not too_large_a_rows.any():
						not_sparse = False


						break

					activity[activity == 0] = 10**5
					min_activity_cell = np.argmin(activity, -1) 
					activity[too_large_a_rows, min_activity_cell[too_large_a_rows]] = 0
					activity[activity == 10**5] = 0

					a = Network.calcFireSparsity(activity)
					i+=1
					#time.sleep(5)


				i=0

				normalize(activity)
				
			if len(internal_pattern.shape) == 1:
				a = Network.calcFireSparsity(activity)
				not_sparse = True
				i=0
		
				while a > self.sparsity:
					min_activity = np.min(activity[activity !=0])
					activity[activity == min_activity] = 0
					a = Network.calcFireSparsity(activity)
					i+=1

				normalize(activity)

			return activity

		

		if external_activity != None: 
			normalize(external_activity)
			
			out_fire = calcOutput(internal_pattern = input_pattern)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = calcOutput(internal_pattern =out_fire)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
				if c == self.cycles -1:
					print 'all ' +str(c) +' cycles used'
			return out_fire
			
		else: #rec dynamics without consistent input form the outside
			out_fire = super(AutoAssociation, self).getOutputLinearthreshold(input_pattern=input_pattern, discrete = discrete)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = super(AutoAssociation, self).getOutputLinearthreshold(input_pattern=out_fire, discrete = discrete)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
			return out_fire
	
	def getOutputId(self,  external_activity = None, input_pattern=None):
		
		
		def calcOutput(internal_pattern = None, external_activity = None):
			
			internal_activity = self.calcActivity(input_pattern=internal_pattern)
			normalize(internal_activity)
		
			activity = internal_activity*self.internal_force  + external_activity*self.external_force

			return activity

		

		if external_activity != None: 
			normalize(external_activity)

			out_fire = calcOutput(internal_pattern = input_pattern, external_activity = external_activity)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = calcOutput(internal_pattern =out_fire, external_activity = external_activity)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
				if c == self.cycles -1:
					print 'all ' +str(c) +' cycles used'
			return out_fire
			
		else: #rec dynamics without consistent input form the outside
			out_fire = super(AutoAssociation, self).getOutputWTA(input_pattern=input_pattern, discrete = discrete)
			for c in range(self.cycles):
				out_old = np.copy(out_fire)
				out_fire = super(AutoAssociation, self).getOutputWTA(input_pattern=out_fire, discrete = discrete)
				if (out_old == out_fire).all():
					print 'stop after ' + str(c) +' cycles'
					break
			return out_fire
	
		
		return activity
		
	
	#########Recall Function###########
	#As in Network; Now with additional external activity possible 
	def recall(self, input_pattern = None, external_activity = None, key = '', first = None):
		if first == None:
			first = input_pattern.shape[-2]
		if first >= 0:
			input_pattern = input_pattern[:,:,:first]
			self.noisy_output = self.getOutput(self,input_pattern = input_pattern, external_activity = external_activity[:,:,:first])
			self.Cor['StoredRecalled'+key] = Corelations(patterns_1 = self.output_stored[:,:first], patterns_2 = self.noisy_output)
			self.Cor['RecalledRecalled'+key] = Corelations(patterns_1 =self.noisy_output[:,0], patterns_2 = self.noisy_output)
		else:
			input_pattern = input_pattern[:,:,first:]
			self.noisy_output = self.getOutput(self,input_pattern = input_pattern, external_activity = external_activity[:,:,first:])
			self.Cor['StoredRecalled'+key] = Corelations(patterns_1 = self.output_stored[:,first:], patterns_2 = self.noisy_output)
			self.Cor['RecalledRecalled'+key] = Corelations(patterns_1 =self.noisy_output[:,0], patterns_2 = self.noisy_output)
		
class OneShoot(Network):

	'''
	
	Network where input first triggers output with existing weights and then weights are adjusted. Different to Association networks, where a pair of patterns is associated. 
	'''


	def __init__(self, **kwargs):
		

		super(OneShoot, self).__init__(**kwargs)
		self.co_factor = 1.0/self.cells # needed for neural gas; it decrases learning amount through time
	#one shoot learning methods
	
	def learnFiringDependent(self,input_pattern = None, output_pattern = None, cofactor_increase = None): # one shoot learning of patterns; hebbian association with input and outputfiring; outputpattern is opiional
		if output_pattern ==None:
			output_pattern = self.getOutput(self,input_pattern = input_pattern)
		self.weights += self.learnrate * np.einsum('j,i,ij->ij', input_pattern*1, output_pattern*1, self.connection)
		normalize(self.weights)
		
	def learnActivityDependent(self,input_pattern =None, output_pattern = None, cofactor_increase = 0): # one shoot version, here learning rate is dependent on output activity not firing; output_pattern is not needed
		activity = self.calcActivity(input_pattern = input_pattern)
		activity[np.argsort(activity)[:-self.number_winner]]=0
		self.hebbianLearning(input_pattern = input_pattern, output_pattern = activity)
		normalize(self.weights)
		
	def learnSiTreves(self,input_pattern =None, output_pattern = None, cofactor_increase = None): 

		#input_mean = np.sum(input_pattern)*1./input_pattern.shape[0]
		
	
		input_mean = (np.dot(self.connection, input_pattern)*1./np.sum(self.connection, axis = -1)).repeat(input_pattern.shape[0]).reshape(self.connection.shape[0], input_pattern.shape[0])
		input_p = np.tile(input_pattern, (self.connection.shape[0],1)) -input_mean
		self.weights += self.learnrate * np.einsum('ij,i,ij->ij', input_p, output_pattern*1, self.connection)
		self.weights[self.weights< 0] = 0
		normalize(self.weights)
		
		
	def learnOneShootAllPattern(self,input_pattern = None, method = None, key = 'StoredStored', first = None, store_output = 1): # one shoot learning of all patterns using giving method; output activity during learning is stored
		
		print 'one shoot learning all patterns'
		normalize(self.weights)
		
		if store_output:
			output = np.zeros([input_pattern.shape[0],input_pattern.shape[1], self.cells])
			if self.learnrate != 0:
				for env in range(input_pattern.shape[0]):
					for p in range(input_pattern.shape[1]):
						output[env,p] = self.getOutput(self,input_pattern = input_pattern[env, p], env = env)
						method(self,input_pattern = input_pattern[env,p], output_pattern = output[env,p])
			else:
				output = self.getOutput(self,input_pattern = input_pattern)
			self.output_stored = output
			self.input_stored = input_pattern
			if first == None:
				first = input_pattern.shape[-2]
			if first >= 0:
				self.Cor[key] = Corelations(patterns_1 = self.output_stored[:,:first])
			else:
				self.Cor[key] = Corelations(patterns_1 = self.output_stored[:,first:])
				
		else:
			for env in range(input_pattern.shape[0]):
				for p in range(input_pattern.shape[1]):
					method(self,input_pattern = input_pattern[env,p], output_pattern = self.getOutput(self,input_pattern = input_pattern[env, p], env = env))

	def learnAccordingToRank(self, input_pattern = None, output_pattern = None, cofactor_increase = 0): # outputpattern not needed
		
		activity = self.calcActivity(input_pattern = input_pattern)
		activity_sort = np.argsort(activity)
		learnrate = calcExp(-self.co_factor*(np.arange(self.cells)))[activity_sort]
		self.weights += np.outer(learnrate, input_pattern) *self.connection
		normalize(self.weights)
		self.co_factor += cofactor_increase
				
class Incremental(OneShoot):

	
	'''
	
	OneShoot Network that learns input over several epochs 
	'''
	
	def __init__(self, input_cells=None, cells=None, number_winner=None, connectivity = None, learnrate= None, subtract_input_mean = None, subtract_output_mean = None,  initMethod = None, sparsity = None, storage_mode=None, actFunction = None, active_in_env = None, n_e = None,weight_mean = None, weight_sigma = None):
		

		OneShoot.__init__(self, input_cells = input_cells, cells = cells, number_winner = number_winner, connectivity = connectivity, learnrate = learnrate, subtract_input_mean = subtract_input_mean, subtract_output_mean = subtract_output_mean, initMethod = initMethod, sparsity = sparsity, actFunction = actFunction,active_in_env = active_in_env, n_e = n_e,weight_mean = weight_mean, weight_sigma = weight_sigma)
		self.storage_mode = storage_mode
		normalize(self.weights)
		
	
	def learnIncremental(self, input_pattern= None, input_pattern_to_store=None, no_times = None, number_to_store = None, method = None, cofactor_increase = 0, env = 0 ): # learns enviroment and determines the pattern to store in the input and its output firing either during learning (online) or after learning (offline)
			
			self.input_stored= np.zeros([self.n_e, number_to_store, self.input_cells])
			if input_pattern_to_store == None:
				self.input_stored[env] = input_pattern[env][np.array(random.sample(range(input_pattern.shape[1]), number_to_store))] # the inputs that are considered as stored
			else:
				self.input_stored[env] = input_pattern_to_store[env]
			self.output_stored = np.zeros([self.n_e,number_to_store, self.weights.shape[0]]) #the stored outputs; i.e. the activation of the stored inputs, at the time when the input is stored
			if self.storage_mode == 'online':
				print 'online'
				time_to_store = np.sort(random.sample(range(no_times), int((0.0+self.number_to_store))))# when pattern is stored; here randomly during learning phase
			else:
				time_to_store = [no_times+1] #output to store is computed after learning

			stored =0 #number of pattern stored yet
			for t in range(no_times):
				if t != time_to_store[stored]:
					method(self, input_pattern = input_pattern[env][np.random.randint(input_pattern.shape[1])], cofactor_increase = cofactor_increase) # learns randomly choosen input_pattern according to method
				else:
					self.output_stored[env][stored] = self.getOutput(self,input_pattern = self.input_stored[env][stored], env = env)
					method(self, input_pattern = self.input_stored[env][stored], output_pattern = self.output_stored[env][stored], cofactor_increase = cofactor_increase) # learns input_pattern to store according to method
					stored +=1
			if self.storage_mode == 'offline':
				self.output_stored[env] = self.getOutput(self,input_pattern = self.input_stored, env = env)
			

##################Paramter####################################################################
class Parameter():
	

	'''
	
	Parameter Class. If some paramters are not given in the Simulation, usually paramters of this class are used instead of raising an error.
	'''
	
	#Parameter
	no_pattern = 400
	number_to_store = 252
	n_e=1
	first  = None
	cells = dict(Ec = 1100, Dg = 12000, Ca3 =2500, Ca1 = 4200)#cell numbers of each region
	sparsity = dict(Ec = 0.35, Dg = 0.005, Ca3 = 0.032, Ca1 = 0.09)#activity level of each region (if WTA network)
	number_winner = dict(Ec = int(cells['Ec']*sparsity['Ec']), Dg = int(cells['Dg']*sparsity['Dg']), Ca3 = int(cells['Ca3']*sparsity['Ca3']), Ca1 = int(cells['Ca1']*sparsity['Ca1']) ) 
	connectivity = dict(Ec_Dg = 0.32, Dg_Ca3 = 0.0006, Ca3_Ec = 0.32, Ec_Ca3 =0.32, Ca3_Ca3 = 0.24, Ca3_Ca1 = 0.32, Ca1_Ec = 0.32, Ec_Ca1 = 0.32, Ca1_Sub = 0.32, Sub_Ec = 0.32, Ec_Sub = 0.32)# probability given cell is connected to given input cell
	learnrate = dict(Ec_Dg = 0.5, Dg_Ca3 = None, Ca3_Ec = 1, Ec_Ca3 =1, Ca3_Ca3=1, Ca3_Ca1 = 0.5, Ec_Ca1 = 1, Ca1_Ec = 1, Ca1_Sub = 1, Sub_Ec = 1, Ec_Sub = 0)
	initMethod = dict(Ec_Dg = Network.makeWeightsUniformDistributed, Dg_Ca3 = Network.makeWeightsUniformDistributed, Ec_Ca3 =Network.makeWeightsZero, Ca3_Ec =Network.makeWeightsZero, Ca3_Ca3 = Network.makeWeightsZero, Ec_Ca1 = Network.makeWeightsNormalDistributed, Ca3_Ca1 = Network.makeWeightsZero, Ca1_Ec =Network.makeWeightsZero)
	actFunctionsRegions = dict(Ec_Dg = Network.getOutputWTALinear, Dg_Ca3 = Network.getOutputWTA, Ca3_Ec = Network.getOutputWTALinear, Ec_Ca3 = Network.getOutputWTA, Ca3_Ca3 = AutoAssociation.getOutputWTA, Ca3_Ca1= Network.getOutputWTALinear, Ca1_Ec = Network.getOutputWTALinear, Ec_Ca1 = Network.getOutputWTALinear)
	
	rolls = 0#whether Parameter of rolls 1995 are used 
	if rolls:
		cells = dict(Ec = 600, Dg = 1000, Ca3 =1000, Ca1 = 1000)#cell numbers of each region
		sparsity = dict(Ec = 0.05, Dg = 0.05, Ca3 = 0.05, Ca1 = 0.01, Sub = 0.097)#activity level of each region (if WTA network)
		number_winner = dict(Ec = int(cells['Ec']*sparsity['Ec']), Dg = int(cells['Dg']*sparsity['Dg']), Ca3 = int(cells['Ca3']*sparsity['Ca3']), Ca1 = int(cells['Ca1']*sparsity['Ca1']) )
		connectivity = dict(Ec_Dg = 60./600, Dg_Ca3 = 4./1000, Ca3_Ec = 60./1000, Ec_Ca3 =120./600., Ca3_Ca3 = 200./1000, Ca3_Ca1 = 200./1000., Ca1_Ec = 1000./1000, Ec_Ca1 = 1, Ca1_Sub = 0.32, Sub_Ec = 0.32, Ec_Sub = 0.32)
		learnrate = dict(Ec_Dg = 0.5, Dg_Ca3 = None, Ca3_Ec = 1, Ec_Ca3 =1, Ca3_Ca3=1, Ca3_Ca1 = 10, Ec_Ca1 = 1, Ca1_Ec = 1, Ca1_Sub = 1, Sub_Ec = 1, Ec_Sub = 0)
		initMethod = dict(Ec_Dg = Network.makeWeightsUniformDistributed, Dg_Ca3 = Network.makeWeightsUniformDistributed, Ec_Ca3 =Network.makeWeightsZero, Ca3_Ec =Network.makeWeightsZero, Ca3_Ca3 = Network.makeWeightsZero, Ec_Ca1 = Network.makeWeightsNormalDistributed, Ca3_Ca1 = Network.makeWeightsZero, Ca1_Ec =Network.makeWeightsZero)
		actFunctionsRegions = dict(Ec_Dg = Network.getOutputLinearthreshold, Dg_Ca3 = Network.getOutputWTA, Ec_Ca3 = Network.getOutputWTA, Ca3_Ca3 = AutoAssociation.getOutputWTA, Ca3_Ca1= Network.getOutputLinearthreshold, Ca1_Ec = Network.getOutputWTA, Ec_Ca1 = Network.getOutputWTALinear, Ca3_Ec = None )
	
	incrementalLearnMethod = OneShoot.learnFiringDependent#learns either activtiy dpendent, or firing dependent
	incremental_storage_mode = None # learns pattern either 'online', i.e. during learning statisitcs or 'offline', i.e. after learning input statistics
	no_incremental_times = None
	active_in_env = dict(Ec = None, Dg =  None, Ca3 =None, Ca1 = None)
	
	#rec dynamics
	external_force = 1
	internal_force = 3
	cycles = 15
	
	noise_levels = np.arange(0,cells['Ec']+1, int(cells['Ec']/15))
	noise_levels_ca3 =[0]
	noise_levels_ca1 =[0]

	subtract_input_mean = 1
	subtract_output_mean = 0 #=1 for all Ca3_Ca3 Autoassociations

	def __init__():
		pass
################################################################################################		

################################################ Analysis Classes #######################################################
class Corelations(object):
	
	'''
	
	computes pearson correlations <a,b>/ab; where a is element of patterns_1 and b of patterns_2: Copmutes the correlation pairwise for patterns in each noise level. Different Environments are lumped together and treated as one. 
	
	:param patterns_1: original stored pattern
	:type patterns_1: array of dimension 2 (pattern, cellfire) or 3 (env, pattern, cellfire)
	:param patterns_2:  noisy_original pattern; if not given autocorrelation with patterns_1 is computed
	:type patterns_2: array of dimension 3 (noise, pattern, cellfire) or 4 (env, noise, pattern, cellfire)
	:param in_columns: If True, transpoes data given as (fire, pattern) or (noise, fire, pattern) into right dimension order.
	:type in_colums: Bool
		 
	 '''
			
	
	def __init__(self, patterns_1=None, patterns_2=None, in_columns = False, env = 1):


		self.orig_vs_orig = None #
		self.orig_vs_other = None #average corelation of original pattern and noisy version of the original; array has length len(noise_levels)
		self.over_orig_vs_orig = None#overlaps
		self.over_orig_vs_other = None
		if len(patterns_1.shape) == 3:
			self.patterns_1= patterns_1.reshape(patterns_1.shape[0]*patterns_1.shape[1], patterns_1.shape[-1]) # env, pat, cell into env*pat,cell
		else:
			self.patterns_1= patterns_1
		if len(patterns_1.shape) == 1: #pattern
			self.patterns_1 = patterns_1.reshape(patterns_1.shape[0], 1) #pattern, cell

		if patterns_2 == None:
			self.patterns_2 = np.tile(self.patterns_1, (2,1,1))
			self.one_patterns = True
		else:
			self.one_patterns = False
			if len(patterns_2.shape) == 4:
				self.patterns_2= np.swapaxes(patterns_2, 0,1).reshape(patterns_2.shape[1], patterns_2.shape[0]*patterns_2.shape[2], patterns_2.shape[-1])
			if len(patterns_2.shape) == 3:#env, pattern, cell
				if env:#env, pattern, cell
					self.patterns_2 = np.tile(patterns_2.reshape(patterns_2.shape[0]*patterns_2.shape[1], patterns_2.shape[-1]), (1,1,1)) #2, env*patt, cell
				else:#noise, pattern, cell
					self.patterns_2 = patterns_2
			if len(patterns_2.shape) == 2:#noise, pattern
				self.patterns_2 = patterns_2.reshape(patterns_2.shape[0],patterns_2.shape[1],1)
	

		if in_columns: #if data is given as columns, transpose them into rows !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! not tested with env framework
			self.patterns_1 = np.transpose(self.patterns_1)
			self.patterns_2 = np.transpose(self.patterns_2, (0,2,1))
			
			


		
		self.distance_matrix = None # matrix that has at (i,j) the euclidean distance between pattern i and j as entry
		self.corelations = None # Corelation Matrix; (abc) is corelation of pattern b and c with noise a
		self.calculated_cor = False #indicates if self.corelation is yet computed, this way double computation is avoided
		self.overlaps = None# Matrix of overlaps;(abc) is overlap of pattern b and c with noise a
		self.calculated_over = False #indicates if self.overlaps is yet computed, this way double computation is avoided
		
		self.covariance = None
		self.covariance_ev = None
		self.covariance_ew = None
		self.number_relevant = None
		self.projection_matrix = None
		
		
		self.distances = None
		self.all_distances = None
		self.p_d =None
		self.p_fire_given_distance = None
		
		self.fire_fire_distances = []
		self.fire_distances = []
		self.silent_distances = []
		self.fire_silent_distances = []
		self.p_d_fire= []
		self.p_d_silent = []
		self.p_fire_given_fire_and_distance = []
		self.p_fire_given_silent_and_distance = []
		self.p_fire_given_fire_and_distance_weighted_distance = []
		self.p_fire_given_silent_and_distance_weighted_distance = []
		for i in range(self.patterns_1.shape[-1]):
			self.fire_fire_distances +=[None]
			self.fire_distances +=[None]
			self.silent_distances +=[None]
			self.fire_silent_distances +=[None]
			self.p_d_fire +=[None]
			self.p_d_silent +=[None]
			self.p_fire_given_fire_and_distance +=[None]
			self.p_fire_given_silent_and_distance +=[None]
			self.p_fire_given_fire_and_distance_weighted_distance +=[None]
			self.p_fire_given_silent_and_distance_weighted_distance +=[None]

	######### calc Methods ####################
	def calcCor(self, noise_level = None, in_steps = False):
		'''
		
		computes the pearson corelation matrix self.corelations; self.corelation[abc] = corleation of pattern b and  pattern c at noise_level a =  <b - <b>, c - <c>)/||b||*||c|
		
		:param noise_level: noise_level at which the matrix is computed; if None then it is computed for all levels
		:type noise_level: int in the intervall (0, len(noise_levels))
		:param in_steps: If True and noise_level != None, corrleations are calculated individually for each pair. This is necessary only for huge data sets, when memory demands are too high.
		:type in_steps: Bool
		'''
		if noise_level == None:
			if not self.calculated_cor:
				if self.patterns_1.shape[-1] != 1:
					mean_subtracted_1 = self.patterns_1 - (np.einsum('pi->p', self.patterns_1*1)/(self.patterns_1.shape[-1]+0.0)).repeat(self.patterns_1.shape[-1]).reshape(self.patterns_1.shape)
					mean_subtracted_2 = self.patterns_2 - np.einsum('npi->np', self.patterns_2*1).repeat(self.patterns_2.shape[-1]).reshape(self.patterns_2.shape)/(self.patterns_2.shape[-1]+0.0)
					self.p1_norm_inverse = 1./np.sqrt(np.einsum('...ai, ...ai ->...a', mean_subtracted_1, mean_subtracted_1)) #input_norm_inverse[b]= 1/norm(b)
					self.p2_norm_inverse = 1./np.sqrt(np.einsum('...bi, ...bi ->...b', mean_subtracted_2, mean_subtracted_2))#p2_norm_inverse[a,b]= 1/norm(b(a)) ; b at noise level a
					self.corelations = np.einsum('pi, nqi, p, nq-> npq', mean_subtracted_1, mean_subtracted_2, self.p1_norm_inverse, self.p2_norm_inverse )#cor[abc] = at noise a over of patt b (noise =0) and c (noise = a)= <b - <b>, c - <c>)/||b||*||c||
				else:
					mean_subtracted_1 = self.patterns_1  - (np.einsum('pi->i', self.patterns_1*1)/(self.patterns_1.shape[-2]+0.0)).repeat(self.patterns_1.shape[-2]).reshape(self.patterns_1.shape)
					mean_subtracted_2 = self.patterns_2 - np.einsum('npi->ni', self.patterns_2*1).repeat(self.patterns_2.shape[-2]).reshape(self.patterns_2.shape)/(self.patterns_2.shape[-2]+0.0)
					self.p1_norm_inverse = 1./np.sqrt(np.einsum('ai, ai -> ', mean_subtracted_1, mean_subtracted_1)) #input_norm_inverse[b]= 1/norm(b)
					self.p2_norm_inverse = 1./np.sqrt(np.einsum('nbi, nbi -> n', mean_subtracted_2, mean_subtracted_2))#p2_norm_inverse[a,b]= 1/norm(b(a)) ; b at noise level a
					self.corelations = np.einsum('pi, nqi, n-> n', mean_subtracted_1, mean_subtracted_2, self.p2_norm_inverse ).reshape(1, self.patterns_2.shape[0]) * self.p1_norm_inverse #cor[abc] = at noise a over of patt b (noise =0) and c (noise = a)= <b - <b>, c - <c>)/||b||*||c||
				

				self.calculated_cor = True
		else:
			if self.corelations == None:
				self.corelations = np.zeros([self.patterns_2.shape[-3],self.patterns_2.shape[-2], self.patterns_2.shape[-2]])
				self.p2_norm_inverse = np.zeros([self.patterns_2.shape[-3], self.patterns_2.shape[-2]])
			if (self.corelations[noise_level] == 0).all():
				mean_subtracted_1 = self.patterns_1 - (np.einsum('pi->p', self.patterns_1*1)/(self.patterns_1.shape[-1]+0.0)).repeat(self.patterns_1.shape[-1]).reshape(self.patterns_1.shape)
				mean_subtracted_2 = self.patterns_2[noise_level] - np.einsum('pi->p', self.patterns_2[noise_level]*1).repeat(self.patterns_2.shape[-1]).reshape(self.patterns_2[0].shape)/(self.patterns_2.shape[-1]+0.0)
				self.p1_norm_inverse = 1./np.sqrt(np.einsum('...ai, ...ai ->...a', mean_subtracted_1, mean_subtracted_1)) #input_norm_inverse[b]= 1/norm(b)
				self.p2_norm_inverse[noise_level] = 1./np.sqrt(np.einsum('bi, bi ->b', mean_subtracted_2, mean_subtracted_2))#p2_norm_inverse[a,b]= 1/norm(b(a)) ; b at noise level a
				if in_steps:
					for i in range(mean_subtracted_1.shape[0]):
						for j in range(mean_subtracted_2.shape[0]):
							self.corelations[noise_level][i][j] = np.dot(mean_subtracted_1[i],mean_subtracted_2[j])* self.p1_norm_inverse[i]* self.p2_norm_inverse[noise_level][j]

				else:
					self.corelations[noise_level] = np.einsum('pi, qi, p, q-> pq', mean_subtracted_1, mean_subtracted_2, self.p1_norm_inverse, self.p2_norm_inverse[noise_level] )# cor[abc] = at noise a over of patt b (noise =0) and c (noise = a)= <b(0), 
	
	def calcOverlaps(self, noise_level = None):
		'''
		
		computes the overlap matrix self.overlaps at noise_level; overlap(p,q) = p^T q (inner product); self.overlaps[abc] = overlap of pattern b and  pattern c at noise_level a
	
		:param noise_level: noise_level at which the matrix is computed; if None then it is computd for all levels
		:type noise_level: int in the intervall (0, len(noise_levels))
		'''
		
		if noise_level == None:
			if not self.calculated_over:
				self.overlaps = np.einsum('bi, aci->abc', self.patterns_1*1, self.patterns_2*1)#noise a, stored_pattern b, recalled c
				self.calculated_over = True
		else:
			if self.overlaps == None:
				self.overlaps = np.zeros([self.patterns_2.shape[-3],self.patterns_2.shape[-2], self.patterns_2.shape[-2]])
			if (self.overlaps[noise_level] == 0).all():
				self.overlaps[noise_level] = np.einsum('bi, ci->bc', self.patterns_1*1, self.patterns_2[noise_level]*1)
	
	def calcOverlapsNoMean(self, noise_level = None):
		'''
		
		computes the overlap matrix self.overlapsNoMean at noise_level; overlapNoMean(p,q) = (p)^T * (q-q_0) where q_0 is the mean of q
	
		:param noise_level: noise_level at which the matrix is computed; if None then it is computd for all levels
		:type noise_level: int in the intervall (0, len(noise_levels))
		'''
		if noise_level == None:
			#mean_subtracted_2 = self.patterns_2 - np.einsum('npi->np', self.patterns_2*1).repeat(self.patterns_2.shape[-1]).reshape(self.patterns_2.shape)/(self.patterns_2.shape[-1]+0.0)
			mean_subtracted_2 = self.patterns_2 - np.einsum('npi->ni', self.patterns_2*1).repeat(self.patterns_2.shape[-2]).reshape(self.patterns_2.shape)/(self.patterns_2.shape[-2]+0.0)
			self.overlapsNoMean = np.einsum('bi, aci->abc', self.patterns_1*1, mean_subtracted_2*1)#noise a, stored_pattern b, recalled c
		else:
			self.overlapsNoMean = np.zeros([self.patterns_2.shape[-3],self.patterns_2.shape[-2], self.patterns_2.shape[-2]])
			#mean_subtracted_2 = self.patterns_2[noise_level] - np.einsum('pi->p', self.patterns_2[noise_level]*1).repeat(self.patterns_2.shape[-1]).reshape(self.patterns_2[0].shape)/(self.patterns_2.shape[-1]+0.0)
			mean_subtracted_2 = self.patterns_2[noise_level] - np.einsum('pi->i', self.patterns_2[noise_level]*1).repeat(self.patterns_2.shape[-2]).reshape(self.patterns_2[0].shape)/(self.patterns_2.shape[-2]+0.0)
			self.overlapsNoMean[noise_level] = np.einsum('bi, ci->bc', self.patterns_1*1, mean_subtracted_2*1)
	
	def calcDistanceMatrix(self, locations, round_it = 'fix', bins = None):
		
		'''
		
		calculates Distance Matrix given locations. Entry (i,j) is the eucledean distance between location i and j. 
		
		:param round_it:I f round_it != 0, distances are rounded. if round_it is an number, this number indicates the number of digits it is rounded to. If round it = 'fix', distances are rounded to next bin, specified by bins.
		:type round_it: int or 'fix'
		:param: bins: if round_it equals 'fix', distances are rounded up to their next bin in bins. If none, bins = np.linspace(0,1.5, 20)
		:type bins: np.array
		'''
		

		self.distance_matrix = np.sqrt(np.sum((np.tile(locations, (1,self.patterns_1.shape[0])).reshape(self.patterns_1.shape[0], self.patterns_1.shape[0],2) - np.tile(locations, (self.patterns_1.shape[0],1)).reshape(self.patterns_1.shape[0], self.patterns_1.shape[0],2))**2, axis = -1)) # (i,j) = eucl distance loc_i loc_j 
		if round_it != 0:
			print 'calc Dis matrix Cor with rounding'
			if round_it =='fix':
				print 'use fix bins for distances'
				if bins == None:
					print 'make fix bins'
					bins = np.linspace(0,1.5, 20)
				for i in range(1,bins.shape[0]): # distance are divided into evenly distributed 50 bins; first bin only contains 0 distance 
						self.distance_matrix[(self.distance_matrix <= bins[i]) - (self.distance_matrix <= bins[i-1])] = bins[i] #every entry that is <= bins[i] but without beeing <= bins[i-1] becomes bins[i]

			else:
				self.distance_matrix = np.round(self.distance_matrix, round_it)
			
		
	#########################gets with Corelations######################
	def getCor(self, at_noise=None): #get correlations at_noise
		'''
		
		get correlations at noise level at_noise
		'''
		self.calcCor(noise_level = at_noise)
		return self.corelations[at_noise]
		
	def getCorOrigOrig(self, at_noise=None):
		'''
		
		get diagonal of self.corelation; These are the correlations of original patterns with reconstructed version; if at_noise = None, gets all noise_levels and gets array of dimension two (noise, diagonal)
		'''
		self.calcCor()
		if at_noise == None:
			corelations = np.copy(self.corelations)
		else:
			corelations = np.copy(self.corelations[at_noise])
		return np.diagonal(corelations, 0, -1,-2)
		
	def getCorOrigOther(self, at_noise=None, subtract_orig_orig = 1, pattern = None, column = False): #
		
		'''
		
		get correlations entries of self.corealtions at_noise_level; if at_noise = None, gets all noise_levels and gets array of dimension two (noise, entries). If self.one_pattern, only entries (i,j) with i<=j are returned, since the entry (i,j) is equal to the entry (j,i) here.
	
		:param pattern: if pattern != None, it gets only the correation of this pattern with all others.
		:type pattern: integer indicating index of pattern
		:param column: if pattern != None, it determines whether self.correlations[at_noise, pattern] (row) or self.correlations[at_noise, :, pattern] (column) is returned
		:type column: bool
		:param subtract_orig_orig: If True, only correlation(p_j, p_i) with i!= j are returned; i.e. not the entries at the diagonal
		:type subtract_orig_orig: Bool
		 orig_vs_other as array; if noise = None, we have additional noise dimension in return, if one_patterns this is triangle entries of self.corelations, else it is all entries; subtract_orig_orig determines, if diagonal is included (orig_orig)
		'''
		self.calcCor(noise_level = at_noise)
		if pattern == None: # all patterns
			if at_noise == None: #
				corelations = np.copy(self.corelations)
				if self.one_patterns:#triangle is sufficent since cor matrix is symetric, here we use lower triangle
					if subtract_orig_orig:#without diag
						tril0 = np.tril_indices(corelations.shape[-1], -1)[0]
						tril1 = np.tril_indices(corelations.shape[-1], -1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						return_value = corelations[ind0, tril0, tril1]
					else: #with diagonal
						tril0 = np.tril_indices(corelations.shape[-1], 0)[0]
						tril1 = np.tril_indices(corelations.shape[-1], 0)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
					
						return_value = corelations[ind0, tril0, tril1]
				else: #lower and upper differ and must be used both
					if subtract_orig_orig:
						#lower triangel
						tril0 = np.tril_indices(corelations.shape[-1], -1)[0]
						tril1 = np.tril_indices(corelations.shape[-1], -1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						cor_lower = corelations[ind0, tril0, tril1]
					
						#upper triangel
						tril0 = np.triu_indices(corelations.shape[-1], 1)[0]
						tril1 = np.triu_indices(corelations.shape[-1], 1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						cor_upper = corelations[ind0, tril0, tril1]
					
						return_value = np.zeros([corelations.shape[0], corelations.shape[-1]**2 - corelations.shape[-1]])
						for i in range(corelations.shape[0]):
							return_value[i,:(corelations.shape[-1]**2 -corelations.shape[-1])/2] = cor_lower[i]
							return_value[i,(corelations.shape[-1]**2 -corelations.shape[-1])/2:] = cor_upper[i]	
					else:
						for i in range(corelations.shape[0]):
							return_value[i] = np.ravel(corelations[i])
			else:
				corelations = np.copy(self.corelations[at_noise])
				if self.one_patterns:
					if subtract_orig_orig:
						return_value = corelations[np.tril_indices(corelations.shape[-1], -1)]
					else:
						return_value = corelations[np.tril_indices(corelations.shape[-1], 0)]
				else:
					if subtract_orig_orig:
						cor_lower = corelations[np.tril_indices(corelations.shape[-1], -1)]
						cor_upper = corelations[np.triu_indices(corelations.shape[-1], 1)]
						return_value = np.zeros(corelations.shape[-1]**2 -corelations.shape[-1])
						return_value[:(corelations.shape[-1]**2 -corelations.shape[-1])/2] = cor_lower
						return_value[(corelations.shape[-1]**2 -corelations.shape[-1])/2:] = cor_upper
					else:
						return_value = np.ravel(corelations)
		else: #one pattern
			if at_noise != None:
				if column:
					return_value = self.corelations[at_noise][:,pattern]# column pattern, i.e. StoredRecalled, REcalled pattern used
				else:
					return_value = self.corelations[at_noise][pattern]# Stored pattern used
			else:
				return_value = np.zeros([self.corelations.shape[0],self.corelations.shape[1]])
				for i in range(self.corelations.shape[0] ):
					if column:
						return_value[i] = np.ravel(self.corelations[i,:,pattern])# column pattern, i.e. StoredRecalled, REcalled pattern used
					else:
						return_value[i] = np.ravel(self.corelations[i,pattern])# Stored pattern used	
		return return_value
	
	
	def getOrigOtherDistances(self, locations = None, subtract_orig_orig = 1, pattern = None): #
		
		'''
		
		get distances of corelations ,ie. the return array of CorOrigOther()
		'''
		distances = np.copy(self.getDistanceMatrix(round_it = 0, locations = locations))
		if pattern == None: # all patterns
			if self.one_patterns:
				if subtract_orig_orig:
					return_value = distances[np.tril_indices(distances.shape[-1], -1)]
				else:
					return_value = distances[np.tril_indices(distances.shape[-1], 0)]
			else:
				if subtract_orig_orig:
					cor_lower = distances[np.tril_indices(distances.shape[-1], -1)]
					cor_upper = distances[np.triu_indices(distances.shape[-1], 1)]
					return_value = np.zeros(distances.shape[-1]**2 -distances.shape[-1])
					return_value[:(distances.shape[-1]**2 -distances.shape[-1])/2] = cor_lower
					return_value[(distances.shape[-1]**2 -distances.shape[-1])/2:] = cor_upper
				else:
					return_value = np.ravel(distances)
		else: #one pattern
			return_value = distances[pattern]# Stored pattern used
		#self.distance_matrix = None
		return return_value
	
	def getOrigVsOrig(self):
		
		'''
		
		returns the average Corelation of the diagonal in self.correlations. If at_noise = None, at all noise_levels. Then return has dimension (noise_levels)
		'''
		if self.orig_vs_orig == None:
			if self.patterns_1.shape[-1] == 1:
				self.calcCor()
				self.orig_vs_orig = self.corelations[0]
			else:
				self.orig_vs_orig = np.sum(self.getCorOrigOrig(), axis = -1)/(self.patterns_1.shape[0]+0.0)
		return self.orig_vs_orig
		
	def getOrigVsOther(self, at_noise = None):  #returns average Corelation over all patterns, if at_noise = None, we have additional dimension over noise in Return
		'''
		
		returns the average Corelation of the entries away from the diagonal in self.correlations. If at_noise = None, at all noise_levels. Then return has dimension (noise_levels)
		'''
		if at_noise == None:
			if self.orig_vs_other == None:
				divide = self.patterns_1.shape[0]**2 - self.patterns_1.shape[0] +0.0
				if self.one_patterns:
					divide /= 2
				self.orig_vs_other = np.sum(self.getCorOrigOther(subtract_orig_orig = 1, at_noise = at_noise), axis = -1)/divide
			return self.orig_vs_other
			
		else:
			divide = self.patterns_1.shape[0]**2 - self.patterns_1.shape[0] +0.0
			if self.one_patterns:
				divide /= 2
			return np.sum(self.getCorOrigOther(subtract_orig_orig = 1, at_noise = at_noise), axis = -1)/divide

	
	###########gets with Overlaps###############
	#Same as gets with Correlation but uses now self.overlaps instead of self.correlation
	def getOverOrigOrig(self, at_noise=None):
		
		self.calcOverlaps(noise_level = at_noise)
		if at_noise == None:
			overlaps = np.copy(self.overlaps)
		else:
			overlaps = np.copy(self.overlaps[at_noise])
		return np.diagonal(overlaps, 0, -1,-2)
	
	def getOverOrigOther(self, at_noise=None, subtract_orig_orig = 1, pattern = None, column = False):
		self.calcOverlaps(noise_level = at_noise)
		if pattern == None: # all patterns
			if at_noise == None: #
				corelations = np.copy(self.overlaps)
				if self.one_patterns:#triangle is sufficent since cor matrix is symetric, here we use lower triangle
					if subtract_orig_orig:#without diag
						tril0 = np.tril_indices(corelations.shape[-1], -1)[0]
						tril1 = np.tril_indices(corelations.shape[-1], -1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						return_value = corelations[ind0, tril0, tril1]
					else: #with diagonal
						tril0 = np.tril_indices(corelations.shape[-1], 0)[0]
						tril1 = np.tril_indices(corelations.shape[-1], 0)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
					
						return_value = corelations[ind0, tril0, tril1]
				else: #lower and upper differ and must be used both
					if subtract_orig_orig:
						#lower triangel
						tril0 = np.tril_indices(corelations.shape[-1], -1)[0]
						tril1 = np.tril_indices(corelations.shape[-1], -1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						cor_lower = corelations[ind0, tril0, tril1]
					
						#upper triangel
						tril0 = np.triu_indices(corelations.shape[-1], 1)[0]
						tril1 = np.triu_indices(corelations.shape[-1], 1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						cor_upper = corelations[ind0, tril0, tril1]
					
						return_value = np.zeros([corelations.shape[0], corelations.shape[-1]**2 - corelations.shape[-1]])
						for i in range(corelations.shape[0]):
							return_value[i,:(corelations.shape[-1]**2 -corelations.shape[-1])/2] = cor_lower[i]
							return_value[i,(corelations.shape[-1]**2 -corelations.shape[-1])/2:] = cor_upper[i]	
					else:
						for i in range(corelations.shape[0]):
							return_value[i] = np.ravel(corelations[i])
			else:
				corelations = np.copy(self.overlaps[at_noise])
				if self.one_patterns:
					if subtract_orig_orig:
						return_value = corelations[np.tril_indices(corelations.shape[-1], -1)]
					else:
						return_value = corelations[np.tril_indices(corelations.shape[-1], 0)]
				else:
					if subtract_orig_orig:
						cor_lower = corelations[np.tril_indices(corelations.shape[-1], -1)]
						cor_upper = corelations[np.triu_indices(corelations.shape[-1], 1)]
						return_value = np.zeros(corelations.shape[-1]**2 -corelations.shape[-1])
						return_value[:(corelations.shape[-1]**2 -corelations.shape[-1])/2] = cor_lower
						return_value[(corelations.shape[-1]**2 -corelations.shape[-1])/2:] = cor_upper
					else:
						return_value = np.ravel(corelations)
		else: #one pattern
			if at_noise != None:
				if column:
					return_value = self.overlaps[at_noise][:,pattern]# column pattern, i.e. StoredRecalled, REcalled pattern used
				else:
					return_value = self.overlaps[at_noise][pattern]# Stored pattern used
			else:
				return_value = np.zeros([self.overlaps.shape[0],self.overlaps.shape[1]])
				for i in range(self.overlaps.shape[0] ):
					if column:
						return_value[i] = np.ravel(self.overlaps[i,:,pattern])# column pattern, i.e. StoredRecalled, REcalled pattern used
					else:
						return_value[i] = np.ravel(self.overlaps[i,pattern])# Stored pattern used	
		return return_value
	
	def getOverOrigVsOrig(self, normed = 1): #returns average Corelation over all patterns, dimension over noise
		
		'''
		
		As getOrigVsOrig, nut now with self.overlaps instead of self.correlation. if normed, then retun is divided by no winner to get value between 1 and 0
		'''
		
		if self.over_orig_vs_orig == None:
			self.over_orig_vs_orig = np.sum(self.getOverOrigOrig(), axis = -1)/(self.patterns_1.shape[0]+0.0)
			if normed == 1:
				self.over_orig_vs_orig /= np.sum(self.patterns_1[0]) *1. # divide by no winner to get value between 1 and 0
		return self.over_orig_vs_orig

	def getOverOrigVsOther(self, normed = 1):  #returns average Corelation over all patterns, dimension over noise
		
		'''
		
		As getOrigVsOther, nut now with self.overlaps instead of self.correlation. if normed, then retun is divided by no winner to get value between 1 and 0
		'''
		
		if self.over_orig_vs_other == None:
			divide = self.patterns_1.shape[0]**2 - self.patterns_1.shape[0] +0.0
			if self.one_patterns:
				divide /= 2
			self.over_orig_vs_other = np.sum(self.getOverOrigOther(subtract_orig_orig = 1), axis = -1)/divide
			if normed == 1:
				self.over_orig_vs_other /= np.sum(self.patterns_1[0]) *1. # divide by no winner to get value between 1 and 0
		return self.over_orig_vs_other


	###########gets with OverlapsNoMean###############
	#Same as gets with Correlation but uses now self.overlapsNoMean instead of self.correlation
	def getOverOrigOrigNoMean(self, at_noise=None):
		
		self.calcOverlapsNoMean(noise_level = at_noise)
		if at_noise == None:
			overlaps = np.copy(self.overlapsNoMean)
		else:
			overlaps = np.copy(self.overlapsNoMean[at_noise])
		return np.diagonal(overlaps, 0, -1,-2)
	
	def getOverOrigOtherNoMean(self, at_noise=None, subtract_orig_orig = 1, pattern = None, column = False):
		self.calcOverlapsNoMean(noise_level = at_noise)
		if pattern == None: # all patterns
			if at_noise == None: #
				corelations = np.copy(self.overlapsNoMean)
				if self.one_patterns:#triangle is sufficent since cor matrix is symetric, here we use lower triangle
					if subtract_orig_orig:#without diag
						tril0 = np.tril_indices(corelations.shape[-1], -1)[0]
						tril1 = np.tril_indices(corelations.shape[-1], -1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						return_value = corelations[ind0, tril0, tril1]
					else: #with diagonal
						tril0 = np.tril_indices(corelations.shape[-1], 0)[0]
						tril1 = np.tril_indices(corelations.shape[-1], 0)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
					
						return_value = corelations[ind0, tril0, tril1]
				else: #lower and upper differ and must be used both
					if subtract_orig_orig:
						#lower triangel
						tril0 = np.tril_indices(corelations.shape[-1], -1)[0]
						tril1 = np.tril_indices(corelations.shape[-1], -1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						cor_lower = corelations[ind0, tril0, tril1]
					
						#upper triangel
						tril0 = np.triu_indices(corelations.shape[-1], 1)[0]
						tril1 = np.triu_indices(corelations.shape[-1], 1)[1]
						ind0 = np.arange(corelations.shape[0]).repeat(tril0.shape[0]).reshape(corelations.shape[0], tril0.shape[0])
						cor_upper = corelations[ind0, tril0, tril1]
					
						return_value = np.zeros([corelations.shape[0], corelations.shape[-1]**2 - corelations.shape[-1]])
						for i in range(corelations.shape[0]):
							return_value[i,:(corelations.shape[-1]**2 -corelations.shape[-1])/2] = cor_lower[i]
							return_value[i,(corelations.shape[-1]**2 -corelations.shape[-1])/2:] = cor_upper[i]	
					else:
						for i in range(corelations.shape[0]):
							return_value[i] = np.ravel(corelations[i])
			else:
				corelations = np.copy(self.overlapsNoMean[at_noise])
				if self.one_patterns:
					if subtract_orig_orig:
						return_value = corelations[np.tril_indices(corelations.shape[-1], -1)]
					else:
						return_value = corelations[np.tril_indices(corelations.shape[-1], 0)]
				else:
					if subtract_orig_orig:
						cor_lower = corelations[np.tril_indices(corelations.shape[-1], -1)]
						cor_upper = corelations[np.triu_indices(corelations.shape[-1], 1)]
						return_value = np.zeros(corelations.shape[-1]**2 -corelations.shape[-1])
						return_value[:(corelations.shape[-1]**2 -corelations.shape[-1])/2] = cor_lower
						return_value[(corelations.shape[-1]**2 -corelations.shape[-1])/2:] = cor_upper
					else:
						return_value = np.ravel(corelations)
		else: #one pattern
			if at_noise != None:
				if column:
					return_value = self.overlapsNoMean[at_noise][:,pattern]# column pattern, i.e. StoredRecalled, REcalled pattern used
				else:
					return_value = self.overlapsNoMean[at_noise][pattern]# Stored pattern used
			else:
				return_value = np.zeros([self.overlapsNoMean.shape[0],self.overlapsNoMean.shape[1]])
				for i in range(self.overlapsNoMean.shape[0] ):
					if column:
						return_value[i] = np.ravel(self.overlapsNoMean[i,:,pattern])# column pattern, i.e. StoredRecalled, REcalled pattern used
					else:
						return_value[i] = np.ravel(self.overlapsNoMean[i,pattern])# Stored pattern used	
		return return_value
	
	def getOverOrigVsOrigNoMean(self, at_noise = None, normed = 0): #returns average Corelation over all patterns, dimension over noise
		
		'''
		
		As getOrigVsOrig, nut now with self.overlapsNoMean instead of self.correlation. if normed, then retun is divided by no winner to get value between 1 and 0
		'''
		
		over = np.sum(self.getOverOrigOrigNoMean(), axis = -1)/(self.patterns_1.shape[0]+0.0)
		if normed == 1:
			over /= np.sum(self.patterns_1[0]) *1. # divide by no winner to get value between 1 and 0
		if at_noise != None:
			over = over[at_noise]
		return over

	def getOverOrigVsOtherNoMean(self, normed = 0):  #returns average Corelation over all patterns, dimension over noise
		
		'''
		
		As getOrigVsOther, nut now with self.overlapsNoMean instead of self.correlation. if normed, then retun is divided by no winner to get value between 1 and 0
		'''
		
		if self.over_orig_vs_other == None:
			divide = self.patterns_1.shape[0]**2 - self.patterns_1.shape[0] +0.0
			if self.one_patterns:
				divide /= 2
			self.over_orig_vs_other = np.sum(self.getOverOrigOther(subtract_orig_orig = 1), axis = -1)/divide
			if normed == 1:
				self.over_orig_vs_other /= np.sum(self.patterns_1[0]) *1. # divide by no winner to get value between 1 and 0
		return self.over_orig_vs_other


	### Analysis of Distances and Overlaps
	def getOverNoMeanDistances(self, locations= None, bins = None, location = None, at_noise =0):
		
		'''
		
		gets the entries of self.overlapsNoMean sorted over distances of the corresponding locations. bins specifies the bins in which the overlaps are sorted. Does only consider entries (i,j) in self.overlaps where i>j. Also returns used_bins, that are the bins of bins that are non_empty.
		'''
		self.calcOverlapsNoMean(noise_level = at_noise)
		overlaps = np.copy(self.overlapsNoMean[at_noise])

		overlaps[np.tril_indices(overlaps.shape[-1],-1)] = -10**5 # only i>= j
		#overlaps[np.diag_indices(overlaps.shape[-1])] = -10**5 # only i !=j
		#self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		return_value = []
		used_bins = []
		if location != None:
			overlaps = overlaps[location]
			dis = dis[location]
		
		for i in range(bins.shape[0]):
			append = overlaps[dis == bins[i]][overlaps[dis == bins[i]]>-10**5]
			if append.shape[0] != 0:
				return_value.append(np.copy(append))
				used_bins.append(bins[i])
		return [return_value, used_bins]
		
	def getAverageOverlapOverNoMeanDistance(self, locations = None, bins = None, location = None):
		'''
		
		gets the average overlap at each distance bin
		'''
		
		over, used_bins = self.getOverNoMeanDistances(locations, bins, location)
		av = np.zeros(len(used_bins))
		for i in range(len(over)):
			av[i] = np.sum(over[i])/(over[i].shape[0]*1.)
			i+=1
		return [av, used_bins]
		
		
		
	def getOverDistances(self, locations= None, bins = None, location = None, at_noise =0):
		'''
		
		gets the entries of self.overlaps sorted over distances of the corresponding locations. bins specifies the bins in which the overlaps are sorted. Does only consider entries (i,j) in self.overlaps where i=>j. Also returns used_bins, that are the bins of bins that are non_empty. If location != None, only this specific location (pattern) is considered for index i
		'''
		self.calcOverlaps(noise_level = at_noise)
		overlaps = np.copy(self.overlaps[at_noise])

		overlaps[np.tril_indices(overlaps.shape[-1],-1)] = -10**5 # only i>= j
		#overlaps[np.diag_indices(overlaps.shape[-1])] = -10**5 # only i !=j
		#self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		return_value = []
		used_bins = []
		if location != None:
			overlaps = overlaps[location]
			dis = dis[location]
		
		for i in range(bins.shape[0]):
			append = overlaps[dis == bins[i]][overlaps[dis == bins[i]]>-10**5]
			if append.shape[0] != 0:
				return_value.append(np.copy(append))
				used_bins.append(bins[i])
		return [return_value, used_bins]

	def getAverageOverlapOverDistance(self, locations = None, bins = None, location = None):
		'''
		
		gets the average overlap at each distance bin
		'''
		
		over, used_bins = self.getOverDistances(locations, bins, location)
		av = np.zeros(len(used_bins))
		for i in range(len(over)):
			av[i] = np.sum(over[i])/(over[i].shape[0]*1.)
			i+=1
		return [av, used_bins]
		
		
	def getCorDistances(self, locations= None, bins = None, location = None, at_noise =0):
		'''
		
		gets the entries of self.overlaps sorted over distances of the corresponding locations. bins specifies the bins in which the overlaps are sorted. Does only consider entries (i,j) in self.overlaps where i=>j. Also returns used_bins, that are the bins of bins that are non_empty. If location != None, only this specific location (pattern) is considered for index i
		'''
		self.calcCor(noise_level = at_noise)
		overlaps = np.copy(self.corelations[at_noise])

		overlaps[np.tril_indices(overlaps.shape[-1],-1)] = -10**5 # only i>= j
		#overlaps[np.diag_indices(overlaps.shape[-1])] = -10**5 # only i !=j
		#self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		return_value = []
		used_bins = []
		if location != None:
			overlaps = overlaps[location]
			dis = dis[location]
		
		for i in range(bins.shape[0]):
			append = overlaps[dis == bins[i]][overlaps[dis == bins[i]]>-10**5]
			if append.shape[0] != 0:
				return_value.append(np.copy(append))
				used_bins.append(bins[i])
		return [return_value, used_bins]
		
	def getAverageCorOverDistance(self, locations = None, bins = None, location = None):
		'''
		
		gets the average overlap at each distance bin
		'''
		
		over, used_bins = self.getCorDistances(locations, bins, location)
		av = np.zeros(len(used_bins))
		for i in range(len(over)):
			av[i] = np.sum(over[i])/(over[i].shape[0]*1.)
			i+=1
		return [av, used_bins]
		
		

	#### Methods used for findPlaceWeight Analysis
	def getLocationsWithinDistance(self, distance = None, locations = None, location = None, bins = None):
		'''
		
		gets locations within the radius distance and outside the radius around location. Distances are rounded according to bins
		'''
		#self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		distances = dis[location]
		inside = np.flatnonzero(distances <= distance)
		outside = np.flatnonzero(distances > distance)
		return [inside, outside]
		
	def getLocationsAtDistance(self, distance = None, locations = None, location = None, bins = None):
		'''
		
		gets locations at the exact distance to location. Distances are rounded according to bins
		'''
		#self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		distances = dis[location]
		at_distance = np.flatnonzero(distances == distance)
		return at_distance
		
	def getPopulationWithinDistance(self, distance = None, locations = None, location = None, bins = None, with_noise = 0):
		
		'''
		
		gets patterns within the radius distance and outside the radius around location. Distances are rounded according to bins; if with_noise, then all noisy versions of the patterns are returned too.
		'''
		
		[inside, outside] = self.getLocationsWithinDistance(distance = distance, locations = locations, location = location, bins = bins)
		inside_pop = self.patterns_2[0][inside]
		outside_pop =self.patterns_2[0][outside]
		if with_noise:
			for i in range(self.patterns_2.shape[0]-1):
				inside_pop = np.concatenate((inside_pop, self.patterns_2[i+1][inside]))
				outside_pop = np.concatenate((outside_pop, self.patterns_2[i+1][outside]))
		return [inside_pop, outside_pop]
		
	def findPlaceWeight(self, distance = None, locations = None, location = None, bins = None, with_noise = 0, max_weight = -1, boarder = 0, influence = 0, non_negative = 1, alpha = 1):
		
		'''
		
		gets solution of ....
		'''
		
		
		[inside, outside] = self.getPopulationWithinDistance(distance = distance, locations = locations, location = location, bins = bins, with_noise = with_noise)
		
		G = np.concatenate((outside, - inside), axis = 0)
		h = [1.]* outside.shape[0] + [-1.] *inside.shape[0]
		
		if non_negative:
			G = np.concatenate((G, - np.eye(inside.shape[1])*1.), axis = 0)
			h = h + [0] *inside.shape[1]
			
			
		if max_weight != -1:
			#each weight less equal 0.3
			print 'each weight less equal ' +str(max_weight)
			G = np.concatenate((G, np.eye(inside.shape[1])*1.), axis = 0)
			h = h + [max_weight] *inside.shape[1]
			if not non_negative:# w >= - max
				G = np.concatenate((G, -np.eye(inside.shape[1])*1.), axis = 0)
				h = h + [max_weight] *inside.shape[1]
			
		if boarder:
			at_distance = self.patterns_1[self.getLocationsAtDistance(distance = distance, locations = locations, location = location, bins = bins)]
			print 'boarder findplace weight'

			G = np.concatenate((G, at_distance*1. - np.tile(self.patterns_1[location], (at_distance.shape[0], 1))), axis = 0)
			h = h + [0] * at_distance.shape[0]
			
		if influence:
			print 'influence findplace weight'
			ones = np.ones([1,inside.shape[-1]])
			ones[0][:self.modules[1]] = -1 *1./self.modules[1] # 1/n * sum module 4 - 1/m * sum module 1 <= 0
			ones[0][self.modules[1]:self.modules[3]] = 0 # mod 2 and 3 not considered
			ones[0][self.modules[3]:] = 1./(self.modules[4]-self.modules[3])
			
			G = np.concatenate((G, ones*1.), axis = 0)
			h = h + [0]
			
			#same for module 2:  1/n * sum module 4 - 1/m * sum module 2 <= 0
			ones = np.ones([1,inside.shape[-1]])

			ones[0][:self.modules[1]] = 0 
			ones[0][self.modules[1]:self.modules[2]] = -1 *1./(self.modules[2]-self.modules[1])
			ones[0][self.modules[2]:self.modules[3]] = 0
 			ones[0][self.modules[3]:] = 1./(self.modules[4]-self.modules[3])
			
			G = np.concatenate((G, ones*1.), axis = 0)
			h = h + [0]
			
			
			
			
		G = matrix(G)
		h = matrix(h)
			
		#c = matrix([1.] * inside.shape[1]) #minimize sum over components x_i
		
		# maximize differences px - qx for 100 random chosen inside p and outside q - sum x_i as penalty for big x.
		if inside.shape[0] < 10:
			rand_in = np.tile(self.patterns_1[location], (1000,1))
		else:
			if inside.shape[0] < 50:
				rand_in =  100* np.array(random.sample(inside, 10))
			else:
				if inside.shape[0] < 100:
					rand_in =  20* np.array(random.sample(inside, 50))
				else:
					if inside.shape[0] < 500:
						rand_in =  10 * np.array(random.sample(inside, 100))
					else:
						if inside.shape[0] < 1000:
							rand_in =  2 * np.array(random.sample(inside, 500))
						else:
							if inside.shape[0] >= 1000:
								rand_in =  np.array(random.sample(inside, 1000))
		
		if outside.shape[0] >= 1000:
			rand_out = np.array(random.sample(outside, 1000))
		else: 
			if outside.shape[0] >= 100:
				rand_out = 10 *np.array(random.sample(outside, 100))
			else:
				rand_out = 100 *np.array(random.sample(outside, 10))
		c = np.sum(rand_out, axis = -2) - np.sum(rand_in, axis = -2) + np.ones(inside.shape[1], 'float')
		
		
		c = matrix(c)
		
		##old linear 
		#if non_negative:
			#solve = solvers.lp(c,G,h)['x']
		#else:
		##new quadratic. necessary since min x_i^2 and not sim x_i
		p = matrix(alpha * np.eye(inside.shape[1])*1.) # xPx + qx
		c = 1/1000. * (matrix(np.sum(rand_out, axis = -2) - np.sum(rand_in, axis = -2)))# q=c, now without np.ones(inside.shape[1], 'float') term
		
		## min (outside . center) * w
		#c = 1./outside.shape[0] * np.sum(inside, axis = -2) -self.patterns_1[location]
		#c = matrix(c)
		
		solve = solvers.qp(p,c,G,h)['x'] #min sum_i x_i^2 
		return solve
		
	def getNumberWrongPixels(self, activation_map = None, location = None, locations = None, distance = None, bins=None):#return proportion inside wornd and outside wrong
		'''
		
		determines proportion pixel that fire inside the field wrong and outside.
		'''
		[inside, outside] = self.getLocationsWithinDistance(distance = distance, locations = locations, location = location, bins = bins)
		wrong_inside = np.flatnonzero(activation_map[inside] < 1).shape[0]
		wrong_outside = np.flatnonzero(activation_map[outside] > 1).shape[0]
		return [wrong_inside/(inside.shape[0]+0.0), wrong_outside/(outside.shape[0]+0.0)]
		
		
	def findPlaceWeightSVC(self, distance = None, locations = None, location = None, bins = None, with_noise = None, **kwargs):
	
		[inside, outside] = self.getPopulationWithinDistance(distance = distance, locations = locations, location = location, bins = bins, with_noise = with_noise)
		X = np.concatenate((inside, outside))
		Y = np.concatenate((np.ones(inside.shape[0]), np.ones(outside.shape[0])*-1))

		SVC = svm.LinearSVC()
		SVC.fit(X,Y)
		w = SVC.coef_

		return w




	def getOverNoMeanActiveCellsDistances(self, output_patterns, locations = None, bins = None):#get <p_1, p_2 - bar p> for all p such that q_i^1 = q^2 = 1; sorts it into bins, acroding to in which distance 1,2 have
		
		self.calcOverlapsNoMean(noise_level = 0)
		over = np.copy(self.overlapsNoMean[0])
		over[np.diag_indices_from(over)] = -10**10 #to exclude overlap of pattern with itself later
		p_1 = np.array([], 'int') #inidzes pattern_1
		p_2 = np.array([], 'int')#inidzes pattern_2
		cells = np.array([], 'int')
		for cell in range(output_patterns.shape[-1]):
			
			non_z_cell = np.flatnonzero(output_patterns[:,cell]) #cells that fire together with cell
			size = non_z_cell.shape[0]
			p_1 = np.concatenate((non_z_cell.repeat(size, axis = 0),p_1))
			p_2 = np.concatenate((np.tile(non_z_cell, size), p_2))
			cells = np.concatenate((np.ones(size*size, 'int')*cell, cells))#cell index repeated as often as cell is part of one overlap pair

		
		self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		return_value = []
		used_bins = []
		for i in range(bins.shape[0]):
			append = over[p_1,p_2][dis[p_1,p_2] == bins[i]] * output_patterns[p_2, cells][dis[p_1,p_2] == bins[i]]
			append = append[append> -10**4]
			if append.shape[0] != 0:
				return_value.append(append)
				used_bins.append(bins[i])

		return [return_value, used_bins]
		
	def getOverNoMeanSilentCellsDistances(self, output_patterns, locations = None, bins = None):#get <p_1, p_2 - bar p> for all p such that q_i^1 = 0, q_i^2 = 1
		
		self.calcOverlapsNoMean(noise_level = 0)
		over = np.copy(self.overlapsNoMean[0])
		over[np.diag_indices_from(over)] = -10**10 #to exclude overlap of pattern with itself later
		p_1 = np.array([], 'int') #inidzes pattern_1
		p_2 = np.array([], 'int')#inidzes pattern_2
		cells = np.array([], 'int')
		for cell in range(output_patterns.shape[-1]):
			
			output_patterns[:,cell]
			non_z_cell = np.flatnonzero(output_patterns[:,cell]) #cells that fire together with cell
			z_cell = np.flatnonzero(output_patterns[:,cell] == 0)
			size_non = non_z_cell.shape[0]
			size_z= z_cell.shape[0]
			p_1 = np.concatenate((z_cell.repeat(size_non, axis = 0),p_1))
			p_2 = np.concatenate((np.tile(non_z_cell, size_z), p_2))
			cells = np.concatenate((np.ones(size_z*size_non, 'int')*cell, cells))#cell index repeated as often as cell is part of one overlap pair

		
		self.calcDistanceMatrix(locations, bins = bins)
		dis = self.getDistanceMatrix(locations, bins = bins)
		return_value = []
		used_bins = []
		for i in range(bins.shape[0]):
			append = over[p_1,p_2][dis[p_1,p_2] == bins[i]] * output_patterns[p_2, cells][dis[p_1,p_2] == bins[i]]
			append = append[append> -10**4]
			if append.shape[0] != 0:
				return_value.append(append)
				used_bins.append(bins[i])

		return [return_value, used_bins]

	def getMeanActivityActiveCells(self): #gets the average firing of an active cell
		active_cells = np.nonzero(self.patterns_1)
		return np.sum(self.patterns_1[active_cells], -1)/(np.flatnonzero(self.patterns_1).shape[0]*1.)
	
	def getOverLocationWithAllOthers(self, locations = None, location = None):
		overlaps = np.dot(self.patterns_1, self.patterns_1[location])
		return overlaps
		
	def getOverLocationWithAllOthersNoAverage(self, locations = None, location = None):
		overlaps = np.dot(self.patterns_1, self.patterns_1[location])
		av = np.sum(overlaps)/(self.patterns_1.shape[0]*1.)

		return overlaps -av
	
	#Misc
	def getDistanceMatrix(self, locations, round_it = 'fix', bins = None): #returns matrix (loc, loc) with the distance of the two as entry 
		if self.distance_matrix == None:
			self.calcDistanceMatrix(locations, round_it = round_it, bins = bins)

		return self.distance_matrix
	
	def getOccurencesDistances(self, locations, bins = None): #all distances (multiple times) as an array 
		if self.distances == None:
			self.distances= np.ravel(self.getDistanceMatrix(locations, bins = bins))

		return self.distances
		
	def calcOccurencesDistancesFireFire(self,locations, cells= None, bins = None):#returns all distances (multple times) of two locations, whenever cell fires at both; Oc( q=1, q=1, D=d). If cell = None, all cells are considered
		
		distance_matrix = self.getDistanceMatrix(locations = locations, bins = bins)
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		
		for cell in cells:
			if self.fire_fire_distances[cell] == None:
				self.fire_fire_distances[cell] = np.zeros(self.getAllDifferentDistances(locations, bins = bins ).shape[0])
				fire_locs = np.nonzero(self.patterns_1[:,cell])[0]
				distances = distance_matrix[fire_locs.repeat(fire_locs.shape[0]), np.tile(fire_locs, (1,fire_locs.shape[0]))]
				i=0
				for d in self.getAllDifferentDistances(locations, bins = bins ):
					self.fire_fire_distances[cell][i] = np.nonzero(distances == d)[0].shape[0]
					i+=1
					
	def calcOccurencesFireDistances(self,locations, bins = None, cells= None):#returns all distances (multple times) of a location, where a cell fires and all other locations; OC(q = 1, D=d); should be proportional to getOccurencesDistances (since firing and location is independent). In Fact OC(q = 1 , D=d) = oc(D=d) * oc(q=1) = getOccurencesDistances * cell_number (=1 if cells != None) * oc(q=1); if cells = None all cells are considered
		
		distance_matrix = self.getDistanceMatrix(locations = locations, bins = bins)
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]

		for cell in cells:
			if self.fire_distances[cell] == None:
				self.fire_distances[cell] = np.zeros(self.getAllDifferentDistances(locations, bins = bins ).shape[0])
				fire_locs = np.nonzero(self.patterns_1[:,cell])[0]
				distances =np.ravel(distance_matrix[fire_locs])
				i=0
				for d in self.getAllDifferentDistances(locations, bins = bins ):
					self.fire_distances[cell][i] = np.nonzero(distances == d)[0].shape[0]
					i+=1

		
	def calcOccurencesSilentDistances(self,locations, bins = None, cells= None):#returns all distances (multple times) of a location, where cell is silent and all other locations; OC(q = 0, D=d); should be proportional to getOccurencesDistances (since independent). In Fact OC(q = 0 , D=d) = oc(D=d) * P(q=0) = getOccurencesDistances * cell_number * P(q=0)
		distance_matrix = self.getDistanceMatrix(locations = locations, bins = bins )
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.silent_distances[cell] == None:
				self.silent_distances[cell] = np.zeros(self.getAllDifferentDistances(locations, bins = bins ).shape[0])
				silent_locs = np.nonzero(self.patterns_1[:,cell]-1)[0]
				distances = np.ravel(distance_matrix[silent_locs])
				i=0
				for d in self.getAllDifferentDistances(locations, bins = bins ):
					self.silent_distances[cell][i] = np.nonzero(distances == d)[0].shape[0]
					i+=1

	def calcOccurencesDistancesFireSilent(self,locations, bins = None, cells= None):#returns all distances (multple times) of a location, where cell fires and one where not; OC(q=0, q=1, D=d)
		
		distance_matrix = self.getDistanceMatrix(locations = locations, bins = bins )
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.fire_silent_distances[cell] == None:
				self.fire_silent_distances[cell] = np.zeros(self.getAllDifferentDistances(locations, bins = bins ).shape[0])
				fire_locs = np.nonzero(self.patterns_1[:,cell])[0]
				silent_locs = np.nonzero(self.patterns_1[:,cell]-1)[0]
				distances = distance_matrix[fire_locs.repeat(silent_locs.shape[0]), np.tile(silent_locs, (1,fire_locs.shape[0]))]
				i=0
				for d in self.getAllDifferentDistances(locations, bins = bins ):
					self.fire_silent_distances[cell][i] = np.nonzero(distances == d)[0].shape[0]
					i+=1

	def getAllDifferentDistances(self, locations, bins = None): #returns all ocurring distances one time (deleting all multple ones) in increasing size
		if self.all_distances == None:
			self.all_distances = np.sort(list(set(self.getOccurencesDistances(locations = locations, bins = bins))))

		return self.all_distances
		
	def getPDistance(self, locations,bins = None): #returns probablity of a distance; P(D=d) = OC(d)/OC(all d)
		if self.p_d ==None:
			all_distances = self.getAllDifferentDistances(locations, bins = bins )
			self.p_d = np.zeros(all_distances.shape[0])
			i=0
			for d in all_distances:
				self.p_d[i] = np.nonzero(self.getOccurencesDistances(locations, bins = bins ) == d)[0].shape[0]
				i+=1
			self.p_d /= (np.sum(self.p_d)*1.)

		return self.p_d
		
	def calcPDistanceFireLocations(self,locations, bins = None, cells= None): #returns probablity of a distance; P(D=d) = OC(q =1, d)/OC(all q = 1)
		
		self.calcOccurencesFireDistances(locations, bins = bins , cells = cells)
		all_distances = self.getAllDifferentDistances(locations, bins = bins )
		
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.p_d_fire[cell]== None:
				self.p_d_fire[cell] = self.fire_distances[cell]
				if np.sum(self.fire_distances[cell])*1. != 0:
					self.p_d_fire[cell] /=(np.sum(self.fire_distances[cell])*1.)
		
	def calcPDistanceSilentLocations(self,locations, bins = None, cells = None): #returns probablity of a distance; P(D=d) = OC(q =0, d)/OC(all q = 0)
		
		self.calcOccurencesSilentDistances(locations, bins = bins , cells = cells) 
		all_distances = self.getAllDifferentDistances(locations, bins = bins )
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.p_d_silent[cell] == None:
				self.p_d_silent[cell] = self.silent_distances[cell]/(np.sum(self.silent_distances[cell])*1.)
		
	def getPFireGivenDistance(self,locations, bins = None, cells = None):#returns probablity P(q=1 | D=d) = P(q=1,D=d)/P(D=d) = OC(q=1, D=d)/OC(D=d); OC(D=d) must be same counting as OC(q=1, D=d)
		
		all_distances = self.getAllDifferentDistances(locations, bins = bins )
		i=0
		occurences_fire_distances = self.getOccurencesFireDistances(locations, bins = bins , cells = cells)
		occurences_distances = self.getOccurencesDistances(locations, bins = bins )
		for d in all_distances:
			self.p_fire_given_distance[i] = np.nonzero(occurences_fire_distances == d)[0].shape[0]
			self.p_fire_given_distance[i] /= np.nonzero(occurences_distances == d)[0].shape[0]
			i+=1
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		self.p_fire_given_distance/= (len(cells)*1.0) #Divide by cell numbers, since OC(D=d) is counted for each cell once and self.getOccurencesDistances(locations) just one time
		print 'getPFireGivenDistance'
		print 'should be not used!!!!!!!!!!1'
		return self.p_fire_given_distance
		
	def calcPFireGivenFireAndDistance(self, locations, cells = None, bins = None): #returns probablity P(q=1 | q(t) = 1, D=d) = P(q=1 + q(t)=1 + D=d)/P(D=d + q(t)=1) = OC(q=1, q=1, D=d)/OC(q=1, D=d)
		
		all_distances = self.getAllDifferentDistances(locations, bins = bins)
		
		self.calcOccurencesFireDistances(locations, cells = cells, bins = bins)
		self.calcOccurencesDistancesFireFire(locations, cells = cells, bins = bins)
		
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.p_fire_given_fire_and_distance[cell] == None:
				self.p_fire_given_fire_and_distance[cell] = self.fire_fire_distances[cell]
				self.p_fire_given_fire_and_distance[cell][self.fire_distances[cell]!=0] /= (self.fire_distances[cell][self.fire_distances[cell]!=0]*1.)

		
	def calcPFireGivenSilentAndDistance(self, locations, bins = None,  cells = None): #returns probablity P(q=1 | q(t) = 0, D=d) = P(q=1 , q(t)=0 + D=d)/P(D=d + q(t)=0) = OC(q=1, q=0, D=d)/OC(q=0, D=d)
		
		all_distances = self.getAllDifferentDistances(locations, bins = bins )
		self.calcOccurencesSilentDistances(locations, bins = bins , cells = cells) 
		self.calcOccurencesDistancesFireSilent(locations, bins = bins , cells = cells)
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.p_fire_given_silent_and_distance[cell] == None:
				self.p_fire_given_silent_and_distance[cell] = self.fire_silent_distances[cell]
				self.p_fire_given_silent_and_distance[cell][self.silent_distances[cell] != 0] /= (self.silent_distances[cell][self.silent_distances[cell] != 0]*1.)

	def calcPFireGivenFireAndDistanceWeightedDistances(self, locations, bins = None,  cells = None): #returns P(q =1 | q_t = 1, D=d) * P(D=d|q = 1) = Oc(1,1,d)/oc(1,d) * oc(1,d)/oc(1) = oc(1,1,d)/oc(1)
		
		self.calcPFireGivenFireAndDistance(locations, cells, bins = bins)
		self.calcPDistanceFireLocations(locations, bins = bins )
		
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.p_fire_given_fire_and_distance_weighted_distance[cell] == None:
				self.p_fire_given_fire_and_distance_weighted_distance[cell] = self.p_fire_given_fire_and_distance[cell] * self.p_d_fire[cell]

	def calcPFireGivenSilentAndDistanceWeightedDistances(self, locations, bins = None,  cells = None):#returns P(q =1 | q_t = 0, D=d) * P(D=d) = P(q=1, D=d | q_t =0)

				
		self.calcPFireGivenSilentAndDistance(locations, cells, bins = bins)
		self.calcPDistanceSilentLocations(locations, bins = bins )
		
		if cells == None:
			cells = range(self.patterns_1.shape[1])
		else:
			cells = [cells]
		for cell in cells:
			if self.p_fire_given_silent_and_distance_weighted_distance[cell] == None:
				self.p_fire_given_silent_and_distance_weighted_distance[cell] = self.p_fire_given_silent_and_distance[cell] * self.p_d_silent[cell]
	
	def getPFireGivenFireAndDistanceAll(self, locations, bins = None):
		self.calcPFireGivenFireAndDistance(locations, cells = None, bins = bins)
		return np.sum(self.p_fire_given_fire_and_distance, axis = 0)/(self.patterns_1.shape[-1]*1.)
	
	def getPFireGivenSilentAndDistanceAll(self, locations, bins = None):
		self.calcPFireGivenSilentAndDistance(locations, bins = bins ,  cells = None)
		return np.sum(self.p_fire_given_silent_and_distance, axis = 0)/(self.patterns_1.shape[-1]*1.)
	
	def getPFireGivenFireAndDistanceWeightedDistancesAll(self, locations, bins = None):
		self.calcPFireGivenFireAndDistanceWeightedDistances(locations, bins = bins , cells = None)
		return np.sum(self.p_fire_given_fire_and_distance_weighted_distance, axis = 0)/(self.patterns_1.shape[-1]*1.)
	
	def getPFireGivenSilentAndDistanceWeightedDistancesAll(self, locations, bins = None):
		self.calcPFireGivenSilentAndDistanceWeightedDistances(locations, bins = bins , cells = None)
		return np.sum(self.p_fire_given_silent_and_distance_weighted_distance, axis = 0)/(self.patterns_1.shape[-1]*1.)
	
	def calcEigenValuesCovariance(self): #calc ev and ew form covariance matrix of patterns_1, where each column is one observation (data point); e-values and e-vectors are ordered, may first
		
		cov = np.cov(self.patterns_1.T)#patterns_1 is (observation, variable), thus transpose it to (variabele, observation) so that column become data point
		print 'calc covariance'

		w ,v = np.linalg.eig(cov) #w[i] = eigenvalue i and v[:,i] eigenvector of i, thus the columns of v are the eigenvectors
		arg = np.argsort(w)[::-1]
		self.covariance_ew = w[arg]
		self.covariance_ev = v[:, arg]

	def getEigenvaluesCovariance(self, number_relevant= None): #returns the eigenvalues of the covariance of patterns; each colum of pattern is one datapoint
		
		if number_relevant == None:
			number_relevant = self.patterns_1.shape[1]
		if self.covariance_ew == None:
			self.calcEigenValuesCovariance()
		return self.covariance_ew[:number_relevant]

	def getEigenvectorsCovariance(self, number_relevant = None):#patterns = (variable ,observation)
		
		if number_relevant == None:
			number_relevant = self.patterns_1.shape[1]
		if self.covariance_ew == None:
			self.calcEigenValuesCovariance()
		return self.covariance_ev[:,:number_relevant]
		
	def calcPCA(self, number_relevant = None):
		self.PCA = decomposition.PCA(n_components = number_relevant)
		self.PCA.fit(self.patterns_1)
		
	def getProjectedCor(self, number_relevant = None, patterns_to_project_1 = None, patterns_to_project_2 = None, project_just_1 = 0):
		self.calcPCA(number_relevant)
		projected_patterns_1 = self.PCA.transform(patterns_to_project_1)
		if not project_just_1:
			projected_patterns_2 = np.zeros([patterns_to_project_2.shape[0],patterns_to_project_2.shape[1],number_relevant] )
			for i in range(patterns_to_project_2.shape[0]):
				projected_patterns_2[i] = self.PCA.transform(patterns_to_project_2[i])
		else:
			projected_patterns_2 = patterns_to_project_2
		return Corelations(patterns_1 = projected_patterns_1, patterns_2 = projected_patterns_2, env = 0)
		
	
	
	
	
	def getEucledeanDifferenceAfterProjection(self,patterns=None, number_relevant = None): #return av euclidean norm of normalized patterns (data as colums) and projected patterns
		
		p = self.getProjectionMatrix(number_relevant)
		normalize(patterns.T)
		projected = np.dot(p, patterns*1.)
		diff = patterns - projected
		diff_norm_sq = np.dot(diff.T, diff)
		av_norm = np.sum(np.sqrt(np.diagonal(diff_norm_sq)))/(diff.shape[0]*1.)
		return av_norm
		
	def getCorrelationAfterProjection(self,patterns=None, number_relevant = None): #return correlation of patterns (colums) and projected patterns
		
		p = self.getProjectionMatrix(number_relevant)
		projected = np.dot(p, patterns*1.)
		av_norm = Corelations(patterns_1 = patterns, patterns_2 = projected, in_columns = True).getOrigVsOrig()
		return av_norm
		
	def getWhiteData(self):
		if self.covariance_ew == None:
			self.calcEigenValuesCovariance()
		

		ew = np.sqrt(np.copy(self.covariance_ew))
		ew[ew == 0] = np.min(ew[ew != 0])
		d = np.diag(1./ew)
		return np.dot(d, np.dot(self.covariance_ev.T, self.patterns_1.T))
		
		
		
	def getFireTimes(self): #returns array that indicate number of times each cell fires in all pattern
		binary_pattern = np.copy(self.patterns_1)
		binary_pattern[binary_pattern !=0] = 1
		return np.sum(binary_pattern, axis = 0)
		
	def getCellIndizesMaxFire(self, number_cells = 1): #return number_of_cell cells that fire most often during all pattern; 
		fire_times = self.getFireTimes()
		return np.argsort(fire_times)[-number_cells:]
		
	def getCellIndizesMinFire(self, number_cells = 1): #return number_of_cell cells that fire at fewest during all pattern
		fire_times = self.getFireTimes()
		return np.argsort(fire_times)[:number_cells]
	
	def getSpatialStored(self,cell = None, recall = False, at_noise = None): # return firing of cell over enviroment
	
		if recall:
			fire = self.patterns2[at_noise,:,cell]
		else:
			fire = self.patterns_1[:,cell]
		return fire

	def getMeanOver(self, at_noise=None):
		
		self.calcOverlaps(noise_level = at_noise)
		overlaps = np.copy(self.overlaps[at_noise])
		
		if self.one_patterns:
			overlaps[np.tril_indices(overlaps.shape[-1])] =0
			mean = np.sum(overlaps)/((overlaps.shape[-1]**2- overlaps.shape[-1]+0.0)/2.)
		else:
			overlaps[np.diag_indices(overlaps.shape[-1])] = 0
			mean = np.sum(overlaps)/(overlaps.shape[-1]**2- overlaps.shape[-1]+0.0)
		return mean

	def getMeanOverAllNoise(self):
		self.calcOverlaps()
		return np.array(map(self.getMeanOver, range(self.overlaps.shape[0])))

	def getMaxIndRow(self, at_noise = 0): #returns column index from each row of self.corelations where cor is max 
		
		self.calcCor(noise_level = at_noise)
		over = np.round(self.corelations[at_noise],2)
		max_ind_row = np.argsort(over)[:,-1]
		return max_ind_row
		
	def getMaxIndCol(self, at_noise = 0): #returns row index from each column of self.corelations where cor is max 
		
		self.calcCor(noise_level = at_noise)
		over = np.round(self.corelations[at_noise],2).T
		max_ind_col = np.argsort(over)[:,-1]
		return max_ind_col
	
	def getNotMaxIndRow(self, at_noise = 0): #returns the row indizes of self.corelations in which the maximum is not on the diagonal; thus, the rows, in which the corrlation between orig orig is smaller than at least one corelation orig other 
		
		max_ind_in_row = self.getMaxIndRow(at_noise) #column index of each row with maximal argument; dimension = row
		r = np.arange(self.corelations.shape[-1])
		not_max_row = r[max_ind_in_row != r]# each row where the max ind is not on the diagonal
		return not_max_row
		
	def getNotMaxIndCol(self, at_noise = 0): #returns the col indizes of self.corelations in which the maximum is not on the diagonal; thus, the cols, in which the corrlation between orig orig is smaller than at least one corelation orig other 
		
		max_ind_in_col = self.getMaxIndCol(at_noise) #column index of each row with maximal argument; dimension = row
		r = np.arange(self.corelations.shape[-1])
		not_max_col = r[max_ind_in_col != r]# each row where the max ind is not on the diagonal
		return not_max_col
	
	def getNotMaxCorrelationsRow(self, at_noise = 0): #returns the corelation <p,~p>, whenver there is a q!= p such that <p,~q> > <p,~p>
		cor = self.getCor(at_noise = at_noise)
		not_max_row = self.getNotMaxIndRow(at_noise)
		return cor[not_max_row, not_max_row]
		
	
	def getNotMaxCorrelations(self, at_noise = 0): #returns the corelation <p,~p>, whenver there is a q!= p such that <q,~p> > <p,~p>
		cor = self.getCor(at_noise = at_noise)
		not_max_col = self.getNotMaxIndCol(at_noise)
		return cor[not_max_col, not_max_col]

	def getProportionCompletedToWrong(self, at_noise = None):
		if at_noise == None:
			return_value = np.zeros(self.patterns_2.shape[0])
			noise = 0
			for noise in range(self.patterns_2.shape[0]):
				return_value[noise] = np.round(self.getNotMaxIndCol(noise).shape[0]*1./self.patterns_1.shape[0], 3) *100

		else:
			return_value = np.round(self.getNotMaxIndCol(at_noise).shape[0]*1./self.patterns_1.shape[0], 3) *100
		return return_value
	
	
	def plotCorrelationsHeatMap(self,at_noise = 0, pattern = None, In = None, fig = None, ax_index = None, title = None):
		
		ax = fig.add_subplot(ax_index[0], ax_index[1],ax_index[2], aspect = 'equal')
		loc = In.locations[In.store_indizes]		
		cors = self.getCor(at_noise=at_noise)[:,pattern]
		ax.set_title(title)
		ax.set_ylim(-0.02,0.962)
		ax.set_xlim(-0.02,0.962)
		ax.scatter(loc[0][:,0], loc[0][:,1], c = cors, marker = 's', s = 150, faceted = False, cmap=cm.jet)
		#fig.colorbar()
		#mpl.colorbar.ColorbarBase(mpl.colorbar.make_axes(ax)[0])
		ax.scatter(loc[0][pattern,0], loc[0][pattern,1], c = 'k', s= 600, alpha = 1, facecolors = 'None', edgecolor = 'k')
	
	
	def plotCorrelationsHeatMapRow(self,at_noise = 0, pattern = None, In = None, fig = None, ax_index = None, title = None):
		
		ax = fig.add_subplot(ax_index, aspect = 'equal')
		loc = In.locations[In.store_indizes]		
		cors = self.getCor(at_noise=at_noise)[pattern]
		ax.set_title(title)
		ax.set_ylim(0,1)
		ax.set_xlim(0,1)
		ax.scatter(loc[0][:,0], loc[0][:,1], c = cors, marker = 's', s = 140, faceted = False, cmap=cm.jet)
		#fig.colorbar()
		mpl.colorbar.ColorbarBase(mpl.colorbar.make_axes(ax)[0])
		ax.scatter(loc[0][pattern,0], loc[0][pattern,1], c = 'k', s= 600, alpha = 1, facecolors = 'None', edgecolor = 'k')

	def getSimilarPatterns(self, correlation =0.3):
		cor = self.getCor(at_noise = 0)
		similar = Set([])
		for i in range(cor.shape[0]):
			if i not in similar: # if pattern i not have been removed yet
				similar = similar.union(np.flatnonzero(cor[i,i+1:]>= correlation) + i+1) # indices that are similar to pattern i, but have not been considered at previous i. We add i to get original indices.
		return similar
		
	def getSimilarPatternsOverlapNoMean(self, overlap = 100):
		over = self.getOverOrigOtherNoMean(at_noise = 0)
		similar = Set([])
		for i in range(cor.shape[0]):
			if i not in similar: # if pattern i not have been removed yet
				similar = similar.union(np.flatnonzero(over[i,i+1:]>= overlap) + i+1) # indices that are similar to pattern i, but have not been considered at previous i. We add i to get original indices.
		return similar
		
	def getDifferentPatterns(self, correlation = 0.3):
		similar = self.getSimilarPatterns(correlation = correlation)
		different = Set(range(self.corelations.shape[-1])) - similar
		return list(different)
		
	def getCorSpecialPatterns(self, indices = None):
		Cor = Corelations(patterns_1 = self.patterns_1[indices], patterns_2 = np.tile(self.patterns_2[:,indices], (1,1,1,1)))
		return Cor
		
	def getCorDifferentPatterns(self, correlation = 0.3):
		similar = self.getSimilarPatterns(correlation = correlation)
		different = Set(range(self.corelations.shape[-1])) - similar
		Cor = Corelations(patterns_1 = self.patterns_1[list(different)])
		return Cor
			
	def getOverNoMeanActiveCells(self, output_patterns):#get <p_1, p_2 - bar p> for all p such that q_i^1 = q^2 = 1
		
		self.calcOverlapsNoMean(noise_level = 0)
		over = np.copy(self.overlapsNoMean[0])
		over[np.diag_indices_from(over)] = -10**10 #to exclude overlap of pattern with itself later
		p_1 = np.array([], 'int') #inidzes pattern_1
		p_2 = np.array([], 'int')#inidzes pattern_2
		cells = np.array([], 'int')#cell index that fire

		for cell in range(output_patterns.shape[-1]):
			
			non_z_cell = np.flatnonzero(output_patterns[:,cell]) #cells that fire together with cell
			size = non_z_cell.shape[0]
			p_1 = np.concatenate((non_z_cell.repeat(size, axis = 0),p_1))# index of pattern 1, where cell fires in. It is repeated, since it is used for all other indices in non_z_cell
			p_2 = np.concatenate((np.tile(non_z_cell, size), p_2))# index of pattern 2, where cell fires in
			cells = np.concatenate((np.ones(size*size, 'int')*cell, cells))#cell index repeated as often as cell is part of one overlap pair
		r_value = over[p_1,p_2] * output_patterns[p_2, cells]
		return r_value[r_value> -10**3]
		
	def getOverNoMeanSilentCells(self, output_patterns): #get <p_1, p_2 - bar p> for all p such that q_i^1 = q^2 = 1
		
		self.calcOverlapsNoMean(noise_level = 0)
		over = np.copy(self.overlapsNoMean[0])
		over[np.diag_indices_from(over)] = -10**10 #to exclude overlap of pattern with itself later
		p_1 = np.array([], 'int') #inidzes pattern_1
		p_2 = np.array([], 'int')#inidzes pattern_2
		cells = np.array([], 'int')#cell index that fire
		for cell in range(output_patterns.shape[-1]):
			
			#output_patterns[:,cell]
			non_z_cell = np.flatnonzero(output_patterns[:,cell]) #cells that fire together with cell
			z_cell = np.flatnonzero(output_patterns[:,cell] == 0)
			size_non = non_z_cell.shape[0]
			size_z= z_cell.shape[0]
			p_1 = np.concatenate((z_cell.repeat(size_non, axis = 0),p_1))#index where cell of patterns in which it is silent. It is repeated to compare it with all the indices in non_z_cell.
			p_2 = np.concatenate((np.tile(non_z_cell, size_z), p_2))#index where cell of patterns in which it fires. It is repeated to compare it with all the indices in z_cell.
			cells = np.concatenate((np.ones(size_non*size_z, 'int')*cell, cells))#cell index repeated as often as cell is part of one overlap pair

		r_value = over[p_1, p_2] * output_patterns[p_2, cells]#overlaps of paris, weighted by the outputfiring
		return r_value[r_value> -10**3]

	def getCorsWithin(self,size = None, at_noise = 0, n_e = None, subtract_orig_orig = 1):
		self.calcCor(noise_level = at_noise)
		for h in range(n_e):
			ind = np.mgrid[h*size:(h+1)*size, (h+1)*size:self.patterns_1.shape[-2]]
			self.corelations[at_noise][ind[0], ind[1]] = -100
			self.corelations[at_noise][ind[1], ind[0]] = -100
			ind = np.mgrid[h*size:(h+1)*size, 0:h*size]
			self.corelations[at_noise][ind[0], ind[1]] = -100
			self.corelations[at_noise][ind[1], ind[0]] = -100
		cors = self.getCorOrigOther(at_noise=at_noise, subtract_orig_orig = subtract_orig_orig)
		self.calculated_cor = False #make sure self.cor is calculated later again
		self.corelations[at_noise] = 0
		return cors[cors!= -100]
		
	def getDistancesWithin(self,locations = None, subtract_orig_orig = 1):
		distances = np.copy(self.getDistanceMatrix(locations = locations, round_it = 0))
		n_e, size = np.shape(locations)[:-1]
		for h in range(n_e):
			ind = np.mgrid[h*size:(h+1)*size, (h+1)*size:self.patterns_1.shape[-2]]
			distances[ind[0], ind[1]] = -100
			distances[ind[1], ind[0]] = -100
			ind = np.mgrid[h*size:(h+1)*size, 0:h*size]
			distances[ind[0], ind[1]] = -100
			distances[ind[1], ind[0]] = -100		
		if self.one_patterns:
			if subtract_orig_orig:
				return_value = distances[np.tril_indices(distances.shape[-1], -1)]
			else:
				return_value = distances[np.tril_indices(distances.shape[-1], 0)]
		else:
			if subtract_orig_orig:
				cor_lower = distances[np.tril_indices(distances.shape[-1], -1)]
				cor_upper = distances[np.triu_indices(distances.shape[-1], 1)]
				return_value = np.zeros(distances.shape[-1]**2 -distances.shape[-1])
				return_value[:(distances.shape[-1]**2 -distances.shape[-1])/2] = cor_lower
				return_value[(distances.shape[-1]**2 -distances.shape[-1])/2:] = cor_upper
			else:
				return_value = np.ravel(distances)
		self.distance_matrix = None
		return return_value[return_value!= -100]
	


	def getCorsAcross(self,size = None, at_noise = 0, n_e = None, subtract_orig_orig = 1):
		self.calcCor(noise_level = at_noise)
		for h in range(n_e):
			ind = np.mgrid[h*size:(h+1)*size, h*size:(h+1)*size]
			self.corelations[at_noise][ind[0], ind[1]] = -100
		cors = self.getCorOrigOther(at_noise=at_noise, subtract_orig_orig = subtract_orig_orig)
		self.calculated_cor = False #make sure self.cor is calculated later again
		self.corelations[at_noise] = 0
		return cors[cors!= -100]
		
	def getDistancesAcross(self,locations = None, subtract_orig_orig = 1):
		#self.distance_matrix = None
		distances = np.copy(self.getDistanceMatrix(locations = locations, round_it = 0))
		n_e, size = np.shape(locations)[:-1]
		for h in range(n_e):
			ind = np.mgrid[h*size:(h+1)*size, h*size:(h+1)*size]
			distances[ind[0], ind[1]] = -100
		if self.one_patterns:
			if subtract_orig_orig:
				return_value = distances[np.tril_indices(distances.shape[-1], -1)]
			else:
				return_value = distances[np.tril_indices(distances.shape[-1], 0)]
		else:
			if subtract_orig_orig:
				cor_lower = distances[np.tril_indices(distances.shape[-1], -1)]
				cor_upper = distances[np.triu_indices(distances.shape[-1], 1)]
				return_value = np.zeros(distances.shape[-1]**2 -distances.shape[-1])
				return_value[:(distances.shape[-1]**2 -distances.shape[-1])/2] = cor_lower
				return_value[(distances.shape[-1]**2 -distances.shape[-1])/2:] = cor_upper
			else:
				return_value = np.ravel(distances)
		self.distance_matrix = None
		return return_value[return_value!= -100]


################################################Input Classes ######################################################
class Input(Corelations):
	
	'''
	
	Class that creates pattern and builts noisy versions of them.
	
	:param number_cells: number of cells
	:type number_cells: int
	:param n_e: Number of environments
	:type n_e: int
	:param number_patterns: number of total pattern this instance creates
	:type number_patterns: int
	:param store_indizes: indices of patterns that are considered for storing
	:type store_indizes: array of dimension 2 (environment, indices)
	:param number_to_store: number of patterns that are considered for storing
	:type number_to_store: int
	:param inputMethod: How patterns are created
	:type inputMethod: Input.makeInput method
	:param noiseMethod: How noise is created
	:type noiseMethod: Input.makeNoise method
	:param actFunction: How activation is transformed into firing
	:type actFunction: Input.getOutput method
	:param sparsity: Proportion of cells beeing active in each pattern; for getOutputWTA necessary
	:type sparsity: float
	:param noise_levels: levels of noise beeing applied on the pattern; mostly number of cells firing wrongly
	:type noise_levels: list or array of noise_levels
	:param normed: Whether patterns are normalized
	:type normed: bbol
	:param In: When given it uses its paramters
	:type In: Instance of Input
	:param patterns: When given, it uses these patterns as self.patterns
	:type patterns: array of dimension 3 (environment, pattern, cell fire)
	'''
	def __init__(self, number_cells = Parameter.cells['Ec'], n_e = Parameter.n_e,  number_patterns= Parameter.no_pattern, store_indizes = None, number_to_store = None, inputMethod = None, noiseMethod = None, actFunction = None, sparsity = Parameter.sparsity['Ec'], noise_levels = Parameter.noise_levels, normed = 0, In = None, patterns = None, lec_cells = 0, **kwargs):


		self.n_e = n_e
		self.number_patterns = number_patterns #per env
		self.number_to_store = number_to_store #per env
		self.inputMethod = inputMethod
		self.noiseMethod = noiseMethod
		self.actFunction = actFunction
		self.store_indizes = store_indizes

		self.cells = number_cells
		self.noise_levels = np.array(noise_levels)
		self.sparsity = sparsity
		self.n_lec = lec_cells
		
		self.number_winner = int(sparsity*self.cells)


		if number_to_store ==  None:
			self.number_to_store = self.number_patterns
		if In != None: #uses Parameter of given Input Instance
			self.n_e = In.n_e
			self.number_patterns = In.number_patterns #per env
			self.number_to_store = In.number_to_store #per env
			self.inputMethod = In.inputMethod
			self.noiseMethod = In.noiseMethod
			self.actFunction = In.actFunction
		if patterns == None: # uses given patterns as self.patterns
			self.patterns = np.zeros([self.n_e,self.number_patterns, self.cells])
			self.patterns_given = False
		else:
			self.patterns = patterns
			self.patterns_given = True
		
		self.input_stored = np.zeros([self.n_e, self.number_to_store, self.cells])#patterns that are considered for storing
		self.noisy_input_stored = np.zeros([self.n_e,self.noise_levels.shape[0], self.number_to_store, self.cells])#noisy versions of self.input_stored

		#creates self.patterns

		self.makeInput(**kwargs)
		if normed:
			normalize(self.patterns)
		#create self.input_stored and self.noisy_imput_stored
		self.choosePatternToStore(store_indizes = store_indizes)
	
		#Input Instance is a Corelation Instance at the same time.
		super(Input, self).__init__(patterns_1 = self.input_stored, patterns_2= self.noisy_input_stored)
		
	def makeInput(self, **kwargs):
		'''
		
		creates the input; all input patterns are self.patterns. It has dimensions (envionment, pattern, cell firing)
		'''
		if not self.patterns_given:
			if self.inputMethod == None:
				print 'noinput Method gven'
				self.makeInputSparsityLevel()
			else:
				self.inputMethod(self)

		else:
			pass
		
	def makeNoise(self, env = None):
		'''
		
		creates the noisy version of the input pattern self.patterns.
	
		:param env: Environment for which noise is created
		'''
		if self.noiseMethod == None:
			noise =self.makeNoiseRandomFire(pattern = self.patterns[env], noise_levels= self.noise_levels)
		else:
			noise = self.noiseMethod(self, pattern = self.patterns[env], noise_levels = self.noise_levels)
			
		return noise
	
	def choosePatternToStore(self, number_to_store = None, store_indizes = None):
		'''
		
		sets the input patterns in self.patterns that are going to be stored. These patterns are then self.input_stored and their noisy version are in self.noisy_input_stored. 
	
		:param number_to_sore: How many patterns are stored; if None, self.number_to_store is used
		:type number_to_store: int
		:param store_indizes: Indices of patterns that are stored. If None, store_indizes are created by self.makeStoreIndizes(store_indizes)
		:type store_indizes: array of dimension two; (environment, indizes)
		'''
		if number_to_store != None:
			self.number_to_store = number_to_store
		self.makeStoreIndizes(store_indizes)
		self.input_stored = np.zeros([self.n_e, self.number_to_store, self.cells])
		self.noisy_input_stored = np.zeros([self.n_e,self.noise_levels.shape[0], self.number_to_store, self.cells])
		for h in range(self.n_e):
			self.input_stored[h] = self.patterns[h][self.store_indizes[h]] #the pattern that are actually given as inputs; note that self.input_stored[i] corresponds to self.location[self.store_indized][i]
			self.noisy_input_stored[h] = self.makeNoise(h)[:,self.store_indizes[h]]

	def makeStoreIndizes(self, store_indizes=None):
		'''
		
		creates store indizes randomly if store_indizes = None. If store_inidizes != None, these indizes are used.
		
		:param store_indizes: Indices of patterns that are stored. If None, store_indizes are created by self.makeStoreIndizes(store_indizes)
		:type store_indizes: array of dimension two; (environment, indizes)
		'''
		if self.number_to_store > self.number_patterns:
			print 'not enpugh input patterns'
		if store_indizes == None:
			print 'make store indizes'
			self.store_indizes = np.array(map(random.sample, [range(self.patterns.shape[1])]*self.n_e, [self.number_to_store]*self.n_e))
		else:
			self.store_indizes = store_indizes
			print 'store indizes given'
		
	def makeNewNoise(self, method = None):
		'''
		
		creates noisy patterns, when Input Instance was already created. Old noise is overwritten
		
		:param method: Which method is used for creation of noise
		:type method: Input.makeNoise method.
		'''
		self.noiseMethod = method
		self.noisy_input_stored = np.zeros([self.n_e,self.noise_levels.shape[0], self.number_to_store, self.cells])
		for h in range(self.n_e):
			self.noisy_input_stored[h] = self.makeNoise(h)[:,self.store_indizes[h]]
		super(Input, self).__init__(patterns_1 = self.input_stored, patterns_2= self.noisy_input_stored)
		
	
	#################inputMethods#########################
	##creates the input patterns self.patterns
	def makeInputNormalDistributed(self):
		'''
		
		creates patterns, each cell activity is a sample of a normal distribution
		'''
		activity = np.random.normal(loc = 1, scale = 1, size = self.patterns.shape)
		self.patterns = self.actFunction(self, activity = activity)
		
	def makeInputSparsityLevel(self):
		'''
		
		creates patterns, the proportion of cell that fire in each pattern is self.sparsity. These cells have value 1, the others 0
		'''
		active_units = np.int(self.sparsity*self.patterns.shape[-1])
		for h in range(self.n_e):
			for p in self.patterns[h]:
				p[np.array(random.sample(range(self.patterns.shape[-1]), active_units))] = 1
	
	def makeInputExponential(self):
		'''
		
		creates patterns; cells fire according to an exponetial distribution. The proportion of cells firing is self.sparsity 
		'''
		silent_p = 1-2*self.sparsity
		uniform = np.random.uniform(0,1, (self.patterns.shape))
		silent_cells = uniform <= silent_p
		activity = scipy.stats.expon.rvs(size = self.patterns.shape)*2
		activity[silent_cells] = 0
		a = Network.calcFireSparsity(activity)
		self.patterns = activity
	
	
	#####################noiseMethods##########################################
	# These methods are used to create noisy version of given patterns
	def makeNoiseAccordingSparsity(self,pattern=None, noise_levels=None):
		'''
		
		Each noise cell fires with P(fire) = self.sparsity. A cell that fires have valeu 1 others 0
	
		:param pattern: Patterns which are made noisy. 
		:type pattern: array of dimension 2 (pattern, cell fire)
		:param noise_levels: levels of how much amount of noise is given to pattern. Here it is the number of cells that fire wrongly
		:type noise_levels: array of dimension 1
		:param return: noisy version of patterns
		:type return: array of dimension 3 (noise_level, pattern, cell fire)
		'''
		noise = np.tile(pattern, (len(noise_levels),1,1))
		j=1
		for i in noise_levels[1:]:
			wrong = np.array(map(random.sample, [range(pattern.shape[-1])]*pattern.shape[-2], [i]*pattern.shape[-2]))
			noise[j][np.mgrid[0:pattern.shape[0], 0:i][0], wrong] = np.random.uniform(0,1, size =(wrong.shape)) <= self.sparsity
			j +=1
		return noise

	def makeNoiseRandomFire(self,pattern=None, noise_levels=None): 
		'''
		
		Each noisy cell fires accroding to the rate of an arbritrayly chosen cell in that pattern
	
		:param pattern: Patterns which are made noisy. 
		:type pattern: array of dimension 2 (pattern, cell fire)
		:param noise_levels: levels of how much amount of noise is given to pattern. Here it is the number of cells that fire wrongly
		:type noise_levels: array of dimension 1
		:param return: noisy version of patterns
		:type return: array of dimension 3 (noise_level, pattern, cell fire)
		'''
		noise = np.tile(pattern, (len(noise_levels),1,1))
		j=1
		for i in noise_levels[1:]:
			wrong = np.array(map(random.sample, [range(pattern.shape[-1])]*pattern.shape[0], [i]*pattern.shape[0])) # for each pattern a random set of cells is chosen to fire wrongly 
			noise[j][np.mgrid[0:pattern.shape[0], 0:i][0], wrong] = np.array(random.sample(np.ravel(pattern), i*pattern.shape[0])).reshape(wrong.shape)
			j +=1
		return noise

	def makeNoiseZero(self,pattern=None, noise_levels=None): 
		'''
		
		Each noisy cell becomes silent. 
	
		:param pattern: Patterns which are made noisy. 
		:type pattern: array of dimension 2 (pattern, cell fire)
		:param noise_levels: levels of how much amount of noise is given to pattern. Here the noise_levels increase linearly from 0 to number_winner in noise_levels.shape[0] steps. Each level determines, how many cells that fired before are silent in the noisy pattern
		:type noise_levels: array of dimension 1
		:param return: noisy version of patterns
		:type return: array of dimension 3 (noise_level, pattern, cell fire)
		'''
		max_noise = pattern.shape[-1]
		for p in pattern:
			max_noise = min(np.flatnonzero(p).shape[0], max_noise)
		self.noise_levels = np.array(np.linspace(0, max_noise-1, noise_levels.shape[0]), dtype = 'int')
		noise = np.tile(pattern, (len(noise_levels),1,1))
		level=1

		for i in self.noise_levels[1:]:
			for j in range(pattern.shape[0]):
				wrong = random.sample(np.flatnonzero(pattern[j]), i)
				noise[level,j][wrong] = 0
			level +=1

		return noise
	
	
	#later important
	def makeNoiseVector(self,pattern=None, noise_levels=None):
		'''
		
		For each noise_level a noise vector is added to the pattern. The noise vector is a random vector created by a normal distribution.
	
		:param pattern: Patterns which are made noisy. 
		:type pattern: array of dimension 2 (pattern, cell fire)
		:param noise_levels: levels of how much amount of noise is given to pattern. Here it is the number of noise vectors added.
		:type noise_levels: array of dimension 1
		:param return: noisy version of patterns
		:type return: array of dimension 3 (noise_level, pattern, cell fire)
		'''
		noise = np.tile(pattern, (len(noise_levels),1,1))
		rand_vectors = np.random.normal(size = (len(noise_levels), self.cells))
		normalize(rand_vectors)
		rand_vectors *= 10
		noise_vector = rand_vectors[0]
		for i in range(len(noise_levels)-1):
			noise[i+1] += noise_vector
			noise_vector += rand_vectors[i+1]
		return noise
	
	def makeNoiseCovariance(self,pattern=None, noise_levels=None):
		'''
		
		For each noise_level a noise vector is added to the pattern. The first noise vector is the eigenvector with the largest eigenvalue of the covariance matrix of self.patterns. The second is the sum of the first two and so on.  
		
		:param pattern: Patterns which are made noisy. 
		:type pattern: array of dimension 2 (pattern, cell fire)
		:param noise_levels: levels of how much amount of noise is given to pattern. Here it is the number of noise vectors added.
		:type noise_levels: array of dimension 1
		:param return: noisy version of patterns
		:type return: array of dimension 3 (noise_level, pattern, cell fire)
		'''
		noise = np.tile(pattern, (len(noise_levels),1,1))
		Cor = Corelations(patterns_1 = self.patterns)
		ev = Cor.getEigenvectorsCovariance().T
		normalize(ev)
		ev *= 10
		noise_vector = ev[0]
		for i in range(len(noise_levels)-1):
			noise[i+1] += noise_vector
			noise_vector += ev[i+1]
		return noise
		
	###################### getOutputFunctions #################
	### Similar as in class Network
	#important for you
	def getOutputWTA(self, activity = None): #returns the winner of the network given the input patterns; if not discrete, output is the membrane potential of the winners
		'''
		
		calculates outputfiring  given activity; the highest self.number_winner activated neurons fire;
	
		:param activtiy: activity
		:type activity: array of max 4 dimension, last one must be self.connection.shape[1]
		:param return: array
		'''
		size = activity.shape
		winner = np.argsort(activity)[...,-self.number_winner:size[-1]]
		fire_rate = np.ones(size, 'bool')
		out_fire = np.zeros(size, 'bool')
			
		if len(size) ==1:
			out_fire[winner] = fire_rate[winner]
		if len(size) ==2:
			out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
		if len(size) ==3:
			indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
			out_fire[indices[0], indices[1], winner] =1#fire_rate[indices[0], indices[1], winner]
		if len(size) ==4: # env, noise, time, pattern
			indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
			out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
		if len(size) > 4:
				print 'error in input dimension in calckWTAOutput'
		return out_fire
	
	def getOutputWTALinear(self, activity = None): #returns the winner of the network given the input patterns; if not discrete, output is the membrane potential of the winners
		'''
		
		calculates outputfiring  given activity; the highest self.number_winner activated neurons fire;
		
		:param activtiy: activity
		:type activtiy: array of max 4 dimension, last one must be self.connection.shape[1]
		:param return: array
		'''
		size = activity.shape
		winner = np.argsort(activity)[...,-self.number_winner:size[-1]]


		fire_rate = activity
		fire_rate[fire_rate<0] = 0
		out_fire = np.zeros(size)
			
		if len(size) ==1:
			out_fire[winner] = fire_rate[winner]
		if len(size) ==2:

			out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
		if len(size) ==3:
			indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
			out_fire[indices[0], indices[1], winner] =fire_rate[indices[0], indices[1], winner]
		if len(size) ==4: # env, noise, time, pattern
			indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
			out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
		if len(size) > 4:
				print 'error in input dimension in calckWTAOutput'
		return out_fire
	
	#not important
	def getOutputWTARolls(self, activity = None):
		'''
		
		calculates outputfiring  given activity; the highest self.number_winner activated neurons fire;
	
		:param activtiy: activity
		:type activtiy: array of max 4 dimension, last one must be self.connection.shape[1]
		:param return: array
		'''
		size = activity.shape
		winner = np.argsort(activity)[...,-self.number_winner:size[-1]]

		max_activity_pattern = np.max(activity, -1)
		fire_rate = max_activity_pattern.repeat(activity.shape[-1]).reshape(activity.shape)
		out_fire = np.zeros(size)
			
		if len(size) ==1:
			out_fire[winner] = fire_rate[winner]
		if len(size) ==2:
			out_fire[np.mgrid[0:size[0], 0:self.number_winner][0], winner] = fire_rate[np.mgrid[0:size[0], 0:self.number_winner][0], winner]
		if len(size) ==3:
			indices = np.mgrid[0:size[0],0:size[1],0:self.number_winner]
			out_fire[indices[0], indices[1], winner] =fire_rate[indices[0], indices[1], winner]
		if len(size) ==4: # env, noise, time, pattern
			indices = np.mgrid[0:size[0],0:size[1],0:size[2],0:self.number_winner]
			out_fire[indices[0], indices[1], indices[2], winner] = fire_rate[indices[0], indices[1], indices[2], winner]
		if len(size) > 4:
				print 'error in input dimension in calckWTAOutput'
		return out_fire

	def getOutputLinearthreshold(self, activity = None):
		
		activity[activity <0] = 0
		for k in np.linspace(np.min(activity),np.min(np.max(activity, -1)), 20):
			
			if k>=np.max(activity):
				print 'break precalc since otherwise all 0'
				break
			activity_help = np.copy(activity)
			activity_help[activity_help <= k] = 0
			a = Network.calcFireSparsity(activity_help)
			if (a < self.sparsity).any():
				print 'break precalc linthreshold output at k = '+str(k)
				break
			else:
				activity = activity_help
		
		if len(self.patterns.shape) >= 2:
			a = Network.calcFireSparsity(activity)
			not_sparse = True
			i=0
			while not_sparse:
				too_large_a_rows = a > self.sparsity
				if not too_large_a_rows.any():
					not_sparse = False
					break
				activity[activity == 0] = 10**10
				min_activity_cell = np.argmin(activity, -1) 
				activity[too_large_a_rows, min_activity_cell[too_large_a_rows]] = 0
				activity[activity == 10**10] = 0
				a = Network.calcFireSparsity(activity)
				i+=1	
		if len(self.patterns.shape) == 1:
			a = Network.calcFireSparsity(activity)
			not_sparse = True
			i=0
		
			while a > self.sparsity:
				min_activity = np.min(activity[activity !=0])
				activity[activity == min_activity] = 0
				a = Network.calcFireSparsity(activity)
				i+=1
			normalize(activity)
		return activity

	def getOutputId(self, activity = None):
		return activity

	def getOutputMinRate(self, activity = None, min_rate = 0):
		min_fire = np.max(activity, axis = -2) * min_rate
		min_fire = np.tile(min_fire , (activity.shape[-2],1)).reshape(activity.shape)
		activity[activity < min_fire] = 0
		return activity

	def getNoiseInd(self, at_corelation = None): # 
		'''
		
		return best index i where Input has at noise_level[i] correlation most similar to at_corlation
		'''
		noise_ind = np.argmin(np.abs(self.getOrigVsOrig() - at_corelation))
		return noise_ind

	def getInput(self):
			return self.patterns
	
	def getNoise(self):
		return self.noise
		
class Grid(Input):
	'''
	
	Input Class that creates Grid pattern.
	
	:param grid_mode: how grid parameter are set up. If 'linear' spacings increase linearly from 30-50cm, orientations are random. If 'modules' it has 4 Modules with similar spacings and orientations. Phase is in both cases randomly chosen
	:type grid_mode: 'linear' or 'modules'
	:param rat: When grid_mode is 'modules' it determines how modules are split up. rat = 0 it has only modules 1 and 2, rat=1 it has all 4, rat=2 it has only modules 3 and 4.
	:type rat: 0,1 or 2
	:param space: If grid_mode = 'linear' it uses space as spacings, rather than 30-50cm
	:type space: array with lenght equal to number_cells
	'''

	def __init__(self,  grid_mode = 'modules', rat =1, spacings = None, peak = None, theta = None, phase = None, cage = [1,1], r_to_s = 0.25,**kwargs):
		


		#Grid Attributes
		self.spacing = spacings
		self.peak_firing_rate = peak
		self.theta = theta
		self.phase = phase

		
		self.theta_given = 0
		self.spacing_given = 0
		self.peak_given = 0
		self.phase_given = 0
		if spacings != None:
			self.spacing_given = True
		if peak != None:
			self.peak_given = True
		if theta != None:
			self.theta_given = True
		if phase != None:
			self.phase_given = True


		self.grid_mode = grid_mode
		self.rat = rat
		self.cage = cage
		self.r_to_s =r_to_s # radius of field / spacing of cell; Defining field as everthing above 0.2 peak rate, then in Knierim r_to_s = .17. Using Hafting r_to_s is in [0.3, 0.32]; Supposing that at 0.21 * spacing fire rate is 0.5 * peak (as in deAlmeida), r_to_s = 0.32.
		self.k = self.r_to_s**2/np.log(5) #see calcActivity
		
		self.locations = None #x and y coordinates of all pixel 
		self.distance_matrix = None # matrix where entry (i,j) is the eucledean distance between location i and j
		self.makeLocations(number_patterns = kwargs['number_patterns'])
		Input.__init__(self, **kwargs)


		
	def makeLocations(self, number_patterns):
		if self.cage[0]> self.cage[1]:
			self.transposed_cage = True
			self.cage_proportion = self.cage[0]/self.cage[1]
		else:
			self.transposed_cage = False
			self.cage_proportion = self.cage[1]/self.cage[0]
			if int(self.cage_proportion)*self.cage[0] != self.cage[1]:
				print 'cage adjusted to', self.cage
		self.space_res = np.int(np.sqrt(number_patterns/self.cage_proportion))

		if int(self.space_res * self.cage_proportion*self.space_res) - self.space_res * self.cage_proportion*self.space_res != 0:
			print 'number_patterns not suitable'
			self.locations = None
		else:
			if self.transposed_cage:
				self.locations = np.ravel((np.mgrid[0:self.cage_proportion*self.space_res, 0:self.space_res] + 0.0), order = 'F').reshape(number_patterns, 2)
				self.locations[:,0] /= self.cage_proportion*self.space_res/self.cage[0]
				self.locations[:,1] /= self.space_res/self.cage[1]
				self.x_length= self.cage_proportion*self.space_res
				self.y_length = self.space_res
			#self.locations = self.locations.reshape(number_patterns, 2)
			else:
				self.locations = np.ravel((np.mgrid[0:self.space_res, 0:self.cage_proportion*self.space_res] + 0.0), order = 'F').reshape(number_patterns, 2)

				self.locations[:,0] /= self.space_res/self.cage[0]
				self.locations[:,1] /= self.cage_proportion*self.space_res/self.cage[1]
				self.y_length= self.cage_proportion*self.space_res
				self.x_length = self.space_res
				#self.locations = self.locations.reshape(number_patterns, 2)

		
	

	
	def makeInput(self,**kwargs):
		print 'kwargs in makeinput Grid', kwargs
		if self.n_lec >0:
			self.Lec = Lec( number_cells = self.n_lec, n_e = self.n_e,  number_patterns= self.number_patterns, store_indizes = self.store_indizes, number_to_store = self.number_to_store, inputMethod = self.inputMethod, noiseMethod = Lec.makeNoiseDoNothing, actFunction = Input.getOutputWTALinear, sparsity = self.sparsity, noise_levels = [0], cage = self.cage, **kwargs)
		self.makeGrid()
		self.patterns = self.calcGridFiring()

	def makeGrid(self): # makes grid; set ups the parameter
		
		self.n_grid = self.cells - self.n_lec
		ec_firing = np.zeros([self.n_e, self.space_res**2, self.cells]) # self.n_e = number enviromnents, self.space_res**2 = number of different firing rate in each enviromnent, self.cells = number grid cells
		if not self.spacing_given:
			self.spacing = np.zeros([self.n_e,self.n_grid]) +0.0	# space between two peaks in one grid cell
		if not self.theta_given:
			self.theta = np.zeros([self.n_e,self.n_grid])		# Orientation of the grid			
		if not self.peak_given:
			self.peak_firing_rate = np.ones([self.n_e,self.n_grid, 50, 50])# Peak firing rate
		if not self.phase_given:
			self.centers = np.zeros([self.n_e, self.n_grid, 2])	# Origin of the grid
		else:
			self.centers = np.ones([self.n_e, self.n_grid, 2]) * self.phase	# Origin of the grid
		self.field_size = np.zeros([self.n_e,self.n_grid])	# Field size in one grid cell
		self.theta_arc =np.zeros([self.n_e,self.n_grid])	#Orientation of the grid presented in Pi coordinates
		self.rotation = np.zeros([self.n_e,self.n_grid,2,2]) #Rotation Matrices; We first bulit always a grid with 0 orientation and then rotate the whole thing to the true orientation via these matrices


		# for each enviromnent define grid parameters similar found in literature; linear
		if self.grid_mode == 'linear':
			self.gridLinear()
		if self.grid_mode == 'modules':
			self.gridModules()
		
		# parameters needed for calculating the rate:
		self.cos_spacing = self.spacing*np.cos(np.pi/3) # how far i go to the right-left direction, when I move one grid point upwards
		self.sin_spacing = self.spacing*np.sin(np.pi/3) # how far i go to the up-down direction, when I move one grid point to the right
		#self.std = self.field_size**2/np.log(5) #modulates the Gauss according to place field size; 
		
	
	def gridLinear(self):
		'''
		
		Grid is made according to Hafting 2005:  with random origin (phase), random orientation, spacing increasing from 30-50cm.
		'''
		self.modules = np.array(np.linspace(0, self.n_grid, 5), 'int')
		for h in range(self.n_e):
			theta_change = np.random.uniform(0,60)
			origin_change = np.random.uniform(-.25, 0.25, size = 2)
			for i in range(self.n_grid):
				noise_spacing = 0#random.gauss(0, 0.01) #noise mean 0, varianz=1cm
				noise_field_size = 0#random.gauss(0, 0.01)#noise varianz=1cm
				if not self.peak_given:
					self.peak_firing_rate[h][i] = 1
				if h == 0:
					if not self.spacing_given:
						self.spacing[h][i] = 0.3 + (0.2/self.n_grid)*i + noise_spacing 	#Generate a baseline spacing, starting at 0.30m and increasing linearly to0.50m
					#if self.fields == None:
						#self.field_size[h][i] = 0.0977 + 0.0523*(1.0/self.n_grid)*i+ noise_field_size 	#Generate a baseline field size, starting at 9,77cm and increasing linearly 15cm; field size in radius; (area from 300-700cm**2)
					#else:
						#self.field_size[h][i] = self.fields[i]
					if not self.theta_given:
						self.theta[h][i] = random.uniform(0, 360)
					if not self.phase_given:
						self.centers[h][i] = np.array([random.uniform(0, 1),random.uniform(0, 1)])
				else:
					self.spacing[h][i] = self.spacing[0][i] + noise_spacing
					self.field_size[h][i] = self.field_size[0][i] + noise_field_size
					self.theta[h][i] = self.theta[0][i] + theta_change #+ random.gauss(0,2)
					self.centers[h][i] = self.centers[0][i] + origin_change
				self.theta_arc[h][i] = (2*np.pi*self.theta[h][i])/360.
				self.rotation[h][i] =[[np.cos(self.theta_arc[h][i]), np.sin(self.theta_arc[h][i])], [-np.sin(self.theta_arc[h][i]), np.cos(self.theta_arc[h][i])]]


	def gridModules(self):
		'''
		
		Grid is made according to Solstad 2012: It consists of 4 Modules, each having similar orientation and spacing. Phase is random.
		'''
		
		if self.rat ==0: #m1 and m2 only
			m1 = 19 + 2.5
			m2 = 30+2.5
			m3 = 0
			m4 = 0
			thres = np.array([m1, m1+m2, m1+m2+m3, 54.0001])/54.
			spacing_mean = [38.8, 48.4, 65., 98.4]
			spacing_var = [8,8,6,16]
			orient_mean = [-5, -5, 5, -5]
			orient_var = [3,3, 6, 3]
		
		
		if self.rat ==1:
			m1 = 19 + 2.5 #34.7%
			m2 = 30+2.5 #52.4%
			m3 = 5 #8%
			m4 = 3 #4.8%
			thres = np.array([m1, m1+m2, m1+m2+m3, 62.0001])/62.
			spacing_mean = [38.8, 48.4, 65., 98.4]
			#spacing_var = [8,8,6,16]
			spacing_var = [8,8,8,8]
			#orient_mean = [-5, -5, 5, -5]
			#orient_mean = np.random.uniform(0,60, size = 4)
			orient_mean = np.array([15,30,45,60])
			#orient_var = [3,3, 6, 3]
			orient_var = [3,3, 3, 3]
			
		if self.rat ==2:#only m3 and m4
			m1=0
			m2=0
			m3 = 5
			m4 = 3
			thres = np.array([m1, m1+m2, m1+m2+m3, 8.0001])/8.
			spacing_mean = [38.8, 48.4, 65., 98.4]
			spacing_var = [8,8,6,16]
			orient_mean = [-5, -5, 5, -5]
			orient_var = [3,3, 6, 3]
			#!!!!
			#orient_mean = [-5, -5, -5, -5]
			#orient_var = [3,3, 3, 3]
			
		self.modules = np.zeros(5, 'int')	
		choice = np.random.uniform(low = 0, high =1, size = self.n_grid) + 0.000001
		for i in range(1,self.modules.shape[0]):
			self.modules[i] = np.flatnonzero(choice <= thres[i-1]).shape[0]
			self.spacing[0][self.modules[i-1]:self.modules[i]] = np.random.normal(loc = spacing_mean[i-1], scale = np.sqrt(spacing_var[i-1]), size = self.modules[i]- self.modules[i-1])/100.
			###!!!!!!
			#self.theta[0] = np.random.uniform(0, 360, size = self.n_grid)
			self.theta[0][self.modules[i-1]:self.modules[i]] = np.random.normal(loc = orient_mean[i-1], scale = np.sqrt(orient_var[i-1]), size = self.modules[i]- self.modules[i-1])
		self.centers[0] = np.random.uniform(low =0, high = 1, size=(self.n_grid, 2))
		self.theta_arc[0] = (2*np.pi*self.theta[0])/360.
		for i in range(self.n_grid):
			self.rotation[0][i] =[[np.cos(self.theta_arc[0][i]), np.sin(self.theta_arc[0][i])], [-np.sin(self.theta_arc[0][i]), np.cos(self.theta_arc[0][i])]]
		
		if self.n_e >1:
			for h in range(1, self.n_e):
				theta_change = np.random.uniform(0,60, size = 4)
				origin_change = np.random.uniform(-.25, 0.25, size = (4,2))
				noise_spacing = 0#random.gauss(0, 0.01) #noise mean 0, varianz=1cm
				noise_field_size = 0#random.gauss(0, 0.01)#noise varianz=1cm
				for m in range(1,5):
					for i in range(self.modules[m-1],self.modules[m]):
						if not self.peak_given:
							self.peak_firing_rate[h][i] = 1
						self.spacing[h][i] = self.spacing[0][i] + noise_spacing
						self.field_size[h][i] = self.field_size[0][i] + noise_field_size
						self.theta[h][i] = self.theta[0][i] + theta_change[m-1]
						self.centers[h][i] = self.centers[0][i] + origin_change[m-1]
						self.theta_arc[h][i] = (2*np.pi*self.theta[h][i])/360.
						self.rotation[h][i] =[[np.cos(self.theta_arc[h][i]), np.sin(self.theta_arc[h][i])], [-np.sin(self.theta_arc[h][i]), np.cos(self.theta_arc[h][i])]]



	def calcActivity(self, h, location):
		'''
		 calculates and returns the activity in environment h at location location
		'''
		
		t_loc = np.einsum('ijk,ik->ij' ,self.rotation[h], (location - self.centers[h])) # shift and rotate location into coordinates, where the grid has origin at 0 and rotation 0
		k= np.floor(t_loc[:,1]/(self.sin_spacing[h])) #nearest vertex in up-down (y)  direction is the kth or k+1th one
		y1 = (t_loc[:,1] - k*self.sin_spacing[h]) # assume it is the kth one
		y2 = (t_loc[:,1] - (k+1)*self.sin_spacing[h]) # assume it is the k +1th one
		kx1 = np.round((t_loc[:,0]- k*self.cos_spacing[h])/(self.spacing[h])) 
		kx2 = np.round((t_loc[:,0]- (k+1)*self.cos_spacing[h])/(self.spacing[h]))
		x1 = (t_loc[:,0]-k*self.cos_spacing[h]- kx1*self.spacing[h])# nearest vertex in x direction for y1 (if it is the kth one)
		x2 = (t_loc[:,0]-(k+1)*self.cos_spacing[h]- kx2*self.spacing[h])# nearest vertex in x direction for y2
		#dis = np.minimum((x1*x1 + y1*y1), x2*x2+y2*y2) # = euclidean distance**2 to nearest peak
		
		arg = np.argmin(np.array([x1*x1 + y1*y1, x2*x2+y2*y2]), axis = -2)
		vertX = np.array([kx1, kx2], 'int')[arg, np.arange(arg.shape[0])]
		vertY = np.array([k, k+1], 'int')[arg, np.arange(arg.shape[0])]
		dis = np.array([[x1*x1 + y1*y1], [x2*x2+y2*y2]])[arg, np.zeros(arg.shape[0], 'int'), np.arange(arg.shape[0])]

		#activity= np.exp(-dis/self.std[h])*self.peak_firing_rate[h] # rate is calcualted acording to the distance
		activity = np.exp(-dis/(self.spacing[h]**2 * self.k))*self.peak_firing_rate[h][np.arange(vertY.shape[0]), vertX, vertY] # a = exp(-d**2/(s**2*k) as in Knierim, where k = r_to_s**2/log(5) defining everythin above 0.2 is in field


		return activity

		
	def calcGridFiring(self):
		'''
		
		returns population activity of grid over all environments and locations
		'''
		ec_firing = np.zeros([self.n_e, self.number_patterns, self.cells]) # self.n_e = number enviromnents, self.space_res**2 = number of different firing rate in each enviromnent, self.cells = number grid cells
		for h in range(self.n_e): # calc firing rate at each location in each enviroment
			activity = np.zeros([self.number_patterns, self.cells])
			activity[:, :self.n_grid] = np.array(map(self.calcActivity, [h]*self.number_patterns, self.locations))
			if self.n_lec >0:
				activity[:,self.n_grid:] = self.Lec.patterns[h]
			ec_firing[h] = self.actFunction(self, activity = activity)
		return ec_firing
	
	def getTrueRadius(self): #to avoid the smaller sizes aat the boarder, we compute the radius only of the largest field assuming all other have the same size
		if (self.cluster_size == 0).all():
			self.calcClusters()
		max_field_size_cell = np.max(self.cluster_size, axis = -1)
		return np.sqrt(max_field_size_cell/(self.x_length**2*np.pi))
		
	def getTrueRadiusAverage(self):
		return np.sum(self.getTrueRadius())/(self.cells+0.0)
		
	def getStoredlocations(self):
		'''
		
		returns the locations in which pattern are stored
		'''

		return self.locations[self.store_indizes]

class PlaceFields(Input):
	
	
	'''
	
	Input Class that creates pattern with place cells.

	:param no_fields: Number place fields per cell
	:type no_fields: int
	:param field_size: size of each field; Note if size of fields depend on self.sparsity for certain actFunction methods!
	:type field_size: int
	:param ordered: determines whether field centers of the cells are ordered or randomly distributed.
	:type ordered: bool
	:param active_in_env: Determines how many cells are allowed to be active in one environment
	:type active_in_env: int		
	'''
	
	def __init__(self, no_fields = None,field_size = None, centers = None, ordered = 0, active_in_env = None, peak = None, cage = [1,1], center_cage = None, **kwargs):

		#Place Fields Attributes
		self.ordered = ordered #whether centers of place fields are evenly distrivuted or random
		self.centers =centers # place field center
		self.no_fields = no_fields #no fields per cell
		self.field_size = field_size # radius of one field; if actFunction is for example getOutputWTA size is only determined by number winner; otherwise, activity is set to 0 outside of the field, inside it has a guassian fire rate disribution
		self.n_e = kwargs['n_e']
		self.cells = kwargs['number_cells']
		self.peak_firing_rate = peak

		if no_fields == None:
			no_fields = np.ones([self.n_e, self.cells], 'int')*1
		if peak == None:
			self.peak_firing_rate = np.ones([self.n_e, self.cells, np.max(no_fields)])
		self.active_in_env = active_in_env
		if self.active_in_env == None:
			self.active_in_env = self.cells
		self.active_cells = np.zeros([self.n_e, self.active_in_env], 'int')#which cells are active in which envrionment
		self.no_fields = np.zeros([self.n_e, self.cells],'int')
		for h in range(self.n_e):
				self.active_cells[h] = np.array(random.sample(range(self.cells), self.active_in_env))
				self.no_fields[h][self.active_cells[h]] = no_fields[h][self.active_cells] #no fields per cell
		self.max_fields = np.max(self.no_fields, axis = 1) #max field number in each env.
		if self.field_size == None:
			field_size = np.random.normal(loc = .2500, scale = .01, size = (self.n_e, self.cells, max(self.max_fields)))
			self.field_size = np.sqrt(field_size/np.pi)



		self.locations = None #x and y coordinates of all pixel 
		self.distance_matrix = None # matrix where entry (i,j) is the eucledean distance between location i and j
		self.cage = cage
		if center_cage == None: # locations of possible field centers; can be bigger or smaller than cage
			self.center_cage = np.array(self.cage)
		else:
			self.center_cage = np.array(center_cage)
		#self.number_patterns= kwargs['number_patterns']
		#if kwargs['sparsity'] == None:
			#sparsity = 
		self.makeLocations(number_patterns = kwargs['number_patterns'])
		Input.__init__(self, **kwargs)



	def makeInput(self,**kwargs):
		self.number_winner = min(self.number_winner, self.cells)
		self.active_cells = np.zeros([self.n_e, self.active_in_env], 'int')#which cells are active in which envrionment
		if self.active_in_env != self.cells:
			print 'not all cells active in envoronment!!!!'
			for h in range(self.n_e):
					self.active_cells[h] = np.array(random.sample(range(self.cells), self.active_in_env))
		else:
			for h in range(self.n_e):
				self.active_cells[h] = np.arange(self.cells)
		self.makeFields()
		self.patterns = self.calcFiring()

	def makeFields(self):
		if self.centers == None:
			self.centers = np.zeros([self.n_e, self.cells, np.max(self.no_fields), 2])
			if self.ordered:
				for h in range(self.n_e):
					self.centers[h][:int(np.sqrt(self.active_in_env))] = np.ravel((np.mgrid[0:int(np.sqrt(self.active_in_env)), 0: int(np.sqrt(self.active_in_env))] + 0.0)/np.sqrt(self.cells), order = 'F').reshape(self.cells,1, 2)
					self.centers[h][int(np.sqrt(self.active_in_env)):] = np.random.sample(size = [self.active_in_env -int(np.sqrt(self.active_in_env)) ,1, 2])
			else:
				for h in range(self.n_e):
					self.centers[h] = np.random.sample(size = [self.cells, self.max_fields[h], 2]) * np.array(self.cage)*self.center_cage - (0.5*(self.center_cage-1)) * np.array(self.cage)
					for cell in range(self.cells):
						self.centers[h, cell, self.no_fields[h, cell]:] = -10000 #center of fields that exceed number of fields of a cell are put far away
		self.std = self.field_size**2/np.log(5) #modulates the Gauss according to place field size; 
	
	def calcActivity(self, env ,location, field_number): #calc rate map for all cells at location for field_number
		loc_diff = self.centers[env, :, field_number] - location# diff loation to all fields (cells, (x,y))
		dis = np.sum(loc_diff * loc_diff, axis = -1)# eucledan distance squared to each field; (cell, x**2 +y**2)
		activity= np.exp(-dis/self.std[env, :, field_number])*self.peak_firing_rate[env,:,field_number] # rate is calcualted acording to the distance; activity = 0.2* peak rate if dis = size
		activity[activity < 0.2] = 0
		return activity #env ,pattern, cell_activity (with field)

	def calcFiring(self):
		#firing = np.zeros([self.n_e,self.number_patterns, self.cells]) # self.n_e = number enviromnents, self.space_res**2 = number of different firing rate in each enviromnent, self.cells = number grid cells
		activity = np.zeros([self.n_e, self.number_patterns, self.cells])
		for h in range(self.n_e):
			for i in range(self.max_fields[h]): # calc rate map for each field and sum up
				activity[h] += np.array(map(self.calcActivity, [h]*self.number_patterns, self.locations, [i]*self.number_patterns))
		indices = np.mgrid[0:self.n_e,0:self.number_patterns,0:self.active_in_env]
		#act_cells = self.active_cells.repeat(self.number_patterns, axis = 0).reshape(self.n_e, self.number_patterns, self.active_in_env)
		#firing[indices[0], indices[1], act_cells] = self.actFunction(self,activity = activity)
		firing = self.actFunction(self, activity = activity)
		return firing
		#return activity

	def getTrueRadius(self): #in a constructed PlaceField Code with known number of fields, we can calculate the radius in antoherway. To avoid smaller fieled sizes at border,we look at the cell with the largest Place fields and calculate the radius.
		if (self.cluster_size == 0).all():
			self.calcClusters()
		max_field_size_cell = np.max(self.getAverageFieldSizeCell())/(self.no_fields+0.0)
		return np.sqrt(max_field_size_cell/(self.x_length**2*np.pi))
		
	def getStoredlocations(self):
		return self.locations[self.store_indizes]

	def makeLocations(self, number_patterns):
		#if number_patterns == None:
			#number_patterns = self.number
		if self.cage[0]> self.cage[1]:
			self.transposed_cage = True
			self.cage_proportion = self.cage[0]/self.cage[1]
		else:
			self.transposed_cage = False
			self.cage_proportion = self.cage[1]/self.cage[0]
			if int(self.cage_proportion)*self.cage[0] != self.cage[1]:
				print 'cage adjusted to', self.cage
		self.space_res = np.sqrt(number_patterns*1./self.cage_proportion)

		if int(self.space_res * self.cage_proportion*self.space_res) - self.space_res * self.cage_proportion*self.space_res != 0:
			print 'number_patterns not suitable'
			self.locations = None
		else:
			if self.transposed_cage:
				self.locations = np.ravel((np.mgrid[0:self.cage_proportion*self.space_res, 0:self.space_res] + 0.0), order = 'F').reshape(number_patterns, 2)
				self.locations[:,0] /= self.cage_proportion*self.space_res/self.cage[0]
				self.locations[:,1] /= self.space_res/self.cage[1]
			#self.locations = self.locations.reshape(number_patterns, 2)
			else:
				self.locations = np.ravel((np.mgrid[0:self.space_res, 0:self.cage_proportion*self.space_res] + 0.0), order = 'F').reshape(number_patterns, 2)
				self.locations[:,0] /= self.space_res/self.cage[0]
				self.locations[:,1] /= self.cage_proportion*self.space_res/self.cage[1]


class Lec(Input):
	
	def __init__(self, size_kernel = None, cage = [1,1],place_fields = 'medium', **kwargs):
		
		self.locations = None #x and y coordinates of all pixel 
		self.distance_matrix = None # matrix where entry (i,j) is the eucledean distance between location i and j
		self.cage = cage
		self.inputMethod = kwargs['inputMethod']
		self.place_fields = place_fields
		
		self.size = size_kernel #kernel size in cm
		self.makeLocations(number_patterns = kwargs['number_patterns'])
		Input.__init__(self, **kwargs)

		
	
	def makeLocations(self, number_patterns):
		if self.cage[0]> self.cage[1]:
			self.transposed_cage = True
			self.cage_proportion = self.cage[0]/self.cage[1]
		else:
			self.transposed_cage = False
			self.cage_proportion = self.cage[1]/self.cage[0]
			if int(self.cage_proportion)*self.cage[0] != self.cage[1]:
				print 'cage adjusted to', self.cage
		self.space_res = np.sqrt(number_patterns*1./self.cage_proportion)

		if self.transposed_cage:
			self.x_length = self.cage_proportion * self.space_res 
			self.y_length = self.space_res
		else:
			self.y_length = self.cage_proportion * self.space_res
			self.x_length = self.space_res
		
		
		if int(self.space_res * self.cage_proportion*self.space_res) - self.space_res * self.cage_proportion*self.space_res != 0:
			print 'number_patterns not suitable'
			self.locations = None
		else:
			if self.transposed_cage:
				self.locations = np.ravel((np.mgrid[0:self.cage_proportion*self.space_res, 0:self.space_res] + 0.0), order = 'F').reshape(number_patterns, 2)
				self.locations[:,0] /= self.cage_proportion*self.space_res/self.cage[0]
				self.locations[:,1] /= self.space_res/self.cage[1]
			#self.locations = self.locations.reshape(number_patterns, 2)
			else:
				self.locations = np.ravel((np.mgrid[0:self.space_res, 0:self.cage_proportion*self.space_res] + 0.0), order = 'F').reshape(number_patterns, 2)
				self.locations[:,0] /= self.space_res/self.cage[0]
				self.locations[:,1] /= self.cage_proportion*self.space_res/self.cage[1]
				#self.locations = self.locations.reshape(number_patterns, 2)

		
	def makeActiveRegionsSquares(self):
		### fileds shaped as cage
		if self.transposed_cage:
			self.x_length = self.cage_proportion * self.space_res 
			self.y_length = self.space_res
			x_pixel = int(self.space_res*self.cage_proportion/5)
			y_pixel = int(self.space_res/5)
		else:
			self.y_length = self.cage_proportion * self.space_res
			self.x_length = self.space_res
			x_pixel = int(self.space_res/5)
			y_pixel = int(self.space_res*self.cage_proportion/5)
			
		self.space_res = int(self.space_res)
		if self.space_res/5 - self.space_res/5. != 0:
			print 'number_patterns not suitbael for LEC'
		region_ind = []
		region = []
		
		for i in range(int(5)):
			for j in range(int(5)):
				for y in range(y_pixel):
					#region_ind.append(range(i*x_pixel + j* self.space_res + y*self.space_res , i*x_pixel + j* self.space_res + y*self.space_res+x_pixel))
					if self.transposed_cage:
						region_ind.append(range(i*self.cage_proportion * self.space_res*y_pixel + j*x_pixel + y*self.cage_proportion * self.space_res, i*self.cage_proportion * self.space_res*y_pixel + j*x_pixel + y*self.cage_proportion * self.space_res + x_pixel))
					else:
						region_ind.append(range(i* self.space_res*y_pixel + j*x_pixel + y * self.space_res, i* self.space_res*y_pixel + j*x_pixel + y * self.space_res + x_pixel))
				region.append(np.ravel(region_ind))
				region_ind = []
		
		
		
		#### quadratic fields
		#if self.transposed_cage:
			#self.x_length = self.cage_proportion * self.space_res
			#self.y_length = self.space_res
			#x_pixel = int(self.space_res/5)
			#y_pixel = int(self.space_res/5)
		#else:
			#self.y_length = self.cage_proportion * self.space_res
			#self.x_length = self.space_res
			#x_pixel = int(self.space_res/5)
			#y_pixel = int(self.space_res/5)
			
		#self.space_res = int(self.space_res)
		#if self.space_res/5 - self.space_res/5. != 0:

		#region_ind = []
		#region = []
		
		#for i in range(int(5)):
			#for j in range(int(5*self.cage_proportion)):
				#for y in range(y_pixel):
					##region_ind.append(range(i*x_pixel + j* self.space_res + y*self.space_res , i*x_pixel + j* self.space_res + y*self.space_res+x_pixel))
					#if self.transposed_cage:
						#region_ind.append(range(i*self.cage_proportion * self.space_res*y_pixel + j*x_pixel + y*self.cage_proportion * self.space_res, i*self.cage_proportion * self.space_res*y_pixel + j*x_pixel + y*self.cage_proportion * self.space_res + x_pixel))
					#else:
						#region_ind.append(range(i* self.space_res*y_pixel + j*x_pixel + y * self.space_res, i* self.space_res*y_pixel + j*x_pixel + y * self.space_res + x_pixel))
				#region.append(np.ravel(region_ind))
				#region_ind = []
		no_active = np.random.normal(loc = int(len(region)/2.), scale = 1.5, size = (self.patterns.shape[0],self.patterns.shape[-1]) )
		for env in range(self.patterns.shape[0]):
			for i in range(self.patterns.shape[-1]):

				active = np.ravel(random.sample(region, int(no_active[env][i])))
				self.patterns[env,:,i][active] = 1
				
		self.calcFireUniform()
		for env in range(self.patterns.shape[0]):
			for i in range(self.patterns.shape[-1]):
				self.patterns[env,:,i] = self.blur_image(self.patterns[env,:,i].reshape(self.y_length, self.x_length)[::-1]).reshape(self.patterns[env,:,i].shape)
	
	
	def makeActiveRegionsPlaceFields(self):
		no_fields = 2 * np.random.randint(30,150, (self.n_e, self.cells))
		max_fields = np.max(no_fields)
		if self.place_fields == 'medium':
			field_sizes = np.random.normal(loc = 1200, scale = 500, size = self.cells*max_fields*self.n_e)
		
		field_rate = np.ones([self.n_e, self.cells, max_fields])
		field_sizes[field_sizes < 0] *= -1
		field_sizes = field_sizes.reshape(self.n_e, self.cells , max_fields)
		field_in_r = np.sort(np.sqrt(field_sizes/np.pi)/100.)

		HelpPlaceField = PlaceFields(number_cells = self.cells, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputId, number_patterns = self.number_patterns ,n_e =self.n_e,noise_levels = [0], normed = 0, store_indizes = None, cage = self.cage, center_cage = 2., field_size = field_in_r, no_fields = no_fields, peak = field_rate, active_in_env = self.cells, sparsity = 0.1)
		self.patterns = np.copy(HelpPlaceField.patterns)

	
	def makeInput(self,**kwargs):
		self.inputMethod(self)
		max_fire = np.tile(np.max(self.patterns, -2), (self.number_patterns)).reshape(self.patterns.shape)*1.
		self.patterns = self.patterns/max_fire


		
	def calcFireUniform(self):		
		self.patterns[self.patterns == 1 ] = np.random.uniform(0.3, 1, size = (self.patterns[self.patterns == 1].shape))
		self.patterns[self.patterns == 0 ] = np.random.uniform(0, 0.7, size = (self.patterns[self.patterns == 0].shape))

	def makeActiveFilter(self):
		print 'LEc make active filter'
		print 'mean kernel', self.size
		patterns = np.random.normal(loc =1, scale =1, size = (self.n_e, self.number_patterns,self.cells))
		patterns[patterns < 0] *= -1
		patterns = self.actFunction(self, patterns).reshape(self.n_e, self.cells, self.x_length, self.y_length)
		size = np.random.normal(loc = self.size, scale = self.size/5., size = (self.n_e, self.cells))
		size[size<5] = 5
		print 'min kernel', np.min(size) 
		print 'max kernel', np.max(size) 
		for env in range(self.n_e):
			for cell in range(self.cells):
				self.patterns[env, :,cell] = self.blur_image(im = patterns[env, cell], n = size[env, cell]).reshape(self.number_patterns)
				#self.patterns[env, :,cell] = scipy.ndimage.filters.gaussian_filter(patterns[env, cell], sigma = self.size).reshape(self.number_patterns)
		min_fire = np.tile(np.min(self.patterns, -2), (self.number_patterns)).reshape(self.patterns.shape)*1.
		self.patterns -= min_fire
		
		
	def makeNoiseDoNothing(self, **kwargs):
		return self.patterns[0].reshape([self.noise_levels.shape[0], self.number_patterns, self.cells])
		
	def gauss_kern(self, size, sizey=None):
		""" Returns a normalized 2D gauss kernel array for convolutions """
		size = int(size)    
		if not sizey:
			sizey = size
		else:
			sizey = int(sizey)               
  
		x, y = np.mgrid[-size:size+1, -sizey:sizey+1]
		g = np.exp(-(x**2/float(size)+y**2/float(sizey)))
		g / g.sum()		
		#print 'g shape', g.shape
		#g = np.tile(g, (self.n_e, self.cells, 1,1))
		#print 'g shape', g.shape
		return g
	
	def blur_image(self, im, n = None, ny=None):
		""" blurs the image by convolving with a gaussian kernel of typical
		size n. The optional keyword argument ny allows for a different
		size in the y direction.
		"""
		
		if n == None:
			if self.size == None:
				self.size = 17 * self.space_res/(np.min(self.cage)*100) # size = 17cm as in da costa 2005
				#size in pixel
			n = self.size* self.space_res/(np.min(self.cage)*100)
		else:
			n *= self.space_res/(np.min(self.cage)*100.)
		g = self.gauss_kern(n, sizey=ny)
		#improc = scipy.signal.convolve2d(im,g, mode='same', boundary = 'fill', fillvalue = self.sparsity)
		improc = scipy.ndimage.filters.convolve(im,g, mode='reflect')
		return(improc)    
	
	def example(self):
		xmin, xmax, ymin, ymax = -70, 70, -70, 70
		extent = xmin, xmax, ymin, ymax
		
		X, Y = np.mgrid[-70:70, -70:70]
		Z = np.cos((X**2+Y**2)/200.)+ np.random.normal(size=X.shape)    
		#Z = cos((X**2+Y**2)/200.)
		
		fig1 = plt.figure(1)
		fig1.clf()
		ax1a = fig1.add_subplot(131)
		ax1a.imshow(np.abs(Z), cmap=cm.jet, alpha=.9, interpolation='bilinear', extent=extent)
		ax1a.set_title(r'Noisey')
		
		P = self.gauss_kern(3)
		
		ax1b = fig1.add_subplot(132)
		ax1b.imshow(np.abs(P), cmap=cm.jet, alpha=.9, interpolation='bilinear', extent=extent)
		ax1b.set_title(r'Convolving Gaussian')
		
		U = self.blur_image(Z, 3)
		
		ax1c = fig1.add_subplot(133)
		ax1c.imshow(np.abs(U), cmap=cm.jet, alpha=.9, interpolation='bilinear', extent=extent)
		ax1c.set_title(r'Cleaned')



#############################################Hippocampal Networks######################################################################################
class Hippocampus():
	
	
		
	'''
	
	Base Class for all network architectures. Possible Networks are Ec_Dg, Dg_Ca3, Ec_Ca3, Ca3_Ca3, Ca3_Ca1, Ca1_Ec, Ca3_Ec, Ec_Ca1. It has all the arguments as the class 'Network' and its childs has. It takes them as an dictionary. For example cells = dict('Ec_Ca3' = 1000, 'Ec_Ca1 = 2000'). If a Parameter is not given. Standard Paramter is taken from class 'Parameter'. Furthermore, whole Network instances can be passed as arguments,too. In this case, this network is no loner initilized but uses the passed network as a network. This makes sense, if you have a trained network already and you want to use it again. Additional parameters are described below.
	
	:param just_ca3: Whether the whole architecture is simulated or only till Ca3
	:type just_ca3: bool 
	:param rec: Whether the CA3 network is modelled as an autoassociation memory
	:type rec:  bool
	:param In: Input Instance that gives the patterns that are going to be stored in the architecture.
	:type In: Input Instance, must have same cell number as self.cells['Ec']
	:param InCa1: Input Instance that give input to Ec_CA1 via the weights Ec_Ca1
	:type InCa1: Input Instance, must have same cell number as self.cells['Ec']
	:param Ca3Activation: If CA3Activation != None, then this Input Instance sets up the patterns in CA3.
	:type Ca3Activation: Input Instance, must have same cell number as self.cells['CA3']
	:param Ca1Activation: If CA1Activation != None, then this Input Instance sets up the patterns in CA1. If it is None, InCa1 = In
	:type Ca1ctivation: Input Instance, must have same cell number as self.cells['CA1']
	'''
	
	def __init__(self, just_ca3 = 0,  rec = 1, In = None, InCa1 = None, Ca3Activation = None, Ca1Activation = None, Ec_Dg = None, Dg_Ca3 = None, Ec_Ca3 = None, Ec_Ca1 = None, Ca3_Ca3 = None, Ca3_Ca1 = None, Ca1_Ec = None, Ca3_Ec = None,cells = Parameter.cells, number_winner = Parameter.number_winner, connectivity = Parameter.connectivity, learnrate = Parameter.learnrate, subtract_input_mean = Parameter.subtract_input_mean, subtract_output_mean = Parameter.subtract_output_mean, actFunctionsRegions = Parameter.actFunctionsRegions, initMethod = Parameter.initMethod, cycles  = Parameter.cycles, incremental_storage_mode=Parameter.incremental_storage_mode, no_incremental_times = Parameter.no_incremental_times, number_to_store = Parameter.number_to_store, incrementalLearnMethod = Parameter.incrementalLearnMethod,external_force = Parameter.external_force, internal_force = Parameter.internal_force, first = Parameter.first, active_in_env = Parameter.active_in_env):


		self.In = In
		self.Ec_Dg = Ec_Dg
		self.Dg_Ca3 = Dg_Ca3
		self.Ec_Ca3 = Ec_Ca3
		self.Ec_Ca1 = Ec_Ca1
		self.Ca3_Ca3 = Ca3_Ca3
		self.Ca3_Ca1 = Ca3_Ca1
		self.Ca3_Ec = Ca3_Ec
		self.Ca1_Ec = Ca1_Ec
	
		self.rec = rec
		self.just_ca3 = just_ca3
		self.InCa1 = InCa1
		self.Ca3Activation = Ca3Activation
		self.Ca1Activation = Ca1Activation
	
		self.cells = copy.copy(cells)
		self.number_winner = copy.copy(number_winner)
		self.connectivity = copy.copy(connectivity)
		self.learnrate = copy.copy(learnrate)
		self.subtract_input_mean = copy.copy(subtract_input_mean)
		self.subtract_output_mean = copy.copy(subtract_output_mean)
		self.actFunctionsRegions = copy.copy(actFunctionsRegions)
		self.initMethod = copy.copy(initMethod)
		self.cycles = copy.copy(cycles)
		self.incremental_storage_mode = copy.copy(incremental_storage_mode)
		self.no_incremental_times = copy.copy(no_incremental_times)
		self.number_to_store = copy.copy(number_to_store)
		self.incrementalLearnMethod = incrementalLearnMethod
		self.external_force = copy.copy(external_force)
		self.internal_force = copy.copy(internal_force)
		self.active_in_env = active_in_env
		self.n_e = self.In.n_e
		self.first = first
		if self.first == None:
			self.first = self.In.number_to_store
		
		self.store_ind = np.mgrid[0:self.In.n_e, 0:self.In.number_to_store]#helping attribut, it is needed if CA3Activation is given
	
	
	##########methods for creating the Network instances##############################
	### In General, each network is an HeteroAssocaition Instance
	### Exceptions are Ec_Dg, which is a OneShoot Instance and Ca3_Ca3, which is an AutoAssociation instance
	def makeEc_Dg(self):
		if self.Ec_Dg == None:
			self.Ec_Dg = OneShoot(input_cells=self.cells['Ec'], cells=self.cells['Dg'], number_winner=self.number_winner['Dg'], connectivity = self.connectivity['Ec_Dg'], learnrate= self.learnrate['Ec_Dg'], subtract_input_mean = 0, subtract_output_mean = 0, initMethod = self.initMethod['Ec_Dg'], actFunction = self.actFunctionsRegions['Ec_Dg'],weight_mean = 1, weight_sigma = 0.01, active_in_env = self.active_in_env['Dg'], n_e = self.n_e) 
			
	def makeDg_Ca3(self):
		if self.Dg_Ca3 == None:
			self.Dg_Ca3 = HeteroAssociation(input_cells = self.cells['Dg'], cells = self.cells['Ca3'], number_winner = self.number_winner['Ca3'], connectivity = self.connectivity['Dg_Ca3'], learnrate = self.learnrate['Dg_Ca3'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = self.subtract_output_mean, initMethod = self.initMethod['Dg_Ca3'],actFunction = self.actFunctionsRegions['Dg_Ca3'], active_in_env = self.active_in_env['Ca3'], n_e = self.n_e) 
		
	def makeEc_Ca3(self):
		if self.Ec_Ca3 == None:
			self.ec_ca3_given = False
			self.Ec_Ca3 =  HeteroAssociation(input_cells = self.cells['Ec'], cells = self.cells['Ca3'], number_winner = self.number_winner['Ca3'], connectivity = self.connectivity['Ec_Ca3'], learnrate = self.learnrate['Ec_Ca3'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = self.subtract_output_mean, initMethod = self.initMethod['Ec_Ca3'],actFunction = self.actFunctionsRegions['Ec_Ca3'], active_in_env = self.active_in_env['Ca3'], n_e = self.n_e) 
		else:
			self.ec_ca3_given = True
		
	def makeCa3_Ca3(self):
		if self.Ca3_Ca3 == None:
			self.Ca3_Ca3 = AutoAssociation(input_cells = self.cells['Ca3'], cells = self.cells['Ca3'], number_winner = self.number_winner['Ca3'], connectivity = self.connectivity['Ca3_Ca3'], learnrate = self.learnrate['Ca3_Ca3'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = 1, initMethod = self.initMethod['Ca3_Ca3'], cycles = self.cycles,actFunction = self.actFunctionsRegions['Ca3_Ca3'], external_force = self.external_force, internal_force = self.internal_force, active_in_env = self.active_in_env['Ca3'], n_e = self.n_e) 
			self.Ca3_Ca3.active_cells = self.Ec_Ca3.active_cells
			self.Ca3_Ca3.active_cells_vector = self.Ec_Ca3.active_cells_vector
			self.ca3_ca3_given = False
		else:
			self.ca3_ca3_given = True

	def makeCa3_Ca1(self):
		if self.Ca3_Ca1 == None:
			self.Ca3_Ca1 = HeteroAssociation(input_cells = self.cells['Ca3'], cells = self.cells['Ca1'], number_winner = self.number_winner['Ca1'], connectivity = self.connectivity['Ca3_Ca1'], learnrate = self.learnrate['Ca3_Ca1'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = self.subtract_output_mean, initMethod = self.initMethod['Ca3_Ca1'],actFunction = self.actFunctionsRegions['Ca3_Ca1'], active_in_env = self.active_in_env['Ca1'], n_e = self.n_e) 
			self.ca3_ca1_given = False
		else:
			self.ca3_ca1_given = True
		if self.ca3_ca1_given:
			self.ca3_ca1_given = False
			print '!!!!!!!!!!!!!!!!!!!!! make new CA3_CA1!!!!!!'

	def makeEc_Ca1(self):
		if self.Ec_Ca1 == None:
			self.Ec_Ca1 =  HeteroAssociation(input_cells = self.cells['Ec'], cells = self.cells['Ca1'], number_winner =self.number_winner['Ca1'], connectivity = self.connectivity['Ec_Ca1'], learnrate = self.learnrate['Ec_Ca1'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = self.subtract_output_mean, initMethod = self.initMethod['Ec_Ca1'],actFunction = self.actFunctionsRegions['Ec_Ca1'], active_in_env = self.active_in_env['Ca1'], n_e = self.n_e) 
			self.ec_ca1_given = False
			if self.Ca3_Ca1 != None:
				self.Ec_Ca1.active_cells = self.Ca3_Ca1.active_cells
				self.Ec_Ca1.active_cells_vector = self.Ca3_Ca1.active_cells_vector
		else:
			self.ec_ca1_given = True
	
	def makeCa3_Ec(self):
		if self.Ca3_Ec == None:
			self.Ca3_Ec = HeteroAssociation(input_cells = self.cells['Ca3'], cells = self.cells['Ec'], number_winner = self.number_winner['Ec'], connectivity = self.connectivity['Ca3_Ec'], learnrate = self.learnrate['Ca3_Ec'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = self.subtract_output_mean, initMethod = self.initMethod['Ca3_Ec'],actFunction = self.actFunctionsRegions['Ca3_Ec'])
			self.ca3_ec_given = False
		else:
			self.ca3_ec_given = True
	
	def makeCa1_Ec(self):
		if self.Ca1_Ec == None:
			self.Ca1_Ec =  HeteroAssociation(input_cells = self.cells['Ca1'], cells = self.cells['Ec'], number_winner = self.number_winner['Ec'], connectivity = self.connectivity['Ca1_Ec'], learnrate = self.learnrate['Ca1_Ec'], subtract_input_mean = self.subtract_input_mean, subtract_output_mean = self.subtract_output_mean, initMethod = self.initMethod['Ca1_Ec'],actFunction = self.actFunctionsRegions['Ca1_Ec'], n_e = self.n_e) 
			self.ca1_ec_given = False
		else:
			self.ca1_ec_given = True


	### store method defines how patterns are stored in the network.
	def store(self):
		pass
	
	### recall method defines how patterns are recalled in the network.
	def recall(self):
		pass
	
	### sets up the Activity when Ca3Activation is given
	def setUpCa3Activity(self):
		if self.Ca3Activation == None:
			self.makeEc_Dg()
			self.makeDg_Ca3()
		else:
			self.Dg_Ca3 = Network(input_cells = 1, cells = self.cells['Ca3'], connectivity =1, initMethod = Network.makeWeightsZero, number_winner = 0, active_in_env = self.active_in_env['Ca3'], n_e = self.In.n_e) # dummy network
			self.Dg_Ca3.output_stored = self.Ca3Activation.getInput()[self.store_ind[0],self.In.store_indizes]
	
	### sets up the Activity when Ca1Activation is given
	def setUpCa1Activity(self):
		if self.Ca1Activation == None:
			if self.InCa1 == None:
				self.InCa1 = self.In
			self.makeEc_Ca1()
		else:
			self.Ec_Ca1 = Network(input_cells = 1, cells = self.cells['Ca1'], connectivity =1, initMethod = Network.makeWeightsZero, number_winner = 0,active_in_env = self.active_in_env['Ca1'], n_e = self.In.n_e) # dummy network
			self.Ec_Ca1.output_stored = self.Ca1Activation.getInput()[self.store_ind[0],self.In.store_indizes]

class HippocampusFull(Hippocampus):
	
	'''
	
	The full hippocampal loop. If CA3Activation is not given, input patterns trigger activation in Ec_Dg, which adjust its weights in a competitive net during storage. Activity in Ec_Dg triggers one in Dg_CA3. This activity is autoassociated in Ca3_CA3 and hetero assocatiated in Ec_Ca3. If CA1Activation is not given, InCa1 sets activitys in Ec_Ca1, which is hetero associated with the CA3 patterns in Ca3_Ca1. Finally Ec_CA1 patterns are hetero associated with the original patterns in Ca1_Ec. 
	'''
	
	def __init__(self, **kwargs):
		
		Hippocampus.__init__(self, **kwargs)

		self.setUpCa3Activity()
		self.makeEc_Ca3()
		self.makeCa3_Ca1()
		self.makeCa1_Ec()
		if self.rec:
			self.makeCa3_Ca3()
		if not self.just_ca3:
			self.setUpCa1Activity()
		self.store()
		self.recall()
		
	def store(self):
		if not self.ec_ca3_given:
			if self.Ca3Activation ==None:
				if self.learnrate['Ec_Dg'] !=0:
					print 'learn Ec_Dg'
					self.Ec_Dg.learnOneShootAllPattern(input_pattern = self.In.input_stored, method = self.incrementalLearnMethod, first = self.first)
				else:
					print 'DG does not learn'
					self.Ec_Dg.learnOneShootAllPattern(input_pattern = self.In.input_stored, method = self.incrementalLearnMethod, first = self.first)
				self.Dg_Ca3.output_stored = self.Dg_Ca3.getOutput(self.Dg_Ca3, input_pattern = self.Ec_Dg.output_stored)
			print 'learn Ec_Ca3'
			self.Ec_Ca3.learnAssociation(input_pattern = self.In.input_stored, output_pattern = self.Dg_Ca3.output_stored, first = self.first)
		else:
			self.Dg_Ca3.output_stored = self.Ec_Ca3.output_stored
		if self.rec:
			if not self.ca3_ca3_given:
				print 'learn ca3_ca3'
				self.Ca3_Ca3.learnAssociation(input_pattern = self.Dg_Ca3.output_stored, output_pattern = self.Dg_Ca3.output_stored,first = self.first)
			
		if not self.just_ca3:
			if self.Ca1Activation == None:
				if not self.ec_ca1_given:
					print 'calc Ec_Ca1 output'
					self.Ec_Ca1.output_stored = self.Ec_Ca1.getOutput(self.Ec_Ca1,input_pattern = self.InCa1.input_stored)
			if not self.ca3_ca1_given:
				print 'learn ca3_ca1'
				self.Ca3_Ca1.learnAssociation(input_pattern = self.Ec_Ca3.output_stored, output_pattern = self.Ec_Ca1.output_stored,first = self.first)
			if not self.ca1_ec_given:
				print 'learn ca1_ec'
				self.Ca1_Ec.learnAssociation(input_pattern = self.Ec_Ca1.output_stored, output_pattern = self.In.input_stored,first = self.first)

	def recall(self):
		print 'recall'
		if not self.ec_ca3_given:
			print 'calc Ec_Ca3 output'
			self.Ec_Ca3.recall(input_pattern = self.In.noisy_input_stored, first = self.first)
		if self.rec:
			if not self.ca3_ca3_given:
				print 'calc Ca3 rec output'
				self.Ca3_Ca3.recall(input_pattern = self.Ec_Ca3.noisy_output, external_activity = self.Ec_Ca3.calcActivity(input_pattern = self.In.noisy_input_stored), first = self.first)

		if not self.just_ca3:
			if self.rec:
				if not self.ca3_ca1_given:
					print 'ca3_ca1 rec'
					self.Ca3_Ca1.recall(input_pattern = self.Ca3_Ca3.noisy_output, key = 'Rec', first = self.first)
				if not self.ca1_ec_given:
					print 'ca1_ec_rec'
					self.Ca1_Ec.recall(input_pattern = self.Ca3_Ca1.noisy_output, key = 'Rec', first = self.first)
			if not self.ca3_ca1_given:		
				print 'ca3_ca1 no rec'
				self.Ca3_Ca1.recall(input_pattern = self.Ec_Ca3.noisy_output, key = 'NoRec', first = self.first)
			if not self.ca1_ec_given:
				print 'ca1_ec_norec'
				self.Ca1_Ec.recall(input_pattern = self.Ca3_Ca1.noisy_output, key = 'NoRec', first = self.first)

class HippocampusCa3Ca1OneShoot(HippocampusFull):
	
	
	'''
	
	Same as HippocampusFull, but now Ca3_Ca1 is learned by a OneShoot learning. Hence CA1Activation is ignored and it does not contain Ec_Ca1
	
	:param init_ca3_ca1: How weights in Ca3_Ca1 are set up 
	:param init_ca3_ca1: Network.makeWeights method
	'''
	
	def __init__(self, init_ca3_ca1 = Network.makeWeightsUniformDistributed, **kwargs):

		self.init_ca3_ca1 = init_ca3_ca1
		HippocampusFull.__init__(self, **kwargs)

	def makeCa3_Ca1(self):
		if self.Ca3_Ca1 == None:
			self.Ca3_Ca1 = OneShoot(input_cells = self.cells['Ca3'], cells = self.cells['Ca1'], number_winner = self.number_winner['Ca1'], connectivity = self.connectivity['Ca3_Ca1'], learnrate = self.learnrate['Ca3_Ca1'], subtract_input_mean = None, subtract_output_mean = None, initMethod = self.init_ca3_ca1 ,actFunction = self.actFunctionsRegions['Ca3_Ca1'], active_in_env = self.active_in_env['Ca1'], n_e = self.n_e, weight_mean = 1, weight_sigma = 0.5) 
			self.ca3_ca1_given = False
		else:
			self.ca3_ca1_given = True
		if self.ca3_ca1_given:
			self.ca3_ca1_given = False
			print '!!!!!!!!!!!!!!!!!!!!! make new CA3_CA1!!!!!!'
			
	def store(self):
		if not self.ec_ca3_given:
			if self.Ca3Activation ==None:
				if self.learnrate['Ec_Dg'] !=0:
					print 'learn Ec_Dg'
					self.Ec_Dg.learnOneShootAllPattern(input_pattern = self.In.input_stored, method = self.incrementalLearnMethod, first = self.first)
				else:
					print 'DG does not learn'
					self.Ec_Dg.learnOneShootAllPattern(input_pattern = self.In.input_stored, method = self.incrementalLearnMethod, first = self.first)
				#else:
					#self.Ec_Dg.output_stored = self.Ec_Dg.getOutput(self.Ec_Dg, input_pattern = self.In.input_stored)
				self.Dg_Ca3.output_stored = self.Dg_Ca3.getOutput(self.Dg_Ca3, input_pattern = self.Ec_Dg.output_stored)
			print 'learn Ec_Ca3'
			self.Ec_Ca3.learnAssociation(input_pattern = self.In.input_stored, output_pattern = self.Dg_Ca3.output_stored, first = self.first)
		else:
			self.Dg_Ca3.output_stored = self.Ec_Ca3.output_stored
		if self.rec:
			if not self.ca3_ca3_given:
				print 'learn ca3_ca3'
				self.Ca3_Ca3.learnAssociation(input_pattern = self.Dg_Ca3.output_stored, output_pattern = self.Dg_Ca3.output_stored,first = self.first)
			
		if not self.just_ca3:
			print 'learn ca3_ca1'
			self.Ca3_Ca1.learnOneShootAllPattern(input_pattern = self.Ec_Ca3.output_stored, method = self.incrementalLearnMethod,first = self.first)
			if not self.ca1_ec_given:
				print 'learn ca1_ec'
				self.Ca1_Ec.learnAssociation(input_pattern = self.Ca3_Ca1.output_stored, output_pattern = self.In.input_stored,first = self.first)
	
class EcCa1EC(Hippocampus): 

	'''
	
	Input pattern in In are hetero associated with CA1 patterns in Ec_Ca1 and CA1 patterns are hetero associated with the Input in Ca1_Ec. Patterns in CA1 are given by CA1Activation. If not, they are created from the Input via random and fixed weights over Ec_Ca3_Ca1. 
	'''
	

	def __init__(self, **kwargs):

		Hippocampus.__init__(self,**kwargs )
		self.initMethod['Ec_Ca1'] = Network.makeWeightsZero
		
		self.makeEc_Ca1()
		self.makeCa1_Ec()
		self.setUpCa1Activity()
		self.store()
		self.recall()

	def setUpCa1Activity(self):
		if self.Ca1Activation == None:
			self.initMethod['Ec_Ca3'] = Network.makeWeightsUniformDistributed
			self.initMethod['Ca3_Ca1'] = Network.makeWeightsUniformDistributed
			self.initMethod['Ec_Ca3'] = Network.makeWeightsNormalDistributed
			self.initMethod['Ca3_Ca1'] = Network.makeWeightsNormalDistributed
			if self.Ca3Activation == None:
				self.makeEc_Ca3()
				self.makeCa3_Ca1()
				self.ca3_pattern = self.Ec_Ca3.getOutput(self.Ec_Ca3,input_pattern = self.In.input_stored)
			else:
				self.makeCa3_Ca1()
				self.ca3_pattern = self.Ca3Activation.getInput()[self.store_ind[0], self.In.store_indizes]
			self.ca1_pattern = self.Ca3_Ca1.getOutput(self.Ca3_Ca1,input_pattern = self.ca3_pattern)
		else:
			self.ca1_pattern = self.Ca1Activation.getInput()[self.store_ind[0], self.In.store_indizes]

	def store(self):
		self.Ec_Ca1.learnAssociation(input_pattern = self.In.input_stored, output_pattern = self.ca1_pattern)
		self.Ca1_Ec.learnAssociation(input_pattern = self.Ec_Ca1.output_stored, output_pattern = self.In.input_stored)
			
	def recall(self):
		self.Ec_Ca1.recall(input_pattern = self.In.noisy_input_stored, first = self.first)
		self.Ca1_Ec.recall(input_pattern = self.Ec_Ca1.noisy_output, first = self.first)

	
	
	
#Perform Simulations and accuire data
def saveSimMod():
	Ca3Activation = None
	
	act = dict(Ec_Dg = Network.getOutputWTALinear, Dg_Ca3 = Network.getOutputWTA, Ca3_Ec = Network.getOutputWTALinear, Ec_Ca3 = Network.getOutputWTA, Ca3_Ca3 = AutoAssociation.getOutputWTA, Ca3_Ca1= Network.getOutputWTALinear, Ca1_Ec = Network.getOutputWTALinear, Ec_Ca1 = Network.getOutputWTALinear)
	
	
	##Random
	In = Input(inputMethod = Input.makeInputNormalDistributed, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTALinear, number_patterns = 252, n_e =1,noise_levels = np.arange(0,Parameter.cells['Ec']+1, int(Parameter.cells['Ec']/15)))

	learn = Parameter.learnrate
	learn['Ec_Dg'] = 0.5
	hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn, actFunctionsRegions = act)
	saveHippoWeights(hippo = hippo, title = 'Rand_252_DG05')
	
	learn['Ec_Dg'] = 0
	hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn, actFunctionsRegions = act)
	saveHippoWeights(hippo = hippo, title = 'Rand_252_DG0')

	Ca3Activation = Input(inputMethod = Input.makeInputNormalDistributed, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTA, number_patterns = 400, n_e =1,noise_levels = [0], number_cells = Parameter.cells['Ca3'], sparsity = Parameter.sparsity['Ca3'])
	hippo = HippocampusFull(In = In,learnrate= learn, Ca3Activation = Ca3Activation)
	saveHippoWeights(hippo = hippo, title = 'Rand_252_Ca3Rand')
	Ca3Activation = None
	
	hippo = EcCa1EC(In = In)
	saveEcCa1EcWeights(hippo = hippo, title = 'Rand_252_EcCa1Ec')

	saveInput(In = In, title ='Random_252')
	

	####GRID
	#####MEC Levels
	for mec_prop in [0, .5, 1]:
		spacings = None
		peak = np.random.uniform(.5, 1.5, size = (1,  int(Parameter.cells['Ec'] * mec_prop), 50,50))
		In = Grid(lec_cells = int(Parameter.cells['Ec'] *(1- mec_prop)), inputMethod = Lec.makeActiveRegionsPlaceFields, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTALinear, number_patterns = 400, number_to_store = 252,n_e =1,noise_levels = np.arange(0,Parameter.cells['Ec']+1, int(Parameter.cells['Ec']/15)), grid_mode = 'modules', peak = peak, spacings = spacings)
		saveInput(In = In, title ='GridMod'+str(mec_prop)+'_252_Env1')
	
		learn = Parameter.learnrate
		learn['Ec_Dg'] = 0.5
		hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn, actFunctionsRegions=act)
		saveHippoWeights(hippo = hippo, title = 'GridMod'+str(mec_prop)+'_252_Env1_DG05')

		learn['Ec_Dg'] = 0
		hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn,actFunctionsRegions = act)
		saveHippoWeights(hippo = hippo, title = 'GridMod'+str(mec_prop)+'_252_Env1_DG0')

		if mec_prop == 1:
			Ca3Activation = Input(inputMethod = Input.makeInputNormalDistributed, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTA, number_patterns = In.number_patterns, n_e =In.n_e,noise_levels = [0], number_cells = Parameter.cells['Ca3'], sparsity = Parameter.sparsity['Ca3'])
			hippo = HippocampusFull(In = In,learnrate= learn, Ca3Activation = Ca3Activation)
			saveHippoWeights(hippo = hippo, title = 'GridMod'+str(mec_prop)+'_252_Env1_Ca3Rand')
			Ca3Activation = None
	
		hippo = EcCa1EC(In = In)
		saveEcCa1EcWeights(hippo = hippo, title = 'GridMod'+str(mec_prop)+'_252_Env1_EcCa1Ec')
	
	###Env	
	Ca3Activation = None
	for env in [1,3,6,9]:
		mec_prop = 1
		spacings = None
		peak = np.random.uniform(.5, 1.5, size = (env,  int(Parameter.cells['Ec'] * mec_prop), 50,50))
		In = Grid(lec_cells = int(Parameter.cells['Ec'] *(1- mec_prop)), inputMethod = Lec.makeActiveRegionsPlaceFields, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTALinear, number_patterns = 400, number_to_store = int(252/env),n_e =env,noise_levels = np.arange(0,Parameter.cells['Ec']+1, int(Parameter.cells['Ec']/15)), grid_mode = 'modules', spacings = spacings, peak = peak)
		saveInput(In = In, title ='GridMod1_252_Env'+str(env))
	
		learn = Parameter.learnrate
		learn['Ec_Dg'] = 0.5
		hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn, actFunctionsRegions=act)
		saveHippoWeights(hippo = hippo, title ='GridMod1_252_Env'+str(env)+'_DG05')

		learn['Ec_Dg'] = 0
		hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn,actFunctionsRegions = act)
		saveHippoWeights(hippo = hippo, title ='GridMod1_252_Env'+str(env)+'_DG0')	
	
		hippo = EcCa1EC(In = In)
		saveEcCa1EcWeights(hippo = hippo, title ='GridMod1_252_Env'+str(env)+'_EcCa1Ec')
	
		if env == 1:
			Ca3Activation = Input(inputMethod = Input.makeInputNormalDistributed, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTA, number_patterns = In.number_patterns, n_e =In.n_e,noise_levels = [0], number_cells = Parameter.cells['Ca3'], sparsity = Parameter.sparsity['Ca3'])
			hippo = HippocampusFull(In = In,learnrate= learn, Ca3Activation = Ca3Activation)
			saveHippoWeights(hippo = hippo, title ='GridMod1_252_Env'+str(env)+'_Ca3Rand')
			Ca3Activation = None
	
	
	for env in [9]:
		mec_prop = 0.5
		peak = np.random.uniform(.5, 1.5, size = (env,  int(Parameter.cells['Ec'] * mec_prop), 50,50))
		spacings = None
		In = Grid(lec_cells = int(Parameter.cells['Ec'] *(1- mec_prop)), inputMethod = Lec.makeActiveRegionsPlaceFields, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTALinear, number_patterns = 400, number_to_store = int(252/env),n_e =env,noise_levels = np.arange(0,Parameter.cells['Ec']+1, int(Parameter.cells['Ec']/15)), grid_mode = 'modules', peak = peak, spacings = spacings)
		saveInput(In = In, title ='GridMod0.5_252_Env'+str(env))
	
		learn = Parameter.learnrate
		learn['Ec_Dg'] = 0.5
		hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn, actFunctionsRegions=act)
		saveHippoWeights(hippo = hippo, title ='GridMod0.5_252_Env'+str(env)+'_DG05')

		learn['Ec_Dg'] = 0
		hippo = HippocampusFull(In = In, just_ca3=0, Ca3Activation = Ca3Activation, learnrate= learn,actFunctionsRegions = act)
		saveHippoWeights(hippo = hippo, title ='GridMod0.5_252_Env'+str(env)+'_DG0')	
	
		hippo = EcCa1EC(In = In)
		saveEcCa1EcWeights(hippo = hippo, title ='GridMod0.5_252_Env'+str(env)+'_EcCa1Ec')
	
		if env == 1:
			Ca3Activation = Input(inputMethod = Input.makeInputNormalDistributed, noiseMethod = Input.makeNoiseRandomFire, actFunction = Input.getOutputWTA, number_patterns = In.number_patterns, n_e =In.n_e,noise_levels = [0], number_cells = Parameter.cells['Ca3'], sparsity = Parameter.sparsity['Ca3'])
			hippo = HippocampusFull(In = In,learnrate= learn, Ca3Activation = Ca3Activation)
			saveHippoWeights(hippo = hippo, title ='GridMod0.5_252_Env'+str(env)+'_Ca3Rand')
			Ca3Activation = None

	collapse = 0


#Figure 1
def ecTestMod():

	#In = load('GridMod0.5_252_Env6.p')
	ax_ind = [4,4]
	fig = plt.figure()
	
	gs = gridspec.GridSpec(16,16)
	ax1 = plt.subplot(gs[:11, :11])
	ax2 = plt.subplot(gs[:7,12:])
	ax3 = plt.subplot(gs[9::,12:])
	ax4 = plt.subplot(gs[12:,:12])
	ax4.set_visible(0)
	
	
	
	#img=mpimg.imread('g12.png')
	#ax1.imshow(img)
	ax1.spines['top'].set_visible(False)
	ax1.spines['right'].set_visible(False)
	ax1.spines['left'].set_visible(False)
	ax1.spines['bottom'].set_visible(False)
	plt.setp(ax1.get_yticklabels(), visible=False)
	plt.setp(ax1.get_yticklines(),visible=False)
	plt.setp(ax1.get_xticklabels(), visible=False)
	plt.setp(ax1.get_xticklines(),visible=False)
	makeLabel(ax = ax1, label = 'A', sci = 0 )
	
	
	In = load('GridMod1_252_Env1.p')
	#ax.set_title('Spacing')
	ax2.hist([In.spacing[0][:In.modules[1]], In.spacing[0][In.modules[1]: In.modules[2]],In.spacing[0][In.modules[2]: In.modules[3]],In.spacing[0][In.modules[3]: In.modules[4]]], label = ['M1', 'M2', 'M3', 'M4'], bins = 20, histtype = 'barstacked', normed = 0)
	ax2.legend(loc='best',prop={'size':9})
	ax2.set_xlim(0,1.2)
	ax2.set_xlabel('Spacings in meter')
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	ax2.xaxis.get_major_ticks()[-1].label1.set_visible(False)
	makeLabel(ax = ax2, label = 'B', sci = 0 )
	
	ax3.hist([In.theta[0][In.modules[3]: In.modules[4]], In.theta[0][:In.modules[1]], In.theta[0][In.modules[1]: In.modules[2]],In.theta[0][In.modules[2]: In.modules[3]]], color = ['c', 'b', 'g', 'r'], bins = 20, histtype = 'barstacked', normed = 0)
	for tick in ax3.xaxis.get_major_ticks()[::2]:
		tick.label1.set_visible(False)
	ax3.set_xlabel('Orientations in degree')
	makeLabel(ax = ax3, label = 'C', sci = 0 )
	
	
	In = load('GridMod0.5_252_Env9.p')
	plt.setp(ax4.get_yticklabels(), visible=False)
	plt.setp(ax4.get_yticklines(),visible=False)
	plt.setp(ax4.get_xticklabels(), visible=False)
	plt.setp(ax4.get_xticklines(),visible=False)
	points = ax4.get_position().get_points()
	left = points[0,0]
	bottom = points[0,1]
	h = points[1,1] - bottom
	w = points[1,0] - left
	grid = ImageGrid(fig, rect = [left, bottom, w,h], nrows_ncols = (2,6), axes_pad=0.05, aspect = 1 ,share_all= 0, cbar_mode = 'single')
	max_fire = np.max(np.array([np.max(In.patterns[0:2,:,In.modules[0]]),np.max(In.patterns[0:2,:,In.modules[1]]),np.max(In.patterns[0:2,:,In.modules[2]]),np.max(In.patterns[0:2,:,In.modules[3]]), np.max(In.patterns[1,:,550:552]), np.max(In.patterns[0,:,550:552])])) 
	for i in range(2):
		s = grid[i*6].imshow(In.patterns[i,:,0].reshape(20,20), interpolation = 'none',vmax = max_fire)
		grid[i*6+1].imshow(In.patterns[i,:,In.modules[1]].reshape(20,20), interpolation = 'none',vmax = max_fire)
		grid[i*6+2].imshow(In.patterns[i,:,In.modules[2]].reshape(20,20), interpolation = 'none',vmax = max_fire)
		grid[i*6+3].imshow(In.patterns[i,:,In.modules[3]].reshape(20,20), interpolation = 'none',vmax = max_fire)
		grid[i*6+4].imshow(In.patterns[i,:,550].reshape(20,20), interpolation = 'none',vmax = max_fire)
		grid[i*6+5].imshow(In.patterns[i,:,551].reshape(20,20), interpolation = 'none',vmax = max_fire)


	grid.cbar_axes[0].colorbar(s)
	for i in range(12):
		plt.setp(grid[i].get_yticklabels(), visible=False)
		plt.setp(grid[i].get_yticklines(),visible=False)
		plt.setp(grid[i].get_xticklabels(), visible=False)
		plt.setp(grid[i].get_xticklines(),visible=False)
	makeLabel(ax = grid[0], label = 'D', sci = 0 )
	grid[4].set_ylabel('1m')
	grid[4].set_xlabel('1m')
	

	collapse = 0

#Figure 2
def rollsToScaled():
	### Set Parameter.rolls = 1 before run! ###	
	In = Input(number_patterns = 100, number_to_store = 100, inputMethod = Input.makeInputSparsityLevel, noiseMethod = Input.makeNoiseZero,noise_levels = np.arange(0,Parameter.cells['Ec']+1, int(Parameter.cells['Ec']/19)))
	Ca3Activation = None
	
	hippo = HippocampusCa3Ca1OneShoot(In = In, just_ca3=0, rec = 1, Ca3Activation = Ca3Activation, first= None)
	
	x_max =25
	y_max = 550
	
	fig = plt.figure()

	ax5 = fig.add_subplot(231, aspect = 'equal')
	ax5.set_ylim(0, 1.)
	ax5.set_xlim(0, 1.)
	ax5.set_xlabel('Cue quality')
	ax5.set_ylabel('Correlation', multialignment = 'center')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'CA3')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'CA1')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'EC')
	ax5.plot(In.getOrigVsOrig(), hippo.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g',linestyle = '--')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r',linestyle = '--')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b',linestyle = '--')
	ax5.plot(np.linspace(0,1, 10), np.linspace(0,1, 10), c='k')
	ax5.legend(loc='lower right',prop={'size':9},numpoints = 1)
	makeLabel(ax = ax5, label = 'A', sci = 0 )
	for tick in ax5.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	number_winner = Parameter.number_winner
	learn = Parameter.learnrate
	number_winner['Ca1'] = 100
	hippo100 = HippocampusCa3Ca1OneShoot(In = In, just_ca3=0, rec = 1, Ca3Activation = Ca3Activation, learnrate = learn,number_winner=number_winner, first= None)

	ax = fig.add_subplot(232)
	ax.set_xlabel('Number patterns for which \n a cell is active')
	ax.hist([hippo.Ca3_Ca1.Cor['RecalledRecalledRec'].getFireTimes(), hippo100.Ca3_Ca1.Cor['RecalledRecalledRec'].getFireTimes()], align = 'left', bins = range(0, 100), label = ['a = 0.01','a = 0.1'], histtype = 'step', color = ['m', 'k'])	
	ax.set_xlim(-0.4,x_max)
	ax.set_aspect(ax.get_xlim()[1]/(ax.get_ylim()[1]*1.), 'box')
	ax.legend(loc='upper right',prop={'size':9})
	ax.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	makeLabel(ax = ax, label = 'B', sci = 1 )
	plt.gca().set(adjustable='box')
	for tick in ax.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	hippo100.first = 10
	hippo100.recall()
	ax = fig.add_subplot(233, aspect = 'equal')
	ax.set_xlabel('Cue quality ')
	ax.set_ylabel('Correlation', multialignment = 'center')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'First 10', marker = 'd')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--', marker = 'd')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'First 10', marker = 'd')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r', linestyle = '--', marker = 'd')
	hippo100.first = -10
	hippo100.recall()
	ax.plot(In.getOrigVsOrig(), hippo100.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'Last 10', marker = '*')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--', marker = '*')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'Last 10', marker = '*')
	ax.plot(In.getOrigVsOrig(), hippo100.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r', linestyle = '--', marker = '*')
	ax.plot(np.linspace(0,1, 10), np.linspace(0,1, 10), c='k')
	ax.set_ylim(0, 1.)
	ax.set_xlim(0, 1.)
	ax.legend(loc='lower left',prop={'size':9}, numpoints = 1)
	makeLabel(ax = ax, label = 'C', sci = 0 )
	for tick in ax.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	connectivity = Parameter.connectivity
	connectivity['Ca1_Ec'] = 200./1000
	number_winner['Ca1'] = 10
	hippo = HippocampusCa3Ca1OneShoot(In = In, just_ca3=0, rec = 1, Ca3Activation = Ca3Activation, learnrate = learn,number_winner=number_winner, first= None, connectivity = connectivity)
	number_winner['Ca1'] = 100
	hippoHetero = HippocampusFull(In = In, just_ca3=0, rec = 1, Ca3Activation = Ca3Activation, learnrate = learn,number_winner=number_winner, first= None, connectivity=connectivity)
	
	ax = fig.add_subplot(234, aspect = 'equal')
	ax.set_xlabel('Cue quality ')
	ax.set_ylabel(r"$Corr_{EC}$")
	ax.plot(In.getOrigVsOrig(), hippo.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'a = 0.01', marker = 'd')
	ax.plot(In.getOrigVsOrig(), hippo.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--', marker = 'd')
	ax.plot(In.getOrigVsOrig(), hippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'a = 0.1', marker = '*')
	ax.plot(In.getOrigVsOrig(), hippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--', marker = '*')
	ax.plot(np.linspace(0,1, 10), np.linspace(0,1, 10), c='k')
	ax.set_ylim(0, 1.)
	ax.set_xlim(0, 1.)
	ax.legend(loc='lower right',prop={'size':9}, numpoints =1)
	makeLabel(ax = ax, label = 'D', sci = 0 )
	for tick in ax.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)

	connectivity = dict(Ec_Dg = 0.32, Dg_Ca3 = 0.0006, Ca3_Ec = 0.32, Ec_Ca3 =0.32, Ca3_Ca3 = 0.24, Ca3_Ca1 = 0.32, Ca1_Ec = 0.32, Ec_Ca1 = 0.32, Ca1_Sub = 0.32, Sub_Ec = 0.32, Ec_Sub = 0.32)
	cells = dict(Ec = 1100, Dg = 12000, Ca3 =2500, Ca1 = 4200)
	
	sparsity = dict(Ec = 0.35, Dg = 0.005, Ca3 = 0.032, Ca1 = 0.09)#activity level of each region (if WTA network)
	number_winner = dict(Ec = int(cells['Ec']*sparsity['Ec']), Dg = int(cells['Dg']*sparsity['Dg']), Ca3 = int(cells['Ca3']*sparsity['Ca3']), Ca1 = int(cells['Ca1']*sparsity['Ca1']) ) 
	act =dict(Ec_Dg = Network.getOutputWTALinear, Dg_Ca3 = Network.getOutputWTA, Ca3_Ec = Network.getOutputWTALinear, Ec_Ca3 = Network.getOutputWTA, Ca3_Ca3 = AutoAssociation.getOutputWTA, Ca3_Ca1= Network.getOutputWTALinear, Ca1_Ec = Network.getOutputWTA, Ec_Ca1 = Network.getOutputWTALinear)

	In = Input(number_patterns = 300, number_to_store = 250, inputMethod = Input.makeInputSparsityLevel, noiseMethod = Input.makeNoiseZero, number_cells = cells['Ec'], sparsity = sparsity['Ec'],noise_levels = np.arange(0,Parameter.cells['Ec']+1, int(Parameter.cells['Ec']/19)),n_e=1)
	
	hippo = HippocampusFull(In = In, just_ca3=0, rec = 1, learnrate = learn,number_winner=number_winner, cells = cells, first= None, connectivity=connectivity, actFunctionsRegions = act, active_in_env = cells)
	
	ax5 = fig.add_subplot(235, aspect = 'equal')
	ax5.set_ylim(0, 1.)
	ax5.set_xlim(0, 1.)
	ax5.set_xlabel('Cue quality ')
	ax5.set_ylabel('Correlation', multialignment = 'center')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'Ca3')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'Ca1')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'Ec' )
	ax5.plot(In.getOrigVsOrig(), hippo.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g',linestyle = '--')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r',linestyle = '--')
	ax5.plot(In.getOrigVsOrig(), hippo.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b',linestyle = '--', label = 'No Rec')
	ax5.plot(np.linspace(0,1, 10), np.linspace(0,1, 10), c='k')
	makeLabel(ax = ax5, label = 'E', sci = 0 )
	for tick in ax5.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	fig.tight_layout(rect = (0,0,1,0.97))
	
	
	collaps = 0
	
#Figure 3
def overlapActiveCa3Random():
	
	fig = plt.figure()


	# Random
	In = load('Random_252.p')
	In2 = load('GridMod1_252_Env1.p')
	In.locations = In2.locations
	In.store_indizes = In2.store_indizes

	title = 'Rand_252'
	ca3_code = ''
	HippoDg = load('Rand_252_DG05.p')	
	Hippo0 = load('Rand_252_DG0.p')
	HippoCa3Random = load('Rand_252_Ca3Rand.p')

	

	#dg strategies
	r_values = []
	In.one_patterns = 1
	ax = fig.add_subplot(221)
	ax.set_xlabel('Input Correlation')
	ax.set_ylabel('CA3 Correlation')
	ax.set_xlim(-0.1, 1.1)
	ax.set_ylim( -0.1, 1.1)
	ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoDg.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), edgecolors = 'g', facecolors = 'w')
	line, r = regressionLine(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoDg.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
	r_values.append(r)
	ax.plot([0,1], [line(0), line(1)], c = 'g')
	ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), edgecolors = 'b', facecolors = 'w')
	line, r = regressionLine(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
	r_values.append(r)
	ax.plot([0,1], [line(0), line(1)], c = 'b')
	ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), edgecolors = 'r', facecolors = 'w')
	line, r = regressionLine(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
	r_values.append(r)
	ax.plot([0,1], [line(0), line(1)], c = 'r')
	makeLabel(ax = ax, label = 'A', sci = 0 )
	r_values = np.round(r_values, 2)
	ax.text(0, 0.9, s = 'r = '+str(r_values[0]), color = 'g')
	ax.text(0, 0.8, s = 'r = '+str(r_values[1]), color = 'b')
	ax.text(0, 0.7, s = 'r = '+str(r_values[2]), color = 'r')
	
	
	
	#performance ca3
	ax1 = fig.add_subplot(222)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA3}$")
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'plastic DG')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'b', label = 'static DG')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), label = 'random CA3')
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'r', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	ax1.legend(loc='lower right',prop={'size':9})
	makeLabel(ax = ax1, label = 'B', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	
	#performance CA1
	ax1 = fig.add_subplot(223)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA1}$")
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'g', label = 'learn')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'no learn')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'CA3 Random')
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax1, label = 'C', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)

	#performance Ec
	ax2 = fig.add_subplot(224)
	ax2.set_ylim(0, 1.)
	ax2.set_xlim(0, 1.)
	ax2.set_xlabel('Cue quality ')
	ax2.set_ylabel(r"$Corr_{EC}$")
	ax2.plot(In.getOrigVsOrig(), HippoDg.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'g', label = 'learn')
	ax2.plot(In.getOrigVsOrig(), Hippo0.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'no learn')
	ax2.plot(In.getOrigVsOrig(), HippoCa3Random.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'CA3 Random')
	ax2.plot(In.getOrigVsOrig(), HippoDg.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), Hippo0.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), HippoCa3Random.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax2, label = 'D', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	fig.tight_layout(rect = (0,0,1,0.97))

#Figure 4
def dgNodgMod():
	
	###Grid 
	In = load('GridMod1_252_Env1.p')
	HippoDg = load('GridMod1_252_Env1_DG05.p')
	Hippo0 = load('GridMod1_252_Env1_DG0.p')
	HippoCa3Random = load('GridMod1_252_Env1_Ca3Rand.p')

	fig = plt.figure(figsize = (7.5, 8.75))
	fig.subplots_adjust(left = .13, bottom = .08, right = .96, top = .95, wspace=0.5, hspace=0.5)
	
	# examples of cells
	grid = ImageGrid(fig, [4,3,1], nrows_ncols = (3,3),axes_pad=0.0, aspect = True,share_all=True)
	cells = np.random.randint(0,2500, size = 9)
	cells[0:6] = np.array([177, 2113, 1333, 904 ,1991 , 858 ])

	for i in range(9):
		
		grid[i].set_aspect(1.0)
		if i in [0,1,2]:
			c = 'g'
			ax, s = plotCell(In =In, cell = cells[i], fig = fig, ax= grid[i], env = 0, patterns = HippoDg.Ec_Ca3.Cor['StoredStored'].patterns_2, color = c, binary = True, size = 8)
		if i in [3,4,5]:
			c = 'b'
			ax,s  = plotCell(In =In, cell = cells[i], fig = fig, ax= grid[i], env = 0, patterns = Hippo0.Ec_Ca3.Cor['StoredStored'].patterns_2, color = c, binary = True, size = 8)
		if i in [6,7,8]:
			c = 'r'
			ax,s = plotCell(In =In, cell = cells[i], fig = fig, ax= grid[i], env = 0, patterns = HippoCa3Random.Ec_Ca3.Cor['StoredStored'].patterns_2, color = c, binary = True, size = 8)

		plt.setp(ax.get_yticklabels(), visible=False)
		plt.setp(ax.get_yticklines(),visible=False)
		plt.setp(ax.get_xticklabels(), visible=False)
		plt.setp(ax.get_xticklines(),visible=False)
	makeLabel(ax = grid[0], label = 'A', sci = 1 )
	grid[6].set_ylabel('1m')
	grid[6].set_xlabel('1m')
	
	###dg strategies
	r_values = []
	In.one_patterns = 1
	ax = fig.add_subplot(4,3,2)
	ax.set_xlabel('Input Corr')
	ax.set_ylabel('CA3 Corr')
	ax.set_xlim(-0.1, 1.1)
	ax.set_ylim( -0.1, 1.1)
	ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoDg.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), edgecolors = 'g', facecolors = 'w')
	line, r = regressionLine(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoDg.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), relevant = -.2)
	r_values.append(r)
	ax.plot([0,1], [line(0), line(1)], c = 'g')
	ax.annotate(s = str(np.round(line.coeffs[0],2)), xy = [1, line(1)], xytext = [.87, line(1)-0.12], color = 'g', size = 'small')
	ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), edgecolors = 'b', facecolors = 'w')
	line, r = regressionLine(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), relevant = -.2)
	r_values.append(r)
	ax.plot([0,1], [line(0), line(1)], c = 'b')
	ax.annotate(s = str(np.round(line.coeffs[0],2)), xy = [1, line(1)], xytext = [.87, line(1)-0.12], color = 'b', size = 'small')
	ax.scatter(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), edgecolors = 'r', facecolors = 'w')
	line, r = regressionLine(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), relevant = -.2)
	r_values.append(r)
	ax.plot([0,1], [line(0), line(1)], c = 'r')
	ax.annotate(s = str(np.round(line.coeffs[0],2)), xy = [1, line(1)], xytext = [.87, line(1)-0.1], color = 'r', size = 'small')
	ax.text(0.8, 1, s = 'r=', color = 'g', size = 'small')
	ax.text(.86, .95, s = str(np.round(r_values[0],2)), color = 'g', size = 'small')
	ax.text(.86, 0.85, s = str(np.round(r_values[1],2)), color = 'b', size = 'small')
	ax.text(.86, 0.75, s = str(np.round(r_values[2],2)), color = 'r', size = 'small')
	for tick in ax.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	makeLabel(ax = ax, label = 'B', sci = 0 )
	
	##performance ca3
	ax1 = fig.add_subplot(433)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA3}$")
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'plastic DG')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'b', label = 'static DG')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), label = 'random CA3')
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'r', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	ax1.legend(loc='best',prop={'size':9})
	makeLabel(ax = ax1, label = 'C', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	
	#performance CA1
	ax1 = fig.add_subplot(434)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA1}$")
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'g', label = 'learn')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'no learn')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ca3_Ca1.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'CA3 Random')
	ax1.plot(In.getOrigVsOrig(), HippoDg.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), Hippo0.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), HippoCa3Random.Ca3_Ca1.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax1, label = 'D', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)

	#performance Ec
	ax2 = fig.add_subplot(435)
	ax2.set_ylim(0, 1.)
	ax2.set_xlim(0, 1.)
	ax2.set_xlabel('Cue quality ')
	ax2.set_ylabel(r"$Corr_{EC}$")
	ax2.plot(In.getOrigVsOrig(), HippoDg.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'g', label = 'learn')
	ax2.plot(In.getOrigVsOrig(), Hippo0.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'no learn')
	ax2.plot(In.getOrigVsOrig(), HippoCa3Random.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'r', label = 'CA3 Random')
	ax2.plot(In.getOrigVsOrig(), HippoDg.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), Hippo0.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), HippoCa3Random.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'r', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax2, label = 'E', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	##histogramms activation ca3
	ax15= fig.add_subplot(4,3,6)
	ax15.hist(HippoCa3Random.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[1], normed = 1, histtype = 'step', bins =50, color = 'k', linestyle = 'dashed', label = 'silent cells')
	ax15.hist(HippoDg.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[0], label = 'active cells', normed = 1, histtype = 'step', bins=50, color = 'k')
	ax15.hist(HippoDg.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[0], normed = 1, histtype = 'step', bins=50, color = 'g')
	ax15.hist(HippoDg.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[1], normed = 1, histtype = 'step', bins =50, color = 'g', linestyle = 'dashed')
	ax15.hist(Hippo0.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[0], normed = 1, histtype = 'step', bins=50, color = 'b')
	ax15.hist(Hippo0.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[1], normed = 1, histtype = 'step', bins =50, color = 'b', linestyle = 'dashed')
	ax15.hist(HippoCa3Random.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[0], normed = 1, histtype = 'step', bins=50, color = 'r')
	ax15.hist(HippoCa3Random.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0])[1], normed = 1, histtype = 'step', bins =50, color = 'r', linestyle = 'dashed')
	ax15.set_xlim(-50, 152)
	ax15.set_xlabel(r"$h_i of CA3 cell$")
	ax15.set_xlabel(r"$h_i$" +' of CA3 cell')
	ax15.set_ylabel(r"$pdf(h_i)$")
	ax15.legend(loc = 'upper right',prop={'size':8})
	ax15.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	makeLabel(ax = ax15, label = 'F', sci = 1)
	ax15.minorticks_on()
	locator = mpl.ticker.FixedLocator([Hippo0.Ca1_Ec.Cor['StoredStored'].getOverOrigVsOrigNoMean(at_noise = 0)*Hippo0.connectivity['Ec_Ca3']], nbins=None)

	[active, silent] = HippoCa3Random.Ec_Ca3.getActivationActiveSilentCells(input_pattern = In.input_stored[0]) 

	
	formater = mpl.ticker.FixedFormatter(['S'])
	ax15.xaxis.set_minor_locator(locator)
	ax15.xaxis.set_minor_formatter(formater)
	ax15.tick_params('x', direction = 'in', length=15, width=3, color = 'k',labelcolor = 'k', labelsize =12, which='minor')
	for tick in ax15.xaxis.get_major_ticks():
		tick.label1.set_visible(False)
	locs = np.array(ax15.xaxis.get_majorticklocs())
	ax15.xaxis.get_major_ticks()[np.flatnonzero(locs == -50)].label1.set_visible(1)
	ax15.xaxis.get_major_ticks()[np.flatnonzero(locs == 0)].label1.set_visible(1)
	ax15.xaxis.get_major_ticks()[np.flatnonzero(locs == 150)].label1.set_visible(1)
	ax15.xaxis.get_major_ticks()[-1].label1.set_visible(False)


	
	#crosstalks
	overs = []
	ax5 = fig.add_subplot(4,3,7)
	overs.append(In.getOverNoMeanSilentCells(output_patterns = HippoDg.Ec_Ca3.Cor['StoredStored'].patterns_1))
	overs.append(In.getOverNoMeanActiveCells(output_patterns = HippoDg.Ec_Ca3.Cor['StoredStored'].patterns_1))
	overs.append(np.concatenate((overs[0], overs[1])))
	for o in overs:
		o *= HippoDg.connectivity['Ec_Ca3']
	ax5.set_xlabel(r"$X_{(i,s,t)}$")
	ax5.set_ylabel(r"$pdf(X_{(i,s,t)})$")
	ax5.hist(overs[1], linestyle = 'solid', normed = 1, histtype = 'step', bins=50, label = r"$q_i^{(t)}=1$", color = 'g')
	ax5.hist(overs[0], linestyle = 'dashed', normed = 1, histtype = 'step', bins=50, label = r"$q_i^{(t)}=0$", color = 'g')
	ax5.set_xlim(-8,23)
	ax5.legend(loc = 'best',prop={'size':9})
	makeLabel(ax = ax5, label = 'G', sci = 0 )
	
	locator3 = mpl.ticker.FixedLocator(np.arange(0, 17, 4)/100., nbins=None)
	formater3 = mpl.ticker.FixedFormatter(['0', '.04', '.08', '.12', '.16'])
	ax5.yaxis.set_major_locator(locator3)
	ax5.yaxis.set_major_formatter(formater3)
	for tick in ax5.xaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)


	#Ec overlap
	bins = np.concatenate((np.arange(0,0.2,0.02), np.linspace(0.2,1.1, 8), np.array([1.415])))
	ax = fig.add_subplot(438)
	ax.set_xlabel('Distance (s,t)')
	ax.set_ylabel(r"$(\mathbf{p}^{(s)} - \bar{p})^T\mathbf{p}^{(t)} >$")
	ax.set_xlim(0,1.41)
	av = In.getAverageOverlapOverNoMeanDistance(locations = In.getStoredlocations(), bins = bins)
	ax.plot(av[1], av[0], marker = 'o', c = 'k')
	makeLabel(ax = ax, label = 'H', sci = 0 )
	for tick in ax.xaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax.yaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)
	

	ax1 = fig.add_subplot(439)
	ax1.set_xlim(0,1.41)
	dis = HippoDg.Ec_Ca3.Cor['StoredStored'].getAllDifferentDistances(locations = In.getStoredlocations(), bins = bins)[1:]
	
	#relation p and over
	ax = fig.add_subplot(4,3,10)
	p_given_active_non = Hippo0.Ec_Ca3.Cor['StoredStored'].getPFireGivenFireAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax1.plot(dis,p_given_active_non ,label = 'static DG', color = 'b', marker = 'o')
	###inset	
	x0, y0, width, height = [0.5,.1, .7, .17 ]
	ax11 = makeInset(fig = fig, ax = ax1, ax_xy = [x0, y0], ax_width = width, ax_height = height )
	ax11.plot(dis,p_given_active_non ,label = 'static DG', color = 'b', marker = 'o')
	ax.scatter(p_given_active_non, av[0][1:], c = 'b')
	
	p_given_active_non = HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getPFireGivenFireAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax1.plot(dis,p_given_active_non ,label = 'random CA3', color = 'r', marker = 'o')
	ax11.plot(dis,p_given_active_non ,label = 'random CA3', color = 'r', marker = 'o')
	ax.scatter(p_given_active_non, av[0][1:], c = 'r')
	
	p_given_active_non = HippoDg.Ec_Ca3.Cor['StoredStored'].getPFireGivenFireAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax1.plot(dis,p_given_active_non ,label = 'plastic DG', color = 'g', marker = 'o')
	ax11.plot(dis,p_given_active_non ,label = 'plastic DG', color = 'g', marker = 'o')
	ax.scatter(p_given_active_non, av[0][1:], c = 'g')
	
	ax1.set_ylabel(r"$P(q^{(s)}=1|q^{(t)}=1)$")
	ax1.set_xlabel('Distance (s,t)')
	makeLabel(ax = ax1, label = 'I', sci = 0 )

	locator = mpl.ticker.FixedLocator([0, .1 , .2 , .3], nbins=None)
	formater = mpl.ticker.FixedFormatter(['0', '.1' , '.2' , '.3'])
	ax1.yaxis.set_major_locator(locator)
	ax1.yaxis.set_major_formatter(formater)
	for tick in ax1.xaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)

	ax11.set_xlim(0.05, .21)
	ax11.set_ylim(0, .303)
	ax11.tick_params(labelsize = 12)

	ax.set_ylabel(r"$(\mathbf{p}^{(s)} - \bar{p})^T\mathbf{p}^{(t)} >$")
	ax.set_xlabel(r"$P(q^{(s)}=1|q^{(t)}=1)$")
	makeLabel(ax = ax, label = 'J', sci = 0 )
	for tick in ax.yaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)
	ticks = ax.xaxis.get_major_ticks()
	for i in [0,2,3,5,6,8]:
		ticks[i].label1.set_visible(False)



	#P(q| silent)
	bins = np.concatenate((np.arange(0,0.2,0.02), np.linspace(0.2,1.1, 8), np.array([1.415])))
	ax1 = fig.add_subplot(4,3,11)
	dis = HippoDg.Ec_Ca3.Cor['StoredStored'].getAllDifferentDistances(locations = In.getStoredlocations(), bins = bins)[1:]
	p_given_active_non = Hippo0.Ec_Ca3.Cor['StoredStored'].getPFireGivenSilentAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax1.plot(dis,p_given_active_non ,label = 'static DG', color = 'b', marker = 'o')
	p_given_active_non = HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getPFireGivenSilentAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax1.plot(dis,p_given_active_non ,label = 'random CA3', color = 'r', marker = 'o')
	p_given_active_non = HippoDg.Ec_Ca3.Cor['StoredStored'].getPFireGivenSilentAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax1.plot(dis,p_given_active_non ,label = 'plastic DG', color = 'g', marker = 'o')
	ax1.set_ylabel(r"$P(q^{(s)}=1|q^{(t)}=0)$")
	ax1.set_xlabel('Distance (s,t)')
	ax1.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	makeLabel(ax = ax1, label = 'K', sci = 1 )
	ax1.set_xlim(0,1.41)
	for tick in ax1.xaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax1.yaxis.get_major_ticks()[1:-1:2]:
		tick.label1.set_visible(False)
	ax1.xaxis.get_major_ticks()[-1].label1.set_visible(False)

	x0, y0, width, height = [0.4,.022, .8, .006 ]
	ax11 = makeInset(fig = fig, ax = ax1, ax_xy = [x0, y0], ax_width = width, ax_height = height )	
	dis = HippoDg.Ec_Ca3.Cor['StoredStored'].getAllDifferentDistances(locations = In.getStoredlocations(), bins = bins)[1:]
	p_given_active_non = Hippo0.Ec_Ca3.Cor['StoredStored'].getPFireGivenSilentAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax11.plot(dis,p_given_active_non ,label = 'static DG', color = 'b', marker = 'o')
	p_given_active_non = HippoCa3Random.Ec_Ca3.Cor['StoredStored'].getPFireGivenSilentAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax11.plot(dis,p_given_active_non ,label = 'random CA3', color = 'r', marker = 'o')
	p_given_active_non = HippoDg.Ec_Ca3.Cor['StoredStored'].getPFireGivenSilentAndDistanceAll(locations = In.getStoredlocations(), bins = bins)[1:]
	ax11.plot(dis,p_given_active_non ,label = 'plastic DG', color = 'g', marker = 'o')	
	
	ax11.set_xlim(0.05, .21)
	ax11.set_ylim(.0179, .0341)
	ax11.tick_params(labelsize = 12)
	
	#activations ca1
	ax10= fig.add_subplot(4,3,12)
	ax10.hist(HippoDg.Ca3_Ca1.getActivationActiveSilentCells(input_pattern = HippoDg.Ec_Ca3.Cor['StoredStored'].patterns_1)[0], label = 'active cells', normed = 1, histtype = 'step', bins=50, color = 'g')
	ax10.hist(HippoDg.Ca3_Ca1.getActivationActiveSilentCells(input_pattern = HippoDg.Ec_Ca3.Cor['StoredStored'].patterns_1)[1], label = 'silent cells', normed = 1, histtype = 'step', bins =50, color = 'g', linestyle = 'dashed')
	ax10.hist(Hippo0.Ca3_Ca1.getActivationActiveSilentCells(input_pattern = Hippo0.Ec_Ca3.Cor['StoredStored'].patterns_1)[0], normed = 1, histtype = 'step', bins=50, color = 'b')
	ax10.hist(Hippo0.Ca3_Ca1.getActivationActiveSilentCells(input_pattern = Hippo0.Ec_Ca3.Cor['StoredStored'].patterns_1)[1], normed = 1, histtype = 'step', bins =50, color = 'b', linestyle = 'dashed')
	ax10.hist(HippoCa3Random.Ca3_Ca1.getActivationActiveSilentCells(input_pattern = HippoCa3Random.Ec_Ca3.Cor['StoredStored'].patterns_1)[0], normed = 1, histtype = 'step', bins=50, color = 'r')
	ax10.hist(HippoCa3Random.Ca3_Ca1.getActivationActiveSilentCells(input_pattern = HippoCa3Random.Ec_Ca3.Cor['StoredStored'].patterns_1)[1], normed = 1, histtype = 'step', bins =50, color = 'r', linestyle = 'dashed')
	ax10.set_xlim(-100, 300)
	ax10.set_xlabel(r"$h_i$" +' of CA1 cell')
	ax10.set_ylabel(r"$pdf(h_i)$")
	ax10.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	locator2 = mpl.ticker.FixedLocator(np.arange(5)/100., nbins=None)
	formater2 = mpl.ticker.FixedFormatter(['0', '1', '2', '3', '4'])
	ax10.yaxis.set_major_locator(locator2)
	ax10.yaxis.set_major_formatter(formater2)
	makeLabel(ax = ax10, label = 'L', sci = 1 )

	for tick in ax10.xaxis.get_major_ticks():
		tick.label1.set_visible(False)
	locs = np.array(ax10.xaxis.get_majorticklocs())
	ax10.xaxis.get_major_ticks()[np.flatnonzero(locs +100 == 0)].label1.set_visible(1)
	ax10.xaxis.get_major_ticks()[np.flatnonzero(locs == 0)].label1.set_visible(1)
	ax10.xaxis.get_major_ticks()[np.flatnonzero(locs -300 == 0)].label1.set_visible(1)	
	collapse = 0

#Figure 5
def randomOverviewNew():

	In = load('Random_252.p')
	title = 'Rand_252'
	HippoHetero = load('Rand_252_DG0.p')


	fig = plt.figure(figsize = (7.5, 8.75))

	
	hist_type = 'bar'
	line_width = 1
	n = -4
	noise_cor = np.round(In.getOrigVsOrig()[n], 2)

	ax1 = fig.add_subplot(421)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA3}$")
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'Rec')
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'No Rec', linestyle = '--')
	ax1.plot(In.getOrigVsOrig()[n], HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], c='r', marker = 'd')
	ax1.plot(In.getOrigVsOrig()[n], HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], c='r', marker = 'd')#, markersize = 12)
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax1, label = 'A', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax1.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)


	ax2 = fig.add_subplot(422)
	ax2.set_ylim(0, 1.)
	ax2.set_xlim(0, 1.)
	ax2.set_xlabel('Cue quality ')
	ax2.set_ylabel(r"$Corr_{EC}$")
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig()[n], HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig()[n], c='r', marker = 'd')
	ax2.plot(In.getOrigVsOrig()[n], HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig()[n], c='r', marker = 'd')
	ax2.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax2, label = 'B', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	
	
	bins = np.linspace(-1, 1, 50 )
	ax41 = fig.add_subplot(425)
	ax42 = ax41.twinx()
	ax41.hist(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax42.hist(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax42.hist(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + '\n if not max', color = 'b', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax41.set_xlabel('Correlation')
	ax41.set_xlim(-0.2, 1.01)
	ax41.legend(ax41.get_legend_handles_labels()[0] +ax42.get_legend_handles_labels()[0], ax41.get_legend_handles_labels()[1]+ax42.get_legend_handles_labels()[1], loc = 'upper right',prop={'size':9})
	ax42.plot(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax41.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	
	ax41.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax42.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax41.yaxis.label.set_color('red')
	ax41.tick_params(axis='y', colors='red')
	ax42.yaxis.label.set_color('c')
	ax42.tick_params(axis='y', colors='c')






	ax51 = fig.add_subplot(427)
	ax52 = ax51.twinx()
	ax51.hist(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax52.hist(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax52.hist(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + ',if not max', color = 'b', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax51.set_xlabel('Correlation')
	ax51.set_xlim(-0.2, 1.01)
	ax51.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center')
	ax52.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center')
	ax51.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	ax52.plot(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax51.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax52.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax51.yaxis.label.set_color('red')
	ax51.tick_params(axis='y', colors='red')
	ax52.yaxis.label.set_color('c')
	ax52.tick_params(axis='y', colors='c')


	
	ax20 = fig.add_subplot(423)
	ax20.set_ylim(0,100)
	ax20.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax20.set_xlabel('Cue quality ')
	ax20.plot(In.getOrigVsOrig(), 100 - HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'Rec')
	ax20.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'No Rec', linestyle = '--')
	ax20.set_xlim(0,1)
	ax20.set_ylim(0,100)
	ax20.plot(In.getOrigVsOrig()[n], 100 -HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	ax20.plot(In.getOrigVsOrig()[n], 100 -HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	makeLabel(ax = ax20, label = 'C', sci = 0 )
	for tick in ax20.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax20.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	
	ax2 = fig.add_subplot(424)
	ax2.set_ylim(0,100)
	ax2.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax2.set_xlabel('Cue quality ')
	ax2.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong(), c = 'b', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(),100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong(), c = 'b', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig()[n],100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	ax2.plot(In.getOrigVsOrig()[n],100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	makeLabel(ax = ax2, label = 'D', sci = 0 )
	ax2.set_xlim(0,1)
	ax2.set_ylim(0,100)
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)


	ax71 = fig.add_subplot(426)
	ax72 = ax71.twinx()
	ax71.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r',linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax72.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax72.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + ',if not max', color = 'b',linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax71.set_xlabel('Correlation')
	ax71.set_xlim(-0.2, 1.01)
	ax71.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center')
	ax72.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center')
	ax71.xaxis.get_major_ticks()[-1].label1.set_visible(False)
	ax71.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	ax72.plot(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax71.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax72.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax71.yaxis.label.set_color('red')
	ax71.tick_params(axis='y', colors='red')
	ax72.yaxis.label.set_color('c')
	ax72.tick_params(axis='y', colors='c')



	ax81 = fig.add_subplot(428)
	ax82 = ax81.twinx()
	ax81.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax82.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax82.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + ',if not max', color = 'b',linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax81.set_xlabel('Correlation')
	ax81.set_xlim(-0.2, 1.01)
	ax81.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	ax81.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center')
	ax82.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center')
	ax82.plot(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig()[n], np.max(h[0])*.9, c= 'k', marker = '*', markersize = 10)
	ax81.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax82.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax81.yaxis.label.set_color('red')
	ax81.tick_params(axis='y', colors='red')
	ax82.yaxis.label.set_color('c')
	ax82.tick_params(axis='y', colors='c')


	
	
	max_other = max(ax81.get_ylim()[1], ax71.get_ylim()[1],ax51.get_ylim()[1],ax41.get_ylim()[1])
	max_orig = max(ax82.get_ylim()[1], ax72.get_ylim()[1],ax52.get_ylim()[1],ax42.get_ylim()[1])
	ax81.set_ylim(0, max_other)
	ax82.set_ylim(0, max_orig)
	ax71.set_ylim(0, max_other)
	ax72.set_ylim(0, max_orig)
	ax51.set_ylim(0, max_other)
	ax52.set_ylim(0, max_orig)
	ax41.set_ylim(0, max_other)
	ax42.set_ylim(0, max_orig)

	makeLabel(ax = ax41, label = 'E', sci = 1 )
	makeLabel(ax = ax51, label = 'G', sci = 1 )
	makeLabel(ax = ax71, label = 'F', sci = 1 )
	makeLabel(ax = ax81, label = 'H', sci = 1 )

	
	for ax in [ax51, ax41, ax71, ax81]:
		for tick in ax.xaxis.get_major_ticks()[0:-1:2]:
			tick.label1.set_visible(False)
		for tick in ax.yaxis.get_major_ticks()[0:-1:2]:
			tick.label1.set_visible(False)
	for ax in [ax52, ax42, ax72, ax82]:
		for tick in ax.get_yticklabels(which = 'both')[0:-1:2]:
			print tick
			tick.set_color('w')
			tick.set_text('')

	
	fig.tight_layout(rect = (0,0,1,0.97))
	ax22 = makeInset(fig = fig, ax = ax20, ax_xy = (0.2365, -7),ax_width = 0.65, ax_height = 115)
	img=mpimg.imread('illustration2.PNG')
	ax22.imshow(img)
	ax22.axis('off')
	
	
	collapse = 0

#Figure 6
def gridOverviewNewMod():
	
	print 'gridoverviewNewMod'
	In = load('GridMod1_252_Env1.p')
	title = 'GridMod1_252_Env1.p'
	HippoHetero = load('GridMod1_252_Env1_DG0.p')	
	fig = plt.figure(figsize = (7.5, 8.75))

	
	hist_type = 'bar'
	line_width = 1
	n = -7
	noise_cor = np.round(In.getOrigVsOrig()[n], 2)

	ax1 = fig.add_subplot(421)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA3}$")
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'Rec')
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'No Rec', linestyle = '--')
	ax1.plot(In.getOrigVsOrig()[n], HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], c='r', marker = 'd')
	ax1.plot(In.getOrigVsOrig()[n], HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], c='r', marker = 'd')#, markersize = 12)
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax1, label = 'A', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax1.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)


	ax2 = fig.add_subplot(422)
	ax2.set_ylim(0, 1.)
	ax2.set_xlim(0, 1.)
	ax2.set_xlabel('Cue quality ')
	ax2.set_ylabel(r"$Corr_{EC}$")
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig()[n], HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig()[n], c='r', marker = 'd')
	ax2.plot(In.getOrigVsOrig()[n], HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig()[n], c='r', marker = 'd')
	ax2.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax2, label = 'B', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	
	
	bins = np.linspace(-1, 1, 50 )
	ax41 = fig.add_subplot(425)
	ax42 = ax41.twinx()
	ax41.hist(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax42.hist(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax42.hist(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + '\n if not max', color = 'b', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax41.set_xlabel('Correlation')
	ax41.set_xlim(-0.2, 1.01)
	ax41.legend(ax41.get_legend_handles_labels()[0] +ax42.get_legend_handles_labels()[0], ax41.get_legend_handles_labels()[1]+ax42.get_legend_handles_labels()[1], loc = 'upper right',prop={'size':9})
	ax42.plot(HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax41.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	
	ax41.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax42.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax41.yaxis.label.set_color('red')
	ax41.tick_params(axis='y', colors='red')
	ax42.yaxis.label.set_color('c')
	ax42.tick_params(axis='y', colors='c')






	ax51 = fig.add_subplot(427)
	ax52 = ax51.twinx()
	ax51.hist(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax52.hist(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax52.hist(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + ',if not max', color = 'b', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax51.set_xlabel('Correlation')
	ax51.set_xlim(-0.2, 1.01)
	ax51.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center')
	ax52.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center')
	ax51.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	ax52.plot(HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax51.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax52.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax51.yaxis.label.set_color('red')
	ax51.tick_params(axis='y', colors='red')
	ax52.yaxis.label.set_color('c')
	ax52.tick_params(axis='y', colors='c')


	
	ax20 = fig.add_subplot(423)
	ax20.set_ylim(0,100)
	ax20.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax20.set_xlabel('Cue quality ')
	ax20.plot(In.getOrigVsOrig(), 100 - HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'Rec')
	ax20.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'No Rec', linestyle = '--')
	ax20.set_xlim(0,1)
	ax20.set_ylim(0,100)
	ax20.plot(In.getOrigVsOrig()[n], 100 -HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	ax20.plot(In.getOrigVsOrig()[n], 100 -HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	makeLabel(ax = ax20, label = 'C', sci = 0 )
	for tick in ax20.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax20.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	
	ax2 = fig.add_subplot(424)
	ax2.set_ylim(0,100)
	ax2.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax2.set_xlabel('Cue quality ')
	ax2.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong(), c = 'b', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(),100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong(), c = 'b', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig()[n],100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	ax2.plot(In.getOrigVsOrig()[n],100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong()[n], c='r', marker = 'd')
	makeLabel(ax = ax2, label = 'D', sci = 0 )
	ax2.set_xlim(0,1)
	ax2.set_ylim(0,100)
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)


	ax71 = fig.add_subplot(426)
	ax72 = ax71.twinx()
	ax71.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r',linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax72.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax72.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + ',if not max', color = 'b',linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax71.set_xlabel('Correlation')
	ax71.set_xlim(-0.2, 1.01)
	ax71.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center')
	ax72.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center')
	ax71.xaxis.get_major_ticks()[-1].label1.set_visible(False)
	ax71.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	ax72.plot(HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax71.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax72.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax71.yaxis.label.set_color('red')
	ax71.tick_params(axis='y', colors='red')
	ax72.yaxis.label.set_color('c')
	ax72.tick_params(axis='y', colors='c')



	ax81 = fig.add_subplot(428)
	ax82 = ax81.twinx()
	ax81.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getCorOrigOther(at_noise = n,subtract_orig_orig = 1),bins = bins, normed =0,align = 'left', rwidth = 0.5,label = r"$corr(\tilde p, q)$", color = 'r', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	h = ax82.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getCorOrigOrig(at_noise = n), bins = bins, normed =0, label = r"$corr(\tilde p, p)$", rwidth = 0.5,align = 'mid', color = 'c', linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	if HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getNotMaxCorrelations(at_noise = n).shape[0]>=1:
		ax82.hist(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getNotMaxCorrelations(at_noise = n),bins = bins, normed =0, rwidth = 0.5,align = 'mid',label = r"$corr(\tilde p,p)$" + ',if not max', color = 'b',linewidth = line_width , histtype = hist_type, edgecolor = 'none')
	ax81.set_xlabel('Correlation')
	ax81.set_xlim(-0.2, 1.01)
	ax81.ticklabel_format(style = 'sci', axis = 'y', scilimits = (0,0))
	ax81.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center')
	ax82.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center')
	ax82.plot(HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig()[n], np.max(h[0])*1.1, c= 'k', marker = '*', markersize = 10)
	ax81.set_ylabel(r"$corr(\tilde p, q)$", multialignment='center', color = 'r')
	ax82.set_ylabel(r"$corr(\tilde p, p)$", multialignment='center', color = 'c')
	ax81.yaxis.label.set_color('red')
	ax81.tick_params(axis='y', colors='red')
	ax82.yaxis.label.set_color('c')
	ax82.tick_params(axis='y', colors='c')


	
	
	max_other = max(ax81.get_ylim()[1], ax71.get_ylim()[1],ax51.get_ylim()[1],ax41.get_ylim()[1])
	max_orig = max(ax82.get_ylim()[1], ax72.get_ylim()[1],ax52.get_ylim()[1],ax42.get_ylim()[1])
	ax81.set_ylim(0, max_other)
	ax82.set_ylim(0, max_orig)
	ax71.set_ylim(0, max_other)
	ax72.set_ylim(0, max_orig)
	ax51.set_ylim(0, max_other)
	ax52.set_ylim(0, max_orig)
	ax41.set_ylim(0, max_other)
	ax42.set_ylim(0, max_orig)
	
	makeLabel(ax = ax41, label = 'E', sci = 1 )
	makeLabel(ax = ax51, label = 'G', sci = 1 )
	makeLabel(ax = ax71, label = 'F', sci = 1 )
	makeLabel(ax = ax81, label = 'H', sci = 1 )

	for ax in [ax51, ax41, ax71, ax81]:
		for tick in ax.xaxis.get_major_ticks()[0:-1:2]:
			tick.label1.set_visible(False)
		for tick in ax.yaxis.get_major_ticks()[0:-1:2]:
			tick.label1.set_visible(False)
	for ax in [ax52, ax42, ax72, ax82]:
		for tick in ax.get_yticklabels(which = 'both')[0:-1:2]:
			print tick
			tick.set_color('w')
			tick.set_text('')

	
	fig.tight_layout(rect = (0,0,1,0.97))
	collapse = 0

#Figure 7
def fullVsCa1NewMod():
	

	fig = plt.figure(figsize = (7.5, 8.75))

	

	In = load('Random_252.p')
	title = 'Rand_252'
	HippoHetero = load('Rand_252_DG0.p')
	HippoCa1 = load('Rand_252_EcCa1Ec.p')



	hist_type = 'bar'
	line_width = 1

	ax1 = fig.add_subplot(421)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality')
	ax1.set_ylabel(r"$Corr_{CA3\backslash CA1}$")
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'EC-CA3-CA1-EC')
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), HippoCa1.Ec_Ca1.Cor['StoredRecalled'].getOrigVsOrig(), c = 'm', label = 'EC-CA1-EC')
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	ax1.legend(loc='lower right',prop={'size':9}, numpoints =1)
	makeLabel(ax = ax1, label = 'A', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax1.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)


	ax2 = fig.add_subplot(423)
	ax2.set_ylim(0, 1.)
	ax2.set_xlim(0, 1.)
	ax2.set_xlabel('Cue quality ')
	ax2.set_ylabel(r"$Corr_{EC}$")
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'EC-CA3-CA1-EC')
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), HippoCa1.Ca1_Ec.Cor['StoredRecalled'].getOrigVsOrig(), c = 'm', label = 'EC-CA1-EC')
	ax2.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	ax2.legend(loc='lower right',prop={'size':9}, numpoints =1)
	makeLabel(ax = ax2, label = 'B', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	
	ax2 = fig.add_subplot(425)
	ax2.set_ylim(0,100)
	ax2.set_xlim(0,1)
	ax2.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax2.set_xlabel('Cue quality ')
	ax2.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(),100 -HippoCa1.Ec_Ca1.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'm', label = 'EC-CA1_EC')
	makeLabel(ax = ax2, label = 'C', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	ax2 = fig.add_subplot(427)
	ax2.set_ylim(0,100)
	ax2.set_xlim(0,1)
	ax2.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax2.set_xlabel('Cue quality')
	ax2.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong(), c = 'b', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(),100 - HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong(), c = 'b', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), 100 -HippoCa1.Ca1_Ec.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'm', label = 'EC-CA1-EC')
	makeLabel(ax = ax2, label = 'D', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	


	title = 'GridMod1_252_Env1'
	env=1
	In = load('GridMod1_252_Env'+str(env)+'.p')
	HippoHetero = load('GridMod1_252_Env'+str(env)+'_DG0.p')



	ax1 = fig.add_subplot(422)
	ax1.set_ylim(0, 1.)
	ax1.set_xlim(0, 1.)
	ax1.set_xlabel('Cue quality ')
	ax1.set_ylabel(r"$Corr_{CA3\backslash CA1}$")
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'Rec')
	ax1.plot(In.getOrigVsOrig(), HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getOrigVsOrig(), c = 'g', label = 'No Rec', linestyle = '--')
	ax1.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax1, label = 'E', sci = 0 )
	for tick in ax1.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax1.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)

	ax2 = fig.add_subplot(424)
	ax2.set_ylim(0, 1.)
	ax2.set_xlim(0, 1.)
	ax2.set_xlabel('Cue quality ')
	ax2.set_ylabel(r"$Corr_{EC}$")
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig(), c = 'b', label = 'Rec')
	ax2.plot(In.getOrigVsOrig(), HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig(), c = 'b', label = 'No Rec', linestyle = '--')
	ax2.plot(In.getOrigVsOrig(), In.getOrigVsOrig(), c='k')
	makeLabel(ax = ax2, label = 'F', sci = 0 )
	for tick in ax2.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax2.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)


	ax3 = fig.add_subplot(426)
	ax3.set_xlim(0,1)
	ax3.set_ylim(0,100)
	ax3.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax3.set_xlabel('Cue quality')
	ax3.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ca3_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'Rec')
	ax3.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ec_Ca3.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'g', label = 'No Rec', linestyle = '--')
	makeLabel(ax = ax3, label = 'G', sci = 0 )
	for tick in ax3.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax3.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	
	ax4 = fig.add_subplot(428)
	ax4.set_xlim(0,1)
	ax4.set_ylim(0,100)
	ax4.set_ylabel('Correctly retrieved \n cues (%)', multialignment='center')
	ax4.set_xlabel('Cue quality ')
	ax4.plot(In.getOrigVsOrig(), 100 -HippoHetero.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong(), c = 'b', label = 'Rec')
	ax4.plot(In.getOrigVsOrig(),100 -HippoHetero.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong(), c = 'b', label = 'No Rec', linestyle = '--')
	for tick in ax4.xaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	for tick in ax4.yaxis.get_major_ticks()[0:-1:2]:
		tick.label1.set_visible(False)
	makeLabel(ax = ax4, label = 'H', sci = 0 )
	
	HippoHetero = None
	HippoCa1 = load('GridMod1_252_Env'+str(env)+'_EcCa1Ec.p')
	ax1.plot(In.getOrigVsOrig(), HippoCa1.Ec_Ca1.Cor['StoredRecalled'].getOrigVsOrig(), c = 'm', label = 'EC-CA1-EC')
	ax2.plot(In.getOrigVsOrig(), HippoCa1.Ca1_Ec.Cor['StoredRecalled'].getOrigVsOrig(), c = 'm', label = 'EC-CA1-EC')
	ax3.plot(In.getOrigVsOrig(), 100 -HippoCa1.Ec_Ca1.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'm', label = 'EC-CA1_EC')
	ax4.plot(In.getOrigVsOrig(), 100 -HippoCa1.Ca1_Ec.Cor['StoredRecalled'].getProportionCompletedToWrong(), c = 'm', label = 'EC-CA1-EC')
	
	fig.tight_layout(rect = (0,0,1,0.97))
	collapse = 0

#Figure 8
def inputParameterOverviewAllNew():
	
	cor_diff_rec= np.zeros(4)
	cor_diff_no_rec= np.zeros(4)
	cor_diff_ca1= np.zeros(4)
	
	correct_diff_rec= np.zeros(4)
	correct_diff_no_rec= np.zeros(4)
	correct_diff_ca1= np.zeros(4)
	
	index_0= np.zeros(4)
	index_dg = np.zeros(4)
	
	r_0= np.zeros(4)
	r_dg = np.zeros(4)
	fig = plt.figure()


	ax_index = [3,3]
	i=0
	for mec_prop in [0, .5, 1]:

		In = load('GridMod'+str(mec_prop)+'_252_Env1.p')
		HippoDG = load('GridMod'+str(mec_prop)+'_252_Env1_DG05.p')
		Hippo0 = load('GridMod'+str(mec_prop)+'_252_Env1_DG0.p')
		EcCa1Ec = load('GridMod'+str(mec_prop)+'_252_Env1_EcCa1Ec.p')
		In.one_patterns = 1
		#separation index
		regression = scipy.stats.linregress(In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), HippoDG.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
		index_dg[i] = regression[0] + regression[1]
		r_dg[i] = copy.copy(regression[2])
		
		regression = scipy.stats.linregress(x = In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), y = Hippo0.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
		index_0[i] = regression[0] + regression[1]
		r_0[i] = copy.copy(regression[2])
		
		
		point_distance = In.getOrigVsOrig()[:-1] - In.getOrigVsOrig()[1:]
		#corrs
		cor_rec = Hippo0.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig()
		cor_no_rec = Hippo0.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig()
		cor_ca1 = EcCa1Ec.Ca1_Ec.Cor['StoredRecalled'].getOrigVsOrig()
		
		cor_diff_rec[i] = np.sum((cor_rec[1:]) * point_distance)
		cor_diff_no_rec[i] = np.sum((cor_no_rec[1:]) * point_distance)
		cor_diff_ca1[i] = np.sum((cor_ca1[1:]) * point_distance)
	
		#correct retrieved
		correct_rec = 100 -Hippo0.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong()
		correct_no_rec = 100 -Hippo0.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong()
		correct_ca1 = 100 -EcCa1Ec.Ca1_Ec.Cor['StoredRecalled'].getProportionCompletedToWrong()

		
		correct_diff_rec[i] = np.sum((correct_rec[1:]) * point_distance)
		correct_diff_no_rec[i] = np.sum((correct_no_rec[1:]) * point_distance)
		correct_diff_ca1[i] = np.sum((correct_ca1[1:]) * point_distance)
		i+=1
		



	ax = fig.add_subplot(ax_index[0], ax_index[1], 1)
	ax.set_ylabel(r'$Corr_{EC}$')
	plt.setp(ax.get_xticklabels(), visible=False)
	ax.plot([0, .5, 1], cor_diff_rec[:3][::-1], c = 'g', marker = 'd', label = 'EC-CA3-CA1-EC')
	ax.plot([0, .5, 1], cor_diff_no_rec[:3][::-1], c = 'g', linestyle = '--', marker = 'd')
	ax.plot([0, .5, 1], cor_diff_ca1[:3][::-1], c = 'm', marker = 'd', label = 'EC-CA1-EC')
	ax.set_ylim(-0, 1)
	ax.legend(loc = 'lower right', prop={'size':9})
	makeLabel(ax = ax, label = 'A', sci = 0 )
	ax.set_xticks([0, .5, 1])

	ax = fig.add_subplot(ax_index[0], ax_index[1], 4)
	ax.set_ylabel('correclty retrieved')
	plt.setp(ax.get_xticklabels(), visible=False)
	ax.set_xticks([0, .5, 1])
	ax.set_ylim(-0, 100)
	ax.plot([0, .5, 1], correct_diff_rec[:3][::-1], c = 'g', marker = 'd')
	ax.plot([0, .5, 1], correct_diff_no_rec[:3][::-1], c = 'g', linestyle = '--', marker = 'd')
	ax.plot([0, .5, 1], correct_diff_ca1[:3][::-1], c = 'm', marker = 'd')
	makeLabel(ax = ax, label = 'B', sci = 0 )

	ax = fig.add_subplot(ax_index[0], ax_index[1], 7)
	ax.set_ylabel('separation index')
	ax.set_xlabel('LEC proportion')
	ax.set_xticks([0, .5, 1])
	ax.plot([0, .5, 1], index_dg[:3][::-1], c = 'g', marker = 'd', label = 'plastic DG')
	ax.plot([0, .5, 1], index_0[:3][::-1], c = 'b', marker = 'd', label = 'static DG')
	prop = [0, .5, 1]
	makeLabel(ax = ax, label = 'C', sci = 0 )
	ax.legend(loc = 'best',prop={'size':9})
	ax.set_ylim(0, 1)
		
	i=0
	for env in [1,3,6,9]:
		mec_prop = 1
		
		In = load('GridMod1_252_Env'+str(env)+'.p')
		HippoDG = load('GridMod1_252_Env'+str(env)+'_DG05.p')
		Hippo0 = load('GridMod1_252_Env'+str(env)+'_DG0.p')
		EcCa1Ec = load('GridMod1_252_Env'+str(env)+'_EcCa1Ec.p')

		In.one_patterns = 1
		#separation index
		regression = scipy.stats.linregress(x = In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), y = HippoDG.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
		index_dg[i] = regression[0] + regression[1]
		r_dg[i] = copy.copy(regression[2])

		regression = scipy.stats.linregress(x = In.getCorOrigOther(at_noise = 0,subtract_orig_orig = 1), y = Hippo0.Ec_Ca3.Cor['StoredStored'].getCorOrigOther(at_noise = 0,subtract_orig_orig = 1))
		index_0[i] = regression[0] + regression[1]
		r_0[i] = copy.copy(regression[2])
		
		
		point_distance = In.getOrigVsOrig()[:-1] - In.getOrigVsOrig()[1:]
		#corrs
		cor_rec = Hippo0.Ca1_Ec.Cor['StoredRecalledRec'].getOrigVsOrig()
		cor_no_rec = Hippo0.Ca1_Ec.Cor['StoredRecalledNoRec'].getOrigVsOrig()
		cor_ca1 = EcCa1Ec.Ca1_Ec.Cor['StoredRecalled'].getOrigVsOrig()
		
		cor_diff_rec[i] = np.sum((cor_rec[1:]) * point_distance)
		cor_diff_no_rec[i] = np.sum((cor_no_rec[1:]) * point_distance)
		cor_diff_ca1[i] = np.sum((cor_ca1[1:]) * point_distance)
		

		#correct retrieved
		correct_rec = 100 -Hippo0.Ca1_Ec.Cor['StoredRecalledRec'].getProportionCompletedToWrong()
		correct_no_rec = 100 -Hippo0.Ca1_Ec.Cor['StoredRecalledNoRec'].getProportionCompletedToWrong()
		correct_ca1 = 100 -EcCa1Ec.Ca1_Ec.Cor['StoredRecalled'].getProportionCompletedToWrong()

		
		correct_diff_rec[i] = np.sum((correct_rec[1:]) * point_distance)
		correct_diff_no_rec[i] = np.sum((correct_no_rec[1:]) * point_distance)
		correct_diff_ca1[i] = np.sum((correct_ca1[1:]) * point_distance)
		i+=1	

	##no env
	ax = fig.add_subplot(ax_index[0], ax_index[1], 2)
	plt.setp(ax.get_xticklabels(), visible=False)
	plt.setp(ax.get_yticklabels(), visible=False)
	ax.set_xticks([1, 3, 6,9])
	ax.plot([1, 3, 6,9], cor_diff_rec, c = 'g', marker = 'd')
	ax.plot([1, 3, 6,9], cor_diff_no_rec, c = 'g', linestyle = '--', marker = 'd')
	ax.plot([1, 3, 6,9], cor_diff_ca1, c = 'm', marker = 'd')
	ax.set_ylim(0, 1)
	makeLabel(ax = ax, label = 'D', sci = 0 )
	
	ax = fig.add_subplot(ax_index[0], ax_index[1], 5)
	ax.set_xticks([1, 3, 6,9])
	ax.set_ylim(-0, 100)
	plt.setp(ax.get_xticklabels(), visible=False)
	plt.setp(ax.get_yticklabels(), visible=False)
	ax.plot([1, 3, 6,9], correct_diff_rec, c = 'g', marker = 'd')
	ax.plot([1, 3, 6,9], correct_diff_no_rec, c = 'g', linestyle = '--', marker = 'd')
	ax.plot([1, 3, 6,9], correct_diff_ca1, c = 'm', marker = 'd')
	makeLabel(ax = ax, label = 'E', sci = 0 )
	

	ax = fig.add_subplot(ax_index[0], ax_index[1], 8)
	plt.setp(ax.get_yticklabels(), visible=False)
	ax.set_xlabel('No. of environments')
	ax.set_xticks([1, 3, 6,9])
	ax.plot([1, 3, 6,9], index_dg, c = 'g', marker = 'd', label = 'slope')
	ax.plot([1, 3, 6,9], index_0, c = 'b', marker = 'd')
	ymax = ax.get_ylim()[1]
	ax.set_ylim(0, 1)
	prop = [1, 3, 6,9]
	i=0
	makeLabel(ax = ax, label = 'F', sci = 0 )
	


	###Dg Strateety LEC
	In = load('GridMod0_252_Env1.p')
	title = 'GridMod1_252_6'
	ca3_code = ''
	Hippo0 = load('GridMod0_252_Env1_DG0.p')

	In.one_patterns = 1
	ax = fig.add_subplot(ax_index[0],ax_index[1],3)

	ax.set_ylabel('Correlations in CA3')
	s = ax.scatter(In.getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), c = Hippo0.Ec_Ca3.Cor['StoredStored'].getDistancesWithin(subtract_orig_orig = 1, locations = In.getStoredlocations()), cmap = plt.get_cmap('jet'),vmin = 0, vmax = .5)
	line, r = regressionLine(In.getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e))
	ax.plot([0,1], [line(0), line(1)], c = 'k')
	ax.text(0, 0.9, s = 'r = '+str(np.round(r,2)))
	ax.text(0, 0.7, s = 's = '+str(np.round(line.coeffs[0],2)))
	ax.set_xlim(-0.1, 1.1)
	ax.set_ylim( -0.1, 1.1)
	makeLabel(ax = ax, label = 'G', sci = 0 )
	Format = mpl.ticker.FixedFormatter(['0', '', '', '', '', '0.25', '' , '', '', '', '>0.5'])
	Locator = mpl.ticker.FixedLocator([0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5], nbins=None)
	cbar = fig.colorbar(s, ax = ax, format = Format, ticks = Locator, use_gridspec=True)
	for tick in ax.xaxis.get_major_ticks()[2:-2]:
		tick.label1.set_visible(False)
	for tick in ax.yaxis.get_major_ticks()[2:-2]:
		tick.label1.set_visible(False)
	
	
	###Dg Strateety MEC
	In = load('GridMod1_252_Env9.p')
	title = 'GridMod1_252_6'
	ca3_code = ''
	Hippo0 = load('GridMod1_252_Env9_DG0.p')

	In.one_patterns = 1
	ax = fig.add_subplot(ax_index[0],ax_index[1],6)

	ax.set_ylabel('Correlations in CA3')
	s = ax.scatter(In.getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), c = Hippo0.Ec_Ca3.Cor['StoredStored'].getDistancesWithin(subtract_orig_orig = 1, locations = In.getStoredlocations()), cmap = plt.get_cmap('jet'),vmin = 0, vmax = .5)
	line, r = regressionLine(In.getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorsWithin(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e))
	ax.plot([0,1], [line(0), line(1)], c = 'k')
	ax.text(0, 0.9, s = 'r = '+str(np.round(r,2)))
	ax.text(0, 0.7, s = 's = '+str(np.round(line.coeffs[0],2)))
	ax.set_xlim(-0.1, 1.1)
	ax.set_ylim( -0.1, 1.1)
	makeLabel(ax = ax, label = 'H', sci = 0 )
	Format = mpl.ticker.FixedFormatter(['0', '', '', '', '', '0.25', '' , '', '', '', '>0.5'])
	Locator = mpl.ticker.FixedLocator([0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5], nbins=None)
	cbar = fig.colorbar(s, ax = ax, format = Format, ticks = Locator, use_gridspec=True)
	for tick in ax.xaxis.get_major_ticks()[2:-2]:
		tick.label1.set_visible(False)
	for tick in ax.yaxis.get_major_ticks()[2:-2]:
		tick.label1.set_visible(False)


	ax = fig.add_subplot(ax_index[0],ax_index[1],9)
	s = ax.scatter(In.getCorsAcross(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorsAcross(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), c = Hippo0.Ec_Ca3.Cor['StoredStored'].getDistancesAcross(subtract_orig_orig = 1, locations = In.getStoredlocations()), cmap = plt.get_cmap('jet'), vmin = 0,vmax = .5)
	line, r = regressionLine(In.getCorsAcross(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e), Hippo0.Ec_Ca3.Cor['StoredStored'].getCorsAcross(at_noise = 0,subtract_orig_orig = 1, size = In.number_to_store, n_e = In.n_e))
	ax.plot([0,1], [line(0), line(1)], c = 'k')
	ax.text(0, 0.9, s = 'r = '+str(np.round(r,2)))
	ax.text(0, 0.7, s = 's = '+str(np.round(line(1)-line(0),2)))
	ax.set_xlim(-0.1, 1.1)
	ax.set_ylim( -0.1, 1.1)
	ax.set_ylabel('Correlations in CA3')
	ax.set_xlabel('Correlations in EC')
	Format = mpl.ticker.FixedFormatter(['0', '', '', '', '', '0.25', '' , '', '', '', '>0.5'])
	Locator = mpl.ticker.FixedLocator([0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5], nbins=None)
	cbar = fig.colorbar(s, ax = ax, format = Format, ticks = Locator, use_gridspec=True)
	for tick in ax.xaxis.get_major_ticks()[2:-2]:
		tick.label1.set_visible(False)
	for tick in ax.yaxis.get_major_ticks()[2:-2]:
		tick.label1.set_visible(False)
	makeLabel(ax = ax, label = 'I', sci = 0 )
	fig.tight_layout(rect = (0,0,1,0.97))
	collapse = 0



#saveSimMod()

#ecTestMod()
#rollsToScaled()
#overlapActiveCa3Random()
#dgNodgMod()
#randomOverviewNew()
#gridOverviewNewMod()
#fullVsCa1NewMod()
inputParameterOverviewAllNew()



end = time.time()-begin
print 'finished simulation  in '+str(int(end/3600)) + 'h '+str(int((end-int(end/3600)*3600)/60)) + 'min ' +str(int(end - int(end/3600)*3600- int((end-int(end/3600)*3600)/60)*60)) + 'sec'
save_figures(path = '', start = 1, title = 'Figures_', file_type = 'eps')
plt.show()






